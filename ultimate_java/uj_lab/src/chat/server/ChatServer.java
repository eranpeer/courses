package chat.server;

import java.util.ArrayList;
import java.util.List;

public class ChatServer {

	public static class ChatSession {

		private String room = null;
		private final ChatServer server;
		private String user = null;

		public ChatSession(ChatServer server) {
			this.server = server;
		}

		public String createRoom(String room) {
			System.out.println("got message create room " + user + " " + room);
			return server.addRoom(this, room);
		}

		public String getUser() {
			return user;
		}

		public String join(String room) {
			System.out.println("got message join room " + user + " " + room);
			return server.joinRoom(this, room);
		}

		public String login(String user) {
			return server.login(this, user);
		}

		public String logout() {
			return server.logout(this);
		}

		public String say(String text) {
			System.out.println("got message publish " + user + " " + room + " " + text);
			return server.publish(this, text);
		}

		public void sendLineToClient(String user, String text) {

		}

	}

	final class RoomChangePublisher implements Room.Listener {
		@Override
		public void lineAdded(Room source, Line line) {
			for (String user : source.users()) {
				ChatSession session = null;
				synchronized (activeSessions) {
					session = getClientSession(user);
				}
				if (session != null) {
					session.sendLineToClient(line.getUser(), line.getText());
				} else {
					source.leave(user);
				}
			}
		}

		@Override
		public void userJoined(Room source, String user) {
		}

		@Override
		public void userLeft(Room source, String user) {
		}
	}

	protected List activeSessions = new ArrayList();

	private List rooms = new ArrayList();

	public ChatServer() {
		super();
	}

	public String addRoom(ChatSession chatSession, String roomName) {
		synchronized (rooms) {
			if (getRoom(roomName) != null)
				return "room " + roomName + " already exists";
			Room room = new Room(chatSession.user, roomName);
			room.setListener(new RoomChangePublisher());
			rooms.add(room);
		}
		return null;
	}

	public String joinRoom(ChatSession chatSession, String roomName) {
		Room room = null;
		synchronized (rooms) {
			room = getRoom(roomName);
		}
		if (room == null) {
			return "room " + roomName + " does not exist";
		}

		String msg = leaveCurrRoom(chatSession);
		if (msg != null)
			return msg;

		room.join(chatSession.user);
		chatSession.room = roomName;
		return null;
	}

	public String login(ChatSession session, String user) {
		synchronized (activeSessions) {
			if (getClientSession(user) != null)
				return "user " + user + " is already logged in";
			session.user = user;
			activeSessions.add(session);
		}
		return null;
	}

	public String logout(ChatSession chatSession) {
		leaveCurrRoom(chatSession);
		synchronized (activeSessions) {
			if (!activeSessions.remove(chatSession)) {
				return "not an active session";
			}
		}
		return null;
	}

	public String publish(ChatSession chatSession, String text) {
		if (chatSession.room == null) {
			return "user " + chatSession.room + " is not in a room";
		}
		Room room = null;
		synchronized (rooms) {
			room = getRoom(chatSession.room);
		}
		if (room == null) {
			return "room " + chatSession.room + " does not exist";
		}
		room.addLine(chatSession.user, text);
		return null;
	}

	protected ChatSession getClientSession(String user) {
		for (int i = 0; i < activeSessions.size(); i++) {
			ChatSession s = (ChatSession) activeSessions.get(i);
			if (s.getUser().equals(user)) {
				return s;
			}
		}
		return null;
	}

	private Room getRoom(String roomName) {
		for (int i = 0; i < rooms.size(); i++) {
			Room r = (Room) rooms.get(i);
			if (r.getName().equals(roomName)) {
				return r;
			}
		}
		return null;
	}

	private String leaveCurrRoom(ChatSession chatSession) {
		if (chatSession.room == null)
			return null;
		Room room = null;
		synchronized (rooms) {
			room = getRoom(chatSession.room);
		}
		if (room == null) {
			return "room " + chatSession.room + " does not exist";
		}
		room.leave(chatSession.user);
		return null;
	}

}