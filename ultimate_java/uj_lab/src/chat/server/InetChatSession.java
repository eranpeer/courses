package chat.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.SocketChannel;

import chat.MessageTypes;
import chat.MessageTypes.CreateRoomReq;
import chat.MessageTypes.JoinRoomReq;
import chat.MessageTypes.LoginReq;
import chat.MessageTypes.SendReq;
import chat.server.ChatServer.ChatSession;
import chat.util.BlockingCahnnel2InputStreamAdapter;
import chat.util.BlockingChannel2OutputStreamAdapter;
import chat.util.MessageUtils;

public class InetChatSession extends ChatSession implements Runnable {

	static void logResult(String line) {
		if (line != null)
			System.out.println(line);
	}

	private final SocketChannel channel;
	private OutputStream out;

	public InetChatSession(ChatServer server, SocketChannel clientConnection) throws IOException {
		super(server);
		this.channel = clientConnection;
		out = new BlockingChannel2OutputStreamAdapter(channel);
	}

	public void processCommands(InputStream in) throws IOException, ClassNotFoundException {
		while (true) {
			Object req = MessageUtils.readObject(in);
			if (req instanceof CreateRoomReq) {
				CreateRoomReq createRoomReq = (CreateRoomReq) req;
				logResult(createRoom(createRoomReq.getRoomName()));
			} else if (req instanceof JoinRoomReq) {
				JoinRoomReq joinRoomReq = (JoinRoomReq) req;
				logResult(join(joinRoomReq.getRoomName()));
			} else if (req instanceof SendReq) {
				SendReq sendReq = (SendReq) req;
				logResult(say(sendReq.getText()));
			}
		}
	}

	public void run() {
		try {
			InputStream in = new BlockingCahnnel2InputStreamAdapter(channel);
			if (!login(in))
				return;
			processCommands(in);
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			logout();
			try {
				channel.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public synchronized void sendLineToClient(String user, String text) {
		try {
			out.write(MessageUtils.createMessage(new MessageTypes.NewLineNotification(user, text)));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean login(InputStream in) throws IOException, ClassNotFoundException {
		LoginReq req = (LoginReq) MessageUtils.readObject(in);
		String user = req.getUser();
		System.out.println("got message login " + user);
		String loginResult = login(user);
		logResult(loginResult);
		return loginResult == null;
	}

}