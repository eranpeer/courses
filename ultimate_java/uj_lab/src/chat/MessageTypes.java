package chat;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectInputStream.GetField;
import java.io.Serializable;

public interface MessageTypes {

	public static class LoginReq implements Serializable {
		private static final long serialVersionUID = 1L;
		private String user;

		public LoginReq(String user) {
			this.user = user;
		}

		public String getUser() {
			return user;
		}
	}

	public static class CreateRoomReq implements Serializable {
		private static final long serialVersionUID = 1L;
		private String roomName;
		private String description;

		public CreateRoomReq(String roomName, String description) {
			this.roomName = roomName;
			this.description = description;
		}

		public String getRoomName() {
			return roomName;
		}

		public String getDescription() {
			return description;
		}

		private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
			GetField fields = stream.readFields();
			roomName = (String) fields.get("roomName", null);
			description = (String) fields.get("description", roomName);
		}
	}

	public static class JoinRoomReq implements Serializable {
		private static final long serialVersionUID = 1L;
		private String roomName;

		public JoinRoomReq(String roomName) {
			this.roomName = roomName;
		}

		public String getRoomName() {
			return roomName;
		}
	}

	public static class SendReq implements Serializable {
		private static final long serialVersionUID = 1L;
		private String text;

		public SendReq(String text) {
			this.text = text;
		}

		public String getText() {
			return text;
		}
	}

	public static class NewLineNotification implements Serializable {
		private static final long serialVersionUID = 1L;
		private String user;
		private String text;

		public NewLineNotification(String user, String text) {
			this.user = user;
			this.text = text;
		}

		public String getUser() {
			return user;
		}

		public String getText() {
			return text;
		}
	}

}