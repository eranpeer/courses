package chat.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;

public class ChatClient {

	public static void main(String[] args) throws UnknownHostException, IOException {
		int serverPort = 12345;
		String user = null;
		if (args.length == 2) {
			serverPort = Integer.parseInt(args[0]);
			user = args[1];
		} else if (args.length == 1) {
			user = args[0];
		} else {
			System.out.println("usage: ChatClient [server_port] <user>");
			return;
		}

		Socket serverSocket = null;
		try {
			serverSocket = new Socket("localhost", serverPort);
			ChatSessionProxy serverProxy = new ChatSessionProxy(serverSocket, new ChatSessionProxy.Listener() {
				@Override
				public void onNewLine(String user, String text) {
					System.out.println(user + ": " + text);
				}
			});
			System.out.println("press Ctrl+C to exit");
			serverProxy.login(user);
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			while (true) {
				String commandText = reader.readLine();
				applyCommand(serverProxy, commandText);
			}
		} finally {
			if (serverSocket != null){
				serverSocket.close();
			}
		}
	}

	private static void applyCommand(ChatSessionProxy serverProxy, String commandText) throws IOException {
		String[] tokens = commandText.split("[' ','\t']+");
		try {
			if (tokens[0].equals("create")) {
				String room = tokens[1];
				serverProxy.createRoom(room);
			} else if (tokens[0].equals("join")) {
				String room = tokens[1];
				serverProxy.joinRoom(room);
			} else if (tokens[0].equals("say")) {
				if (tokens.length == 1) {
					throw new ArrayIndexOutOfBoundsException();
				}
				String text = commandText.substring("say".length()).trim();
				serverProxy.say(text);
			} else {
				System.out.println("invalid command " + Arrays.toString(tokens));
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("invalid command line tokens " + Arrays.toString(tokens));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}