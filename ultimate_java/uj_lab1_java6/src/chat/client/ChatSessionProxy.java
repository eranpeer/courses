package chat.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import chat.MessageTypes;

public class ChatSessionProxy {

	public static interface Listener {
		void onNewLine(String user, String text);
	}

	private class ServerNotificationsHandler implements Runnable {
		private InputStream inStream;

		public ServerNotificationsHandler(InputStream inputStream) {
			this.inStream = inputStream;
		}

		public void run() {
			try {
				DataInputStream in = new DataInputStream(inStream);
				while (true) {
					int messageType = in.readInt();
					if (messageType == MessageTypes.NEW_LINE_NOTIFICATION) {
						String user = in.readUTF();
						String text = in.readUTF();
						listener.onNewLine(user, text);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private DataOutputStream out;
	private ChatSessionProxy.Listener listener;

	public ChatSessionProxy(Socket server, ChatSessionProxy.Listener listener) throws IOException {
		this.listener = listener;
		out = new DataOutputStream(server.getOutputStream());
		new Thread(new ServerNotificationsHandler(server.getInputStream())).start();
	}

	public void say(String text) throws IOException {
		out.writeInt(MessageTypes.PUBLISH);
		out.writeUTF(text);
	}

	public void joinRoom(String room) throws IOException {
		out.writeInt(MessageTypes.JOIN_ROOM);
		out.writeUTF(room);
	}

	public void createRoom(String room) throws IOException {
		out.writeInt(MessageTypes.CREATE_ROOM);
		out.writeUTF(room);
	}

	public void login(String user) throws IOException {
		out.writeInt(MessageTypes.LOGIN);
		out.writeUTF(user);
	}

}