package chat;

public interface MessageTypes {
	public static final int LOGIN = 0;
	public static final int CREATE_ROOM = 1;
	public static final int JOIN_ROOM = 2;
	public static final int PUBLISH = 3;
	
	public static final int NEW_LINE_NOTIFICATION = 10;
}