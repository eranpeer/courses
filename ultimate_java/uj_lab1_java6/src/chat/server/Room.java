package chat.server;

import java.util.ArrayList;
import java.util.List;

public class Room {

	public static interface Listener {
		void userJoined(Room source, String user);

		void userLeft(Room source, String user);

		void lineAdded(Room source, Line line);
	}

	private String owner;
	private String name;
	private List<Line> lines = new ArrayList<Line>();
	private List<String> users = new ArrayList<String>();
	private Room.Listener listener;

	public Room(String owner, String name) {
		this.owner = owner;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public String getOwner() {
		return owner;
	}

	public void addLine(String user, String text) {
		Line line = new Line(user, text);
		synchronized (lines) {
			lines.add(line);
		}
		if (listener != null) {
			listener.lineAdded(this, line);
		}
	}

	public void join(String user) {
		synchronized (users) {
			users.add(user);
		}
		if (listener != null) {
			listener.userJoined(this, user);
		}
	}

	public void leave(String user) {
		synchronized (users) {
			users.remove(user);
		}
		if (listener != null) {
			listener.userLeft(this, user);
		}
	}

	public String[] users() {
		synchronized (users) {
			return users.toArray(new String[users.size()]);
		}
	}

	public void setListener(Room.Listener listener) {
		this.listener = listener;
	}
}