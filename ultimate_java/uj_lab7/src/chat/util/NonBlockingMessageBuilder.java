package chat.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * 
 <code>
class ConnectionData {
	ByteBuffer buff = ByteBuffer.allocate(1024);
	NonBlockingMessageBuilder messageBuilder = new NonBlockingMessageBuilder();
}

if (key.isAcceptable()) {
	ServerSocketChannel channel = (ServerSocketChannel) key.channel();
	SocketChannel client = channel.accept();
	client.configureBlocking(false);
	ConnectionData connectionData = new ConnectionData();
	SelectionKey clientKey = client.register(selector, SelectionKey.OP_READ, connectionData);
} else if (key.isReadable()) {
	SocketChannel channel = (SocketChannel) key.channel();
	ConnectionData connectionData = (ConnectionData) key.attachment();
	while (channel.read(connectionData.buff) > 0) {
		connectionData.buff.flip();
		while (connectionData.buff.remaining() > 0) {
			connectionData.messageBuilder.append(connectionData.buff);
			if (!connectionData.messageBuilder.isPayloadReady()) {
				break;
			}
			Object payload = connectionData.messageBuilder.deserializePayload();
			connectionData.messageBuilder.reset();
			
			process(payload);
		}
		connectionData.buff.clear();
	}
}
</code>
 */
public class NonBlockingMessageBuilder {

	static class InnerByteArrayOutputStream extends ByteArrayOutputStream {
		public byte[] accessBuf() {
			return buf;
		}
	}

	private NonBlockingMessageBuilder.InnerByteArrayOutputStream payload = new InnerByteArrayOutputStream();

	private short payloadSize = -1;

	public boolean append(ByteBuffer buff) {
		if (isPayloadReady())
			return true;
		if (buff.remaining() >= 2 && payloadSize == -1) {
			payloadSize = buff.getShort();
		}
		while (payload.size() < payloadSize && buff.remaining() > 0) {
			payload.write(buff.get());
		}
		return payload.size() == payloadSize;
	}

	public Object deserializePayload() throws IOException, ClassNotFoundException {
		try (NoHeaderObjectInputStream objectInputStream = new NoHeaderObjectInputStream(new ByteArrayInputStream(
				payload.accessBuf()))) {
			return objectInputStream.readObject();
		}
	}

	public boolean isPayloadReady() {
		return payload.size() == payloadSize;
	}

	public void reset() {
		payloadSize = -1;
		payload.reset();
	}
}