package chat.server;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.Channels;

import chat.MessageTypes;
import chat.server.ChatServer.ChatSession;
import chat.util.MessageUtils;

public class InetChatSession extends ChatSession {

	static void logResult(String line) {
		if (line != null)
			System.out.println(line);
	}

	private final AsynchronousSocketChannel channel;
	private OutputStream out;

	public InetChatSession(ChatServer server, AsynchronousSocketChannel clientConnection) {
		super(server);
		this.channel = clientConnection;
		out = Channels.newOutputStream(channel);
	}

	public synchronized void sendLineToClient(String user, String text) {
		try {
			out.write(MessageUtils.createMessage(new MessageTypes.NewLineNotification(user, text)));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}