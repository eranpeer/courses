package chat.server;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

import chat.MessageTypes.CreateRoomReq;
import chat.MessageTypes.JoinRoomReq;
import chat.MessageTypes.LoginReq;
import chat.MessageTypes.SendReq;
import chat.util.NonBlockingMessageBuilder;

public class InetChatServer extends ChatServer {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		int port = 12345;
		if (args.length > 0) {
			port = Integer.parseInt(args[0]);
		}
		new InetChatServer(port).run();
	}

	private int port;

	public InetChatServer(int port) {
		this.port = port;
	}

	private final class ClientAcceptHandler implements CompletionHandler<AsynchronousSocketChannel, Object> {
		private final AsynchronousServerSocketChannel ssc;

		private ClientAcceptHandler(AsynchronousServerSocketChannel ssc) {
			this.ssc = ssc;
		}

		@Override
		public void completed(final AsynchronousSocketChannel clientSocket, Object attachment) {
			ConnectionData connectionData = new ConnectionData();
			connectionData.session = new InetChatSession(InetChatServer.this, clientSocket);
			CompletionHandler<Integer, ConnectionData> clientDataHandler = new ClientSocketReadHandler(clientSocket);
			clientSocket.read(connectionData.buff, connectionData, clientDataHandler);
			ssc.accept(null, new ClientAcceptHandler(ssc));
		}

		@Override
		public void failed(Throwable exc, Object attachment) {
		}
	}

	private final class ClientSocketReadHandler implements CompletionHandler<Integer, ConnectionData> {
		private final AsynchronousSocketChannel clientSocket;

		private ClientSocketReadHandler(AsynchronousSocketChannel clientSocket) {
			this.clientSocket = clientSocket;
		}

		@Override
		public void completed(Integer count, ConnectionData connectionData) {
			connectionData.buff.flip();
			try {
				processBuffer(clientSocket, connectionData);
				connectionData.buff.clear();
				clientSocket.read(connectionData.buff, connectionData, this);
			} catch (ClassNotFoundException | IOException e) {
				try {
					clientSocket.close();
					connectionData.session.logout();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}

		@Override
		public void failed(Throwable exc, ConnectionData attachment) {
			attachment.session.logout();
		}
	}

	private static class ConnectionData {
		ChatSession session;
		ByteBuffer buff = ByteBuffer.allocate(1024);
		NonBlockingMessageBuilder messageBuilder = new NonBlockingMessageBuilder();
	}

	public void run() throws IOException, ClassNotFoundException {

		final AsynchronousServerSocketChannel ssc = AsynchronousServerSocketChannel.open();
		ssc.bind(new InetSocketAddress(port));

		System.out.println("server waiting for client connections on port " + ssc.getLocalAddress());
		System.out.println("press Ctrl+C to exit");

		ssc.accept(null, new ClientAcceptHandler(ssc));

		synchronized (this) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void processBuffer(Closeable channel, ConnectionData connectionData) throws IOException, ClassNotFoundException {
		while (connectionData.buff.remaining() > 0) {
			connectionData.messageBuilder.append(connectionData.buff);
			if (!connectionData.messageBuilder.isPayloadReady()) {
				break;
			}
			Object payload = connectionData.messageBuilder.deserializePayload();
			connectionData.messageBuilder.reset();
			processRequest(connectionData.session, payload);
		}
	}

	private void processRequest(ChatSession session, Object req) {
		try {
			if (req instanceof LoginReq) {
				LoginReq loginReq = (LoginReq) req;
				logResult(session.login(loginReq.getUser()));
			} else if (req instanceof CreateRoomReq) {
				CreateRoomReq createRoomReq = (CreateRoomReq) req;
				logResult(session.createRoom(createRoomReq.getRoomName()));
			} else if (req instanceof JoinRoomReq) {
				JoinRoomReq joinRoomReq = (JoinRoomReq) req;
				logResult(session.join(joinRoomReq.getRoomName()));
			} else if (req instanceof SendReq) {
				SendReq sendReq = (SendReq) req;
				logResult(session.say(sendReq.getText()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			session.logout();
			throw e;
		}
	}

	static void logResult(String line) {
		if (line != null)
			System.out.println(line);
	}

}