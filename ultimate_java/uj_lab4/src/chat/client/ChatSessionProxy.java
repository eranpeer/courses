package chat.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.channels.SocketChannel;

import chat.MessageTypes;
import chat.MessageTypes.CreateRoomReq;
import chat.MessageTypes.LoginReq;
import chat.MessageTypes.NewLineNotification;
import chat.MessageTypes.SendReq;
import chat.util.BlockingChannel2InputStreamAdapter;
import chat.util.BlockingChannel2OutputStreamAdapter;
import chat.util.ChannelUtils;

public class ChatSessionProxy {

	public static interface Listener {
		void onNewLine(String user, String text);
	}

	private class ServerNotificationsHandler implements Runnable {
		private InputStream inStream;

		public ServerNotificationsHandler(InputStream in) {
			inStream = in;
		}

		public void run() {
			try {
				ObjectInputStream in = new ObjectInputStream(inStream);
				while (true) {
					NewLineNotification req = (NewLineNotification) in.readObject();
					listener.onNewLine(req.getUser(), req.getText());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private ObjectOutputStream out;
	private ChatSessionProxy.Listener listener;

	public ChatSessionProxy(SocketChannel serverSocket, ChatSessionProxy.Listener listener) throws IOException {
		this.listener = listener;
		out = new ObjectOutputStream(ChannelUtils.newOutputStream(serverSocket));
		new Thread(new ServerNotificationsHandler(ChannelUtils.newInputStream(serverSocket))).start();
//		out = new ObjectOutputStream(new BlockingChannel2OutputStreamAdapter(serverSocket));
//		new Thread(new ServerNotificationsHandler(new BlockingChannel2InputStreamAdapter(serverSocket))).start();
	}

	public void say(String text) throws IOException {
		out.writeObject(new SendReq(text));
	}

	public void joinRoom(String room) throws IOException {
		out.writeObject(new MessageTypes.JoinRoomReq(room));
	}

	public void createRoom(String room, String description) throws IOException {
		out.writeObject(new CreateRoomReq(room, description));
	}

	public void login(String user) throws IOException {
		out.writeObject(new LoginReq(user));
	};

}