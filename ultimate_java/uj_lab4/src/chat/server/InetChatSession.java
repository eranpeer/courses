package chat.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.channels.Channels;
import java.nio.channels.SocketChannel;

import chat.MessageTypes;
import chat.MessageTypes.CreateRoomReq;
import chat.MessageTypes.JoinRoomReq;
import chat.MessageTypes.LoginReq;
import chat.MessageTypes.SendReq;
import chat.server.ChatServer.ChatSession;
import chat.util.BlockingChannel2InputStreamAdapter;
import chat.util.BlockingChannel2OutputStreamAdapter;

public class InetChatSession extends ChatSession implements Runnable {

	static void logResult(String line) {
		if (line != null)
			System.out.println(line);
	}

	private final SocketChannel channel;
	private ObjectOutputStream out;

	public InetChatSession(ChatServer server, SocketChannel clientConnection) throws IOException {
		super(server);
		this.channel = clientConnection;
		out = new ObjectOutputStream(Channels.newOutputStream(channel));
	}

	public void processCommands(ObjectInputStream in) throws IOException, ClassNotFoundException {
		while (true) {
			Object req = in.readObject();
			if (req instanceof CreateRoomReq) {
				CreateRoomReq createRoomReq = (CreateRoomReq) req;
				logResult(createRoom(createRoomReq.getRoomName()));
			} else if (req instanceof JoinRoomReq) {
				JoinRoomReq joinRoomReq = (JoinRoomReq) req;
				logResult(join(joinRoomReq.getRoomName()));
			} else if (req instanceof SendReq) {
				SendReq sendReq = (SendReq) req;
				logResult(say(sendReq.getText()));
			}
		}
	}

	public void run() {
		try {
			ObjectInputStream in = new ObjectInputStream(Channels.newInputStream(channel));
			if (!login(in))
				return;
			processCommands(in);
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			logout();
			try {
				channel.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public synchronized void sendLineToClient(String user, String text) {
		try {
			out.writeObject(new MessageTypes.NewLineNotification(user, text));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean login(ObjectInputStream in) throws IOException, ClassNotFoundException {
		LoginReq req = (LoginReq) in.readObject();
		String user = req.getUser();
		System.out.println("got message login " + user);
		String loginResult = login(user);
		logResult(loginResult);
		return loginResult == null;
	}

}