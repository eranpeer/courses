package chat.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.ServerSocketChannel;

public class InetChatServer extends ChatServer {

	public static void main(String[] args) throws IOException {
		int port = 12345;
		if (args.length > 0) {
			port = Integer.parseInt(args[0]);
		}
		new InetChatServer(port).run();
	}

	private int port;

	public InetChatServer(int port) {
		this.port = port;
	}

	public void run() throws IOException {
		try (ServerSocketChannel ssc = ServerSocketChannel.open()) {
			ssc.configureBlocking(true);
			ssc.bind(new InetSocketAddress(port), 0);
			ServerSocket serverSocket = ssc.socket();

			System.out.println("server waiting for client connections on port " + serverSocket.getLocalPort());
			System.out.println("press Ctrl+C to exit");
			
			while (true) {
				final Socket clientSocket = serverSocket.accept();
				try {
					InetChatSession session = new InetChatSession(this, clientSocket.getChannel());
					new Thread(session).start();
				} catch (IOException e) {
					System.out.println("failed to create session");
				}
			}
		}
	}

}