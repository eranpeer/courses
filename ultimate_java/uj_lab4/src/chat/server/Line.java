package chat.server;

public class Line {
	private String user;
	private String text;

	public Line(String user, String text) {
		this.user = user;
		this.text = text;
	}

	public String getUser() {
		return user;
	}

	public String getText() {
		return text;
	}
}