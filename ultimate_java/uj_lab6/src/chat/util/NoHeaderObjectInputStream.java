package chat.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.StreamCorruptedException;

public class NoHeaderObjectInputStream extends ObjectInputStream {

	public NoHeaderObjectInputStream(InputStream in) throws IOException {
		super(in);
	}

	@Override
	protected void readStreamHeader() throws IOException, StreamCorruptedException {
	}
}