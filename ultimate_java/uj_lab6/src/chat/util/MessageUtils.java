package chat.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

public class MessageUtils {

	public static void writeObject(OutputStream out, Serializable object) throws IOException {
		out.write(createMessage(object));
	}

	public static byte[] createMessage(Serializable payload) throws IOException {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();

		putShort(stream, (short) 0); // make room for the payload size.

		try (NoHeaderObjectOutputStream oos = new NoHeaderObjectOutputStream(stream)) {
			oos.writeObject(payload);
			oos.flush();
		}

		byte[] bytes = stream.toByteArray();
		short payloadSize = (short) (bytes.length - 2);

		putShort(bytes, payloadSize); // write payload size at first 2 bytes of message

		return bytes;
	}

	public static Object readObject(InputStream stream) throws IOException, ClassNotFoundException {
		byte b1 = (byte) stream.read();
		byte b0 = (byte) stream.read();
		short payloadSize = makeShort(b1, b0);
		NoHeaderObjectInputStream in = new NoHeaderObjectInputStream(stream);
		return in.readObject();
	}

	private static short makeShort(byte b1, byte b0) {
		return (short) ((b1 << 8) | (b0 & 0xff));
	}

	private static void putShort(byte[] bytes, short v) {
		bytes[1] = (byte) ((v >>> 0));
		bytes[0] = (byte) ((v >>> 8));
	}

	private static void putShort(OutputStream out, short val) throws IOException {
		out.write((byte) (val >>> 8));
		out.write((byte) (val));
	}
}