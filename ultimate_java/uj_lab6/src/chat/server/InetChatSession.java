package chat.server;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.SocketChannel;

import chat.MessageTypes;
import chat.server.ChatServer.ChatSession;
import chat.util.BlockingChannel2OutputStreamAdapter;
import chat.util.MessageUtils;

public class InetChatSession extends ChatSession {

	static void logResult(String line) {
		if (line != null)
			System.out.println(line);
	}

	private final SocketChannel channel;
	private OutputStream out;

	public InetChatSession(ChatServer server, SocketChannel clientConnection) throws IOException {
		super(server);
		this.channel = clientConnection;
		out = new BlockingChannel2OutputStreamAdapter(channel);
	}

	public synchronized void sendLineToClient(String user, String text) {
		try {
			out.write(MessageUtils.createMessage(new MessageTypes.NewLineNotification(user, text)));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}