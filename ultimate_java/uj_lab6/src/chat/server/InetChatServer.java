package chat.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

import chat.MessageTypes.CreateRoomReq;
import chat.MessageTypes.JoinRoomReq;
import chat.MessageTypes.LoginReq;
import chat.MessageTypes.SendReq;
import chat.util.NonBlockingMessageBuilder;

public class InetChatServer extends ChatServer {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		int port = 12345;
		if (args.length > 0) {
			port = Integer.parseInt(args[0]);
		}
		new InetChatServer(port).run();
	}

	private int port;

	public InetChatServer(int port) {
		this.port = port;
	}

	private static class ConnectionData {
		ChatSession session;
		ByteBuffer buff = ByteBuffer.allocate(1024);
		NonBlockingMessageBuilder messageBuilder = new NonBlockingMessageBuilder();
	}

	public void run() throws IOException, ClassNotFoundException {

		Selector selector = Selector.open();

		ServerSocketChannel ssc = ServerSocketChannel.open();
		ssc.configureBlocking(false);
		ssc.bind(new InetSocketAddress(port));
		System.out.println("server waiting for client connections on port " + ssc.getLocalAddress());
		System.out.println("press Ctrl+C to exit");

		SelectionKey serverChannelKey = ssc.register(selector, SelectionKey.OP_ACCEPT);
		
		
		
		while (selector.select() > 0) {
			Set<SelectionKey> selectedKeys = selector.selectedKeys();
			for (Iterator<SelectionKey> i = selectedKeys.iterator(); i.hasNext();) {
				SelectionKey key = (SelectionKey) i.next();
				i.remove();
				if (key.isAcceptable()) {
					ServerSocketChannel channel = (ServerSocketChannel) key.channel();
					SocketChannel client = channel.accept();
					client.configureBlocking(false);
					// client.socket().setTcpNoDelay(true);
					ConnectionData connectionData = new ConnectionData();
					connectionData.session = new InetChatSession(this, client);
					SelectionKey clientKey = client.register(selector, SelectionKey.OP_READ, connectionData);
				} else if (key.isReadable()) {
					SocketChannel channel = (SocketChannel) key.channel();
					ConnectionData connectionData = (ConnectionData) key.attachment();
					processAvailableBytes(channel, connectionData);
				}
			}
		}
	}

	private void processAvailableBytes(SocketChannel channel, ConnectionData connectionData) throws IOException,
			ClassNotFoundException {
		try {
			while (channel.read(connectionData.buff) > 0) {
				connectionData.buff.flip();
				processBuffer(channel, connectionData);
				connectionData.buff.clear();
			}
		} catch (Exception e) {
			try {
				channel.close();
				connectionData.session.logout();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}

	}

	private void processBuffer(SocketChannel channel, ConnectionData connectionData) throws IOException, ClassNotFoundException {
		while (connectionData.buff.remaining() > 0) {
			connectionData.messageBuilder.append(connectionData.buff);
			if (!connectionData.messageBuilder.isPayloadReady()) {
				break;
			}
			Object payload = connectionData.messageBuilder.deserializePayload();
			connectionData.messageBuilder.reset();

			processRequest(connectionData.session, payload);
		}
	}

	private void processRequest(ChatSession session, Object req) {
		try {
			if (req instanceof LoginReq) {
				LoginReq loginReq = (LoginReq) req;
				logResult(session.login(loginReq.getUser()));
			} else if (req instanceof CreateRoomReq) {
				CreateRoomReq createRoomReq = (CreateRoomReq) req;
				logResult(session.createRoom(createRoomReq.getRoomName()));
			} else if (req instanceof JoinRoomReq) {
				JoinRoomReq joinRoomReq = (JoinRoomReq) req;
				logResult(session.join(joinRoomReq.getRoomName()));
			} else if (req instanceof SendReq) {
				SendReq sendReq = (SendReq) req;
				logResult(session.say(sendReq.getText()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			session.logout();
			throw e;
		}
	}

	static void logResult(String line) {
		if (line != null)
			System.out.println(line);
	}

}