package chat.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import chat.MessageTypes;
import chat.MessageTypes.CreateRoomReq;
import chat.MessageTypes.JoinRoomReq;
import chat.MessageTypes.LoginReq;
import chat.MessageTypes.SendReq;
import chat.server.ChatServer.ChatSession;

public class InetChatSession extends ChatSession implements Runnable {

	static void logResult(String line) {
		if (line != null)
			System.out.println(line);
	}

	private final Socket clientSocket;
	private ObjectInputStream in;
	private ObjectOutputStream out;

	public InetChatSession(ChatServer server, Socket clientConnection) throws IOException {
		super(server);
		this.clientSocket = clientConnection;
		out = new ObjectOutputStream(clientSocket.getOutputStream());
		in = new ObjectInputStream(clientSocket.getInputStream());
	}

	public void processCommands(ObjectInputStream in) throws IOException, ClassNotFoundException {
		while (true) {
			Object req = in.readObject();
			if (req instanceof CreateRoomReq) {
				CreateRoomReq createRoomReq = (CreateRoomReq) req;
				logResult(createRoom(createRoomReq.getRoomName()));
			} else if (req instanceof JoinRoomReq) {
				JoinRoomReq joinRoomReq = (JoinRoomReq) req;
				logResult(join(joinRoomReq.getRoomName()));
			} else if (req instanceof SendReq) {
				SendReq sendReq = (SendReq) req;
				logResult(say(sendReq.getText()));
			}
		}
	}

	public void run() {
		try {
			if (!login(in))
				return;
			processCommands(in);
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			logout();
			try {
				clientSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public synchronized void sendLineToClient(String user, String text) {
		try {
			out.writeObject(new MessageTypes.NewLineNotification(user, text));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean login(ObjectInputStream in) throws IOException, ClassNotFoundException {
		LoginReq req = (LoginReq) in.readObject();
		String user = req.getUser();
		System.out.println("got message login " + user);
		String loginResult = login(user);
		logResult(loginResult);
		return loginResult == null;
	}

}