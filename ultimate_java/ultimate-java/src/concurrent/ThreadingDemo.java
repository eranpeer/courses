package concurrent;

public class ThreadingDemo {

	private boolean initiated = false;
	private long val1 = 0;
	private long val2 = 0;

	void run() throws InterruptedException {

		Thread t1 = new Thread(new Runnable() {

			@Override
			public void run() {
				waitUntilInitiated();
				use(val1, val2);
			}

			private void waitUntilInitiated() {
				synchronized (ThreadingDemo.this) {
					try {
						while (!initiated)
							ThreadingDemo.this.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}

			private void use(long val1, long val2) {
				System.out.println(val1 + "," + val2);
			}
		});

		long start = System.currentTimeMillis();
		t1.start();

		val1 = 1;
		val2 = 2;

		synchronized (this) {
			initiated = true;
			this.notify();
		}

		t1.join();
		long end = System.currentTimeMillis();
		System.out.println(end - start);
	}

	public static void main(String[] args) throws InterruptedException {
		new ThreadingDemo().run();
	}
}