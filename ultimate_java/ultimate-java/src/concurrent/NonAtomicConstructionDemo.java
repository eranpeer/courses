package concurrent;

import org.junit.Test;

/**
 * It looks like on this JRE Object construction is completed before the assignment.
 * 
 * @author Eran
 */
public class NonAtomicConstructionDemo {

	private static class SomeObject {
		long num;

		public SomeObject() {
			for (int i = 0; i <= 1000000; i++) {
				num = i;
				Thread.yield();
			}
		}
	}

	private SomeObject obj = null;

	private Runnable r1 = new Runnable() {

		@Override
		public void run() {
			for (;;) {
				obj = new SomeObject();
			}
		}
	};

	private Runnable r2 = new Runnable() {

		@Override
		public void run() {
			for (;;) {
				try {
					long a = obj.num;
					if (a != 1000000)
						System.out.println(a);
					obj = null;
				} catch (NullPointerException e) {

				}
			}
		}
	};

	@Test
	public void runTest() throws InterruptedException {
		Thread t1 = new Thread(r1);
		Thread t2 = new Thread(r2);
		t1.start();
		t2.start();

		t1.join();
		t2.join();
	}
}