package concurrent;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

import org.junit.Test;

public class ThreadLocalDemo {

	public static class ThreadLocalDateFormat {
		private static final ThreadLocal<DateFormat> dateFormatHolder = new ThreadLocal<DateFormat>() {
			protected DateFormat initialValue() {
				return new SimpleDateFormat("yyyy-MM-dd");
			}
		};

		public static DateFormat getDateFormat() {
			return dateFormatHolder.get();
		}
	}

	// try use this one!! will it work?
	public static class ThreadLocalDateFormat2 {
		private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		public static DateFormat getDateFormat() {
			return dateFormat;
		}
	}

	@Test
	public void test() throws InterruptedException {
		final CyclicBarrier cb = new CyclicBarrier(10);
		final CountDownLatch countDownLatch = new CountDownLatch(cb.getParties());
		for (int i = 0; i < cb.getParties(); i++) {
			new Thread(new Runnable() {

				@Override
				public void run() {
					waitForAll(cb);
					try {
						Date now = new Date();
						for (int j = 0; j < 100; j++) {
							DateFormat dateFormat = ThreadLocalDateFormat.getDateFormat();
							System.out.println(dateFormat.parse(dateFormat.format(now)));
						}
					} catch (ParseException e) {
						e.printStackTrace();
					} finally {
						countDownLatch.countDown();
					}
				}

				public void waitForAll(final CyclicBarrier cb) {
					try {
						cb.await();
					} catch (InterruptedException | BrokenBarrierException e) {
						throw new RuntimeException(e);
					}
				}
			}).start();
		}
		countDownLatch.await();
	}
}
