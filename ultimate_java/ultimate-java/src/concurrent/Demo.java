package concurrent;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

public class Demo {

	/**
	 * 
	 * Pi = 4/1 - 4/3 + 4/5 - 4/7 + 4/9 - 4/11 + 4/13 - ... the formula for one element is: 4.0 * (1
	 * - (i % 2) * 2) / (2 * i + 1) where i is the item's index (0,1,2, ...)
	 */
	private static double calculatePiForSection(long sectionFirst, long sectionLast) {
		double acc = 0.0;
		for (long i = sectionFirst; i <= sectionLast; i++) {
			acc += (4.0 * (1 - (i % 2) * 2)) / (2L * i + 1);
		}
		return acc;
	}

	final static int numOfThreads = 5;
	final static int sectionSize = 100000000 * 2;
	private static CyclicBarrier allAreReady = new CyclicBarrier(numOfThreads, new Runnable() {
		
		@Override
		public void run() {
			System.err.println("starting");
		}
	});

	
	private static CountDownLatch allAreDone = new CountDownLatch(numOfThreads);
	
	private static double pi;
	private static int numOfDoneThreads;

	public static void main(String[] args) throws InterruptedException {
		long start = System.currentTimeMillis();


		for (int i = 0; i < numOfThreads; i++) {
			final long startSection = i * sectionSize;
			final long endSection = startSection + sectionSize;
			new Thread(new Runnable() {

				@Override
				public void run() {
					waitForAll();
					double tmp = calculatePiForSection(startSection, endSection);
					synchronized (Demo.class) {
						pi += tmp;
						notifyDone();
					}
				}

				private void waitForAll() {
					try {
						allAreReady.await();
					} catch (InterruptedException e) {
					} catch (BrokenBarrierException e) {
					}
				}

				private void notifyDone() {
					allAreDone.countDown();
				}
			}).start();
		}

		waitForAllCompleted(numOfThreads);
		System.out.println(pi + ":" + (System.currentTimeMillis() - start));
	}

	public static void waitForAllCompleted(final int numOfThread) throws InterruptedException {
		allAreDone.await();
	}
}
