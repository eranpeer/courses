package concurrent;

import java.util.ArrayList;
import java.util.List;

public class GameOfLife {

	private final Cell[][] cells;

	public GameOfLife(boolean[][] initialState) {
		cells = new Cell[initialState.length][initialState[0].length];
		createCells(initialState);
	}

	private void animate() {
		printBoard();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
	}

	public void run() {
		for (int i = 0; i < cells.length; i++) {
			for (int j = 0; j < cells[i].length; j++) {
				cells[i][j].run();
			}
		}
	}

	private void createCells(boolean[][] initialState) {
		for (int i = 0; i < initialState.length; i++) {
			for (int j = 0; j < initialState[i].length; j++) {
				cells[i][j] = new Cell(i, j, initialState[i][j]);
			}
		}
	}

	private class Cell {

		private volatile boolean alive;
		private final int row;
		private final int col;

		public Cell(int row, int col, boolean initialState) {
			this.row = row;
			this.col = col;
			this.alive = initialState;
		}

		public boolean isAlive() {
			return alive;
		}

		public void run() {
			new Thread(new Runnable() {

				@Override
				public void run() {

				}

			}).start();
		}
		
		private boolean clacNextState(List<Cell> neighbours) {
			int aliveNeighbours = numOfLivingNeighbours(neighbours);
			return calcNextState(aliveNeighbours);
		}

		private boolean calcNextState(int aliveNeighbours) {
			if (isAlive() && aliveNeighbours > 3)
				return false;
			if (isAlive() && aliveNeighbours < 2)
				return false;
			if (isAlive() && (aliveNeighbours == 2 || aliveNeighbours == 3))
				return true;
			if (!isAlive() && aliveNeighbours == 3)
				return true;
			return false;
		}

		private int numOfLivingNeighbours(List<Cell> neighbours) {
			int aliveNeighbours = 0;
			for (Cell neighbour : neighbours) {
				if (neighbour.isAlive())
					aliveNeighbours++;
			}
			return aliveNeighbours;
		}

	}

	
	private List<Cell> getNeighbours(int row, int col) {
		List<Cell> neighbours = new ArrayList<>();
		tryAdd(neighbours, row - 1, col - 1);
		tryAdd(neighbours, row - 1, col);
		tryAdd(neighbours, row - 1, col + 1);
		tryAdd(neighbours, row, col - 1);
		tryAdd(neighbours, row, col + 1);
		tryAdd(neighbours, row + 1, col - 1);
		tryAdd(neighbours, row + 1, col);
		tryAdd(neighbours, row + 1, col + 1);
		return neighbours;
	}

	private void tryAdd(List<Cell> neighbours, int row, int col) {
		try {
			neighbours.add(cells[row][col]);
		} catch (ArrayIndexOutOfBoundsException e) {
		}
	}

	private void printBoard() {
		for (int i = 0; i < cells.length; i++) {
			for (int j = 0; j < cells[i].length; j++) {
				System.out.print(cells[i][j].isAlive() ? "*" : "-");
			}
			System.out.println();
		}
		System.out.println();
	}

	public static void main(String[] args) {
		GameOfLife g = new GameOfLife(new boolean[][] { //
				{ false, false, false, false, false },//
						{ false, false, true, false, false }, //
						{ false, false, true, false, false }, //
						{ false, false, true, false, false },//
						{ false, false, false, false, false } //
				});
		g.run();
	}
}
