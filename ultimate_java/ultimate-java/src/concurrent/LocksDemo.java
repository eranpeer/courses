package concurrent;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.junit.Test;

public class LocksDemo {
	
	
	public static class BoundedStack {
		private Object[] array;
		private int count;

		
		private Lock lock = new ReentrantLock(false);
//		private Condition notFull = lock.newCondition();
//		private Condition notEmpty = lock.newCondition();


		public BoundedStack(int capacity) {
			array = new Object[capacity];
		}

		public void push(Object o){
			synchronized (array) {
				try {
					while (count == array.length){
						array.wait();
					}
					array[count] = o;
					count ++;
					array.notify();
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
			}
		}


		public Object pop(){
			synchronized (array) {
				try {
					while (count == 0){
						array.wait();
					}
					count--;
					Object o = array[count];
					array.notify();
					return o;
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
			}
		}
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static class SyncBlockingBoundedStack {

		private final Object[] array;
		private int count = 0;

		public SyncBlockingBoundedStack(int capacity) {
			array = new Object[capacity];
		}

		public synchronized void push(Object o) throws InterruptedException {
			while (count == array.length)
				this.wait();
			array[count] = o;
			count++;
			notifyAll();
		}

		public synchronized Object pop() throws InterruptedException {
			while (count == 0)
				this.wait();
			count--;
			Object o = array[count];

			// note that just one notify() is not enough we must use notifyAll().
			// Otherwise we may wake-up a thread waiting for pop not for push.
			notifyAll();
			return o;
		}
	}

	public static class LocksBlockingBoundedStack {

		private final Object[] array;
		private int count = 0;

		private Lock lock = new ReentrantLock(false);
		private Condition notFull = lock.newCondition();
		private Condition notEmpty = lock.newCondition();

		public LocksBlockingBoundedStack(int capacity) {
			array = new Object[capacity];
		}

		public void push(Object o) throws InterruptedException {
			lock.lock();
			try {
				while (count == array.length)
					notFull.await();
				array[count] = o;
				count++;
				// no need to use signalAll()
				notEmpty.signal();
			} finally {
				lock.unlock();
			}
		}

		public Object pop() throws InterruptedException {
			lock.lock();
			try {
				while (count == 0)
					notEmpty.awaitUninterruptibly();
				count--;
				Object o = array[count];

				// no need to use signalAll() now.
				// we can only wake-up a waiting to push thread.
				notFull.signal();
				return o;
			} finally {
				lock.unlock();
			}
		}
	}

	public static class BoundedStacka {

		private Object[] array;

		public void BoundedStacka(int capacity) {
			array = new Object[capacity];
		}

		public void push(Object o) throws InterruptedException {
		}

		public Object pop() throws InterruptedException {
			return null;
		}
	}

	@Test
	public void test() throws Exception {
		final SyncBlockingBoundedStack stack = new SyncBlockingBoundedStack(1);
		Thread writer = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					for (int i = 0; i < 1000000; i++) {
						stack.push(i);
						System.out.println("pushing " + i);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		Thread reader = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					for (int i = 0; i < 1000000; i++) {
						Object num = stack.pop();
						System.out.println("poping " + num);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});

		reader.start();
		writer.start();

		reader.join();
		writer.join();
	}

}