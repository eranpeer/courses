package concurrent;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

import org.junit.Test;

public class ForkJoinDemo {

	@SuppressWarnings("serial")
	private static final class SectionTask extends RecursiveTask<Double> {
		private long sectionFirst;
		private long sectionLast;

		public SectionTask(long sectionFist, long sectionLast) {
			this.sectionFirst = sectionFist;
			this.sectionLast = sectionLast;
		}

		@Override
		protected Double compute() {
			long sectionSize = sectionLast - sectionFirst;
			if (sectionSize <= 10000) {
				return calculatePiForSection(sectionFirst, sectionLast);
			}
			long middle = (sectionLast + sectionFirst) / 2;
			SectionTask left = new SectionTask(sectionFirst, middle);
			SectionTask right = new SectionTask(middle + 1, sectionLast);
			left.fork();
			return right.compute() + left.join();
		}
	}

	/**
	 * 
	 * Pi = 4/1 - 4/3 + 4/5 - 4/7 + 4/9 - 4/11 + 4/13 - ... 
	 * the formula for one element is: 4.0 * (1	 - (i % 2) * 2) / (2 * i + 1) where i is the item's index (0,1,2, ...)
	 */
	private static double calculatePiForSection(long sectionFirst, long sectionLast) {
		double acc = 0.0;
		for (long i = sectionFirst; i <= sectionLast; i++) {
			acc += (4.0 * (1 - (i % 2) * 2)) / (2L * i + 1);
		}
		return acc;
	}

	@Test
	public void calcPi() throws Exception {
		ForkJoinPool pool = new ForkJoinPool();
		long start = System.currentTimeMillis();
		double pi = pool.invoke(new SectionTask(0, 10000 * 10000L));
		System.out.println("time: " + (System.currentTimeMillis() - start) + "ms");
		System.out.println(pi);
	}

}