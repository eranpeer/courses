package concurrent;

public class HappensBefore {

	public static class Example1 {

		private int var1;
		private int var2;

		private boolean initiated = false;

		public void example() {
			Thread t = new Thread(new Runnable() {

				@Override
				public void run() {
					while (!initiated)
						// remove the comment and see what happens
						// Thread.yield()
						;
					use(var1, var2);
				}

				private void use(int var1, int var2) {
					if (var1 != 1 || var2 != 2)
						System.out.println(var1 + "" + var2);
					System.out.println("Exited var1:" + var1 + ", var2:" + var2);
				}
			});
			t.start();

			try {
				Thread.sleep(100);
				var1 = 1;
				var2 = 2;
				initiated = true;
			} catch (InterruptedException e) {
			}
		}

		public static void main(String[] args) {
			for (int i = 0; i < 100; i++)
				new Example1().example();
		}
	}

	public static class Example2 {

		private int var1;
		private int var2;

		private boolean initiated = false;

		public void example() {
			Thread t = new Thread(new Runnable() {

				@Override
				public void run() {
					while (!initiated)
						// remove the comment and see what happens
						// synchronized (Example2.this) {}
						;
					use(var1, var2);
				}

				private void use(int var1, int var2) {
					if (var1 != 1 || var2 != 2)
						System.out.println(var1 + "" + var2);
					System.out.println("Exited var1:" + var1 + ", var2:" + var2);
				}
			});
			t.start();

			try {
				Thread.sleep(100);
				var1 = 1;
				var2 = 2;
				initiated = true;
				synchronized (Example2.this) {
				}
			} catch (InterruptedException e) {
			}
		}

		public static void main(String[] args) {
			for (int i = 0; i < 100; i++)
				new Example2().example();
		}
	}

	public static class Example3 {

		private int var1;
		private int var2;

		private boolean initiated = false;

		public void example() {
			Thread t = new Thread(new Runnable() {

				@Override
				public void run() {
					if (!initiated)
						throw new Error("can this error ever happen???");
					use(var1, var2);
				}

				private void use(int var1, int var2) {
					if (var1 != 1 || var2 != 2)
						System.out.println(var1 + "" + var2);
					System.out.println("Exited var1:" + var1 + ", var2:" + var2);
				}
			});
			
			var1 = 1;
			var2 = 2;
			initiated = true;

			t.start();
		}

		public static void main(String[] args) {
			for (int i = 0; i < 100; i++)
				new Example3().example();
		}
	}

}
