package concurrent;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

import org.junit.Test;

public class SemaphoreDemo {

	public static class RestrictedResource {

		private final Semaphore semaphore;

		public RestrictedResource(int permits) {
			semaphore = new Semaphore(permits);
		}

		public void use() {
			System.out.println("queue length: " + semaphore.getQueueLength());
		}

		public void acquire() throws InterruptedException {
			semaphore.acquire();
		}

		public void release() {
			semaphore.release();
		}
	}

	@Test
	public void test() {

		final int numOfThreads = 10;

		final RestrictedResource resource = new RestrictedResource(5);
		final CyclicBarrier cb = new CyclicBarrier(numOfThreads);
		
		for (int thread = 0; thread < numOfThreads; thread++) {
			new Thread(new Runnable() {

				@Override
				public void run() {
					syncWithAllThreads(cb);
					for (int j = 0; j < 10; j++) {
						useResource(resource);
					}
				}

				public void syncWithAllThreads(final CyclicBarrier cb) {
					try {
						cb.await();
					} catch (InterruptedException | BrokenBarrierException e) {
						throw new RuntimeException(e);
					}
				}

				public void useResource(final RestrictedResource resource) {
					try {
						resource.acquire();
					} catch (InterruptedException e) {
						return;
					}
					try {
						resource.use();
					} finally {
						resource.release();
					}
				}
			}).start();
		}
	}
}
