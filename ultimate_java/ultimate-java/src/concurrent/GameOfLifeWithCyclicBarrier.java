package concurrent;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class GameOfLifeWithCyclicBarrier {

	private final CyclicBarrier readyToStartGame;
	private final CyclicBarrier readyToChangeState;
	private final CyclicBarrier afterChangeState;

	private final Cell[][] cells;

	public GameOfLifeWithCyclicBarrier(boolean[][] initialState) {
		cells = new Cell[initialState.length][initialState[0].length];
		final int numOfCells = initialState.length * initialState[0].length;
		readyToStartGame = new CyclicBarrier(numOfCells, new Runnable() {

			@Override
			public void run() {
				animate();
			}
		});

		readyToChangeState = new CyclicBarrier(numOfCells, new Runnable() {

			@Override
			public void run() {
			}
		});

		afterChangeState = new CyclicBarrier(numOfCells, new Runnable() {
			@Override
			public void run() {
				animate();
			}
		});
		
		createCells(initialState);
	}

	private void animate() {
		printBoard();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
	}

	public void run() {
		for (int i = 0; i < cells.length; i++) {
			for (int j = 0; j < cells[i].length; j++) {
				cells[i][j].run();
			}
		}
	}

	private void createCells(boolean[][] initialState) {
		for (int i = 0; i < initialState.length; i++) {
			for (int j = 0; j < initialState[i].length; j++) {
				cells[i][j] = new Cell(i, j, initialState[i][j]);
			}
		}
	}

	private class Cell {

		private volatile boolean alive;
		private final int row;
		private final int col;

		public Cell(int row, int col, boolean initialState) {
			this.row = row;
			this.col = col;
			this.alive = initialState;
		}

		public boolean isAlive() {
			return alive;
		}

		public void run() {
			new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						List<Cell> neighbours = getNeighbours(row, col);
						readyToStartGame.await();

						while (true) {
							boolean newState = clacNextState(neighbours);
							readyToChangeState.await();
							Cell.this.alive = newState;
							afterChangeState.await();
						}
					} catch (InterruptedException | BrokenBarrierException e) {
						throw new Error(e);
					}
				}

				private boolean clacNextState(List<Cell> neighbours) {
					int aliveNeighbours = numOfLivingNeighbours(neighbours);
					return calcNextState(aliveNeighbours);
				}

				private boolean calcNextState(int aliveNeighbours) {
					if (isAlive() && aliveNeighbours > 3)
						return false;
					if (isAlive() && aliveNeighbours < 2)
						return false;
					if (isAlive() && (aliveNeighbours == 2 || aliveNeighbours == 3))
						return true;
					if (!isAlive() && aliveNeighbours == 3)
						return true;
					return false;
				}

				private int numOfLivingNeighbours(List<Cell> neighbours) {
					int aliveNeighbours = 0;
					for (Cell neighbour : neighbours) {
						if (neighbour.isAlive())
							aliveNeighbours++;
					}
					return aliveNeighbours;
				}
			}).start();

		}
	}

	private List<Cell> getNeighbours(int row, int col) {
		List<Cell> neighbours = new ArrayList<>();
		tryAdd(neighbours, row - 1, col - 1);
		tryAdd(neighbours, row - 1, col);
		tryAdd(neighbours, row - 1, col + 1);
		tryAdd(neighbours, row, col - 1);
		tryAdd(neighbours, row, col + 1);
		tryAdd(neighbours, row + 1, col - 1);
		tryAdd(neighbours, row + 1, col);
		tryAdd(neighbours, row + 1, col + 1);
		return neighbours;
	}

	private void tryAdd(List<Cell> neighbours, int row, int col) {
		try {
			neighbours.add(cells[row][col]);
		} catch (ArrayIndexOutOfBoundsException e) {
		}
	}

	private void printBoard() {
		for (int i = 0; i < cells.length; i++) {
			for (int j = 0; j < cells[i].length; j++) {
				System.out.print(cells[i][j].isAlive() ? "*" : "-");
			}
			System.out.println();
		}
		System.out.println();
	}

	public static void main(String[] args) {
		GameOfLifeWithCyclicBarrier g = new GameOfLifeWithCyclicBarrier(new boolean[][] { //
				{ false, false, false, false, false },//
						{ false, false, true, false, false }, //
						{ false, false, true, false, false }, //
						{ false, false, true, false, false },//
						{ false, false, false, false, false } //
				});
		g.run();
	}
}
