package concurrent;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

public class ExecutorsDemo {

	/**
	 * 
	 * Pi = 4/1 - 4/3 + 4/5 - 4/7 + 4/9 - 4/11 + 4/13 - ...
	 *      the formula for one element is: 4.0 * (1 - (i % 2) * 2) / (2 * i + 1) 
	 *      where i is the item's index (0,1,2, ...)
	 */
	private static double calculatePiFor(long sectionIndex, long sectionSize) {
		long sectionFirst = sectionIndex * sectionSize;
		long sectionLast = (sectionIndex + 1) * sectionSize - 1;
		return calculatePiForSection(sectionFirst, sectionLast);
	}

	public static double calculatePiForSection(long sectionFirst, long sectionLast) {
		double acc = 0.0;
		for (long i = sectionFirst; i <= sectionLast; i++) {
			acc += (4.0 * (1 - (i % 2) * 2)) / (2L * i + 1);
		}
		return acc;
	}

	private double acc = 0.0;

	@Test
	public void calcPi() throws Exception {
//		final ExecutorService executor = Executors.newFixedThreadPool(1);
//		final ExecutorService executor = Executors.newFixedThreadPool(2);
//		final ExecutorService executor = Executors.newFixedThreadPool(4);
//		final ExecutorService executor = Executors.newFixedThreadPool(1000);
		final ExecutorService executor = Executors.newCachedThreadPool();
		long start = System.currentTimeMillis();
		for (int i = 0; i < 10000; i++) {
			final int sectionIndex = i;
			executor.submit(new Runnable() {
				@Override
				public void run() {
					double pi = calculatePiFor(sectionIndex, 10000);
					synchronized (executor) {
						acc += pi;						
					}
				}
			});
		}
		
		executor.shutdown();
		executor.awaitTermination(1, TimeUnit.DAYS);
		System.out.println("time: " + (System.currentTimeMillis() - start) + "ms");
		System.out.println(acc);
	}

}