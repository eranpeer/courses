package concurrent;

import org.junit.Test;

/**
 * Configure run configuration to use 32 bit JRE and check the results.
 * 
 * @author Eran
 */
public class NonAtomicLongAssignmentDemo {

	private long val = Long.MAX_VALUE;

	public long changeVal() {
		if (val == Long.MAX_VALUE)
			val = 0L;
		else
			val = Long.MAX_VALUE;
		return val;
	}

	private Runnable r = new Runnable() {

		@Override
		public void run() {
			for (;;) {
				long v = changeVal();
				if (v != 0L && v != Long.MAX_VALUE) {
					System.out.println(v);
				}
			}
		}
	};

	@Test
	public void runTest() throws InterruptedException {
		Thread t1 = new Thread(r);
		Thread t2 = new Thread(r);
		t1.start();
		t2.start();

		t1.join();
	}
}