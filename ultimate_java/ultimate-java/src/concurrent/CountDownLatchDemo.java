package concurrent;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

import org.junit.Test;

public class CountDownLatchDemo {

	/**
	 * 
	 * Pi = 4/1 - 4/3 + 4/5 - 4/7 + 4/9 - 4/11 + 4/13 - ... the formula for one element is: 4.0 * (1
	 * - (i % 2) * 2) / (2 * i + 1) where i is the item's index (0,1,2, ...)
	 */
	private static double calculatePiForSection(long sectionFirst, long sectionLast) {
		double acc = 0.0;
		for (long i = sectionFirst; i <= sectionLast; i++) {
			acc += (4.0 * (1 - (i % 2) * 2)) / (2L * i + 1);
		}
		return acc;
	}

	private double pi = 0;
	
	@Test
	public void test() throws InterruptedException {

		final int sectionSize = 100000;
		final int numOfThreads = 10;
		final CyclicBarrier cb = new CyclicBarrier(numOfThreads);
		final CountDownLatch countDownLatch = new CountDownLatch(numOfThreads);
		
		
		for (int thread = 0; thread < numOfThreads; thread++) {

			final int sectionIndex = thread;

			new Thread(new Runnable() {

				@Override
				public void run() {
					syncWithAllThreads(cb);
					int sectionFirst = sectionIndex * sectionSize;

					double sum = calculatePiForSection(sectionFirst, sectionFirst + sectionSize - 1);
					synchronized (CountDownLatchDemo.this) {
						pi += sum;
					}
					countDownLatch.countDown();
				}

				private void syncWithAllThreads(final CyclicBarrier cb) {
					try {
						cb.await();
					} catch (InterruptedException | BrokenBarrierException e) {
						throw new RuntimeException(e);
					}
				}


			}).start();
		}
		
		// try running without this line
		countDownLatch.await();
		System.out.println(pi);
	}
}