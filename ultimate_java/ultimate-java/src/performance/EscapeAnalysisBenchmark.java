package performance;

import java.util.ArrayList;

/**
 * Turn on with -XX:+DoEscapeAnalysis <br>
 * Turn off with -XX:-DoEscapeAnalysis
 */
public class EscapeAnalysisBenchmark {

	private static ArrayList<ToEscape> list = new ArrayList<>();
	private final static int count = 100000;

	public static class ToEscape {
		private static int counter;
		public long what;

		public ToEscape(long what) {
			this.what = what;
			if (counter++ == 100)
				System.out.println("I Exist");
		}
	}

	/**
	 * The esc obj does not "escapes" the scope of the method. The escape analysis will find out
	 * that the esc object can be optimized. Running this method with or without escape analysis
	 * will produce the different results.
	 */
	private final static void countNotEscaped(boolean save) {
		for (int j = 0; j < count; ++j) {
			ToEscape esc = new ToEscape(j);
			if (save) {
				list.add(null);
				list.remove(list.size()-1);
			}
		}
	}

	/**
	 * The esc obj "escapes" the scope of the method. The escape analysis will find out that the esc
	 * object cannot be optimized. Running this method with or without escape analysis will produce
	 * the same result.
	 */
	private final static void countEscaped(boolean save) {
//		ArrayList<Object> list = new ArrayList<>();
		for (int j = 0; j < count; ++j) {
			ToEscape esc = new ToEscape(j);
			if (save){
				list.add(esc);
			}
		}
	}

	
	/**
	 * try:<br>
	 * java -XX:+DoEscapeAnalysis performance_and_monitoring.EscapeAnalysisBenchmark <br>
	 * java -XX:-DoEscapeAnalysis performance_and_monitoring.EscapeAnalysisBenchmark
	 */
	public static void main(String[] args) throws InterruptedException {
		Thread.sleep(8000);
		long start = System.currentTimeMillis();
		for (int i = 0; i < 10000; ++i) {
			countEscaped(true);
//			countNotEscaped(true);
		}
		long end = System.currentTimeMillis();
		System.out.println(end - start);
	}

}