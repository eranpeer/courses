package performance;

import java.util.concurrent.atomic.AtomicLong;

import org.junit.Test;

/**
 * Try with:<br>
 * 1. long <br>
 * 2. synchronized 3. volatile long <br>
 * 4. AtomicLong <br>
 * 
 * Can you explain the results?
 */
public class SynchronizationBenchmark {

	static class Incrementor implements Runnable {
		private final int iterations;
		long num = 0;

		public Incrementor(int interations) {
			this.iterations = interations;
		}

		public void run() {
			for (long i = 0; i < iterations; i++) {
				num++;
			}
		}
	}

	static class SyncronizedIncrementor implements Runnable {
		private final int iterations;
		long num = 0;

		public SyncronizedIncrementor(int interations) {
			this.iterations = interations;
		}

		public void run() {
			for (long i = 0; i < iterations; i++) {
				synchronized (this) {
					num++;
				}
			}
		}
	}

	static class VolatileIncrementor implements Runnable {
		private final int iterations;
		volatile long num = 0;

		public VolatileIncrementor(int interations) {
			this.iterations = interations;
		}

		public void run() {
			for (long i = 0; i < iterations; i++) {
				num++;
			}
		}
	}

	static class AtomicIncrementor implements Runnable {
		private final int iterations;
		AtomicLong num = new AtomicLong(0);

		public AtomicIncrementor(int interations) {
			this.iterations = interations;
		}

		public void run() {
			for (long i = 0; i < iterations; i++) {
				num.incrementAndGet();
			}
		}
	}

	Incrementor incrementor = new Incrementor(100000000);
//	SyncronizedIncrementor incrementor = new SyncronizedIncrementor(100000000);
//	VolatileIncrementor incrementor = new VolatileIncrementor(100000000);
//	AtomicIncrementor incrementor = new AtomicIncrementor(100000000);

	Thread t1 = new Thread(incrementor);
	Thread t2 = new Thread(incrementor);

	@Test
	public void benchmark() throws InterruptedException {
		long start = System.currentTimeMillis();
		
		t1.start();
		t2.start();
		t1.join();
		t2.join();
		
		System.out.println("result: " + incrementor.num + "   duration: "
				+ (System.currentTimeMillis() - start));
	}
}
