package performance;

import org.junit.Test;

public class ExceptionsBenchmark {

	@Test
	public void benchmark() {
		int[] array = new int[1000];
		array[array.length - 1] = 1;

		long start = System.currentTimeMillis();
		long result = 0;
		for (int i = 1; i < 10000; i++) {
//			result += indexOf1(array, 1);
			result += indexOf2(array, 1);
		}
		System.out.println(System.currentTimeMillis() - start + " " + result);
	}

	private static int indexOf1(int[] array, int value) {
		return indexOfWithReturn(array, 0, value);
	}

	private static int indexOfWithReturn(int[] array, int startIndex, int value) {
		if (startIndex >= array.length)
			return -1;
		if (array[startIndex] == value)
			return startIndex;
		return indexOfWithReturn(array, startIndex + 1, value);
	}

	/**
	 * 
	 * try:
	 * 1. commenting the fillInStackTrace
	 * 2. -XX:-StackTraceInThrowable 
	 */
	private static class StopSearchException extends RuntimeException {
		private int index;

		public StopSearchException(int index) {
			this.index = index;
		}

		@Override
		public Throwable fillInStackTrace() {
			return this;
		}
	}

	private static int indexOf2(int[] array, int value) {
		try {
			indexOfWithException(array, 0, value);
			return -1;
		} catch (StopSearchException e) {
			return e.index;
		}
	}

	private static void indexOfWithException(int[] array, int startIndex, int value) {
		if (startIndex >= array.length) {
			throw new StopSearchException(-1);
		}
		if (array[startIndex] == value) {
			throw new StopSearchException(startIndex);
		}
		indexOfWithException(array, startIndex + 1, value);
	}

}