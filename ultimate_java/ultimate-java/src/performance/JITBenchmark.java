package performance;

public class JITBenchmark {

	private static final int SIZE = 10;

	/**
	 * try running with: 
	 * 	-server 
	 *  -client 
	 *  -XX:+PrintCompilation 
	 *  -XX:CompileThreshold=num
	 * 
	 * extract method on the inner loop and check the printed result!!!
	 */
	public static void main(String[] args) {
		int[] array = new int[SIZE];
		for (int i = 0; i < SIZE; i++) {
			array[i] = 1;
		}

		int result = 0;
		long startTime = System.currentTimeMillis();
		for (long count = 0; count < 1000 * 1000000; count++) {
			// =====
			result += sum(array);
			// =====
		}
		long endTime = System.currentTimeMillis();
		System.out.println("It took: " + (endTime - startTime) + " 	millis. " + result);
	}

	public static long sum(int[] array) {
		long num = 0;
		for (int index = 0; index < SIZE; index++) {
			num += array[index];
		}
		return num;
	}

	public static long sumArray(int[] array) {
		long num = sum(array);
		return num;
	}

}
