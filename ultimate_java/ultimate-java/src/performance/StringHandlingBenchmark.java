package performance;

public class StringHandlingBenchmark {

	public static void main(String[] args) {
//		long start = System.currentTimeMillis();
//		buildString1();
//		System.out.println("using String +=: " + (System.currentTimeMillis() - start));

//		long start = System.currentTimeMillis();
//		buildString2();
//		System.out.println("implemet concat with string builder: " + (System.currentTimeMillis() - start));
//
//		long start = System.currentTimeMillis();
//		buildString3();
//		System.out.println("using String.concat: " + (System.currentTimeMillis() - start));

		long start = System.currentTimeMillis();
		buildString4();
		System.out.println("using StringBuilder: " + (System.currentTimeMillis() - start));
	}

	private static String buildString1() {
		String s = "";
		for (int i = 0; i < 100000; i++) {
			s += i;
		}
		return s;
	}

	private static String buildString2() {
		String s = "";
		for (int i = 0; i < 100000; i++) {
			s = new StringBuilder(s).append(i).toString();
		}
		return s;
	}

//	private static String buildString0() {
//		String s = "";
//		for (int i = 0; i < 100000; i++) {
//			s = new StringBuilder().append(s).append(i).toString();
//		}
//		return s;
//	}

	private static String buildString3() {
		String s = "";
		for (int i = 0; i < 100000; i++) {
			s = s.concat(Integer.toString(i));
		}
		return s;
	}

	private static String buildString4() {
		StringBuilder s = new StringBuilder(1000);
		for (int i = 0; i < 100000; i++) {
			s.append(i);
		}
		return s.toString();
	}
}