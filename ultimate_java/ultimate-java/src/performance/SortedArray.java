package performance;

import java.util.Arrays;

import org.junit.Test;

public class SortedArray {

	@Test
	public void test() {
		run(false);
		run(false);
		run(false);
		
		run(true);
		run(true);
		run(true);
	}

	public void run(boolean sort) {
		byte[] array = new byte[100000000];
		for (int i = 0; i < array.length; i++) {
			array[i] = (byte) (Math.random() * 500);
		}
		long counter = 0;

		if (sort)
			Arrays.sort(array);

		long start = System.nanoTime();
		for (int j = 0; j < 10; j++) {
			for (int i = 0; i < array.length; i++) {
				if (array[i] > 100) {
					counter++;
				}
			}
		}
		long end = System.nanoTime();
		
		System.out.println("It took: " + (end - start) + " " + counter);
	}

}
