package performance;

import java.util.ArrayList;
import java.util.List;

public class VMModeBenchmark {

	private static final int SIZE = 100000;

	/**
	 * try running with:
	 * -server
	 * -client
	 * -XX:+PrintCompilation
	 * -XX:CompileThreshold=num
	 * for example:
	 * java -server -XX:CompileCommandFile=.hotspot_compiler performance.VMModeBenchmark
	 */
	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>(SIZE);
		for (int i = 0; i < SIZE; i++) {
			list.add(1);
		}
		int result = 0;
		long startTime = System.currentTimeMillis();
		for (long count = 0; count < 1000; count++) {
			long num = innerLoop(list);
			result += num;
		}
		long endTime = System.currentTimeMillis();
		System.out.println("It took: " + (endTime - startTime) + " 	millis. " + result);
	}

	public static long innerLoop(List<Integer> list) {
		long num = 0;
		for (int index = 0; index < SIZE; index++) {
			num += list.get(index);
		}
		return num;
	}

}
