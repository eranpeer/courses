package jni;

public class JniExample {

	static {
		System.loadLibrary("JniExample");
	}

	public void someJavaMethod() {
		//System.out.println("Some Java Method");
		throw new RuntimeException("Some exception");
	}

	public native void throwExceptionFromJni();

	public native void callJavaMethod();

	public native SomeAbstractClass instantiateAbstractClass();

	public native Bean createBean(String name, int age, long birthDate, char sex);

	public static native Bean getBean(); // always return the same bean

	public native Bean modifyBean(Bean bean, String name, int age);
	
	public native String prompt(String prompt);
	
}