package jni;

import java.util.Date;

import org.junit.Test;


public class JniTest {

	@Test
	public void jniCreateBean() {
		JniExample jniExample = new JniExample();
		Bean b = jniExample.createBean("Eran", 40, new Date(1970, 9, 8).getTime(), 'M');
		System.out.println(b);
	}

	@Test
	public void jniGetBean() {
		Bean b = JniExample.getBean();
		System.out.println(b);
		b.setName("Eran");
		b = JniExample.getBean();		
		System.out.println(b);
	}

	@Test
	public void jniTryInstantiateAbstractClass() {
		JniExample jniExample = new JniExample();
		SomeAbstractClass o = jniExample.instantiateAbstractClass();
		System.out.println(o);
	}

	@Test
	public void jniCallJavaMethodFromJni() {
		JniExample jniExample = new JniExample();
		jniExample.callJavaMethod();
	}

	@Test
	public void jniCallThrowExceptionFromJni() {
		JniExample jniExample = new JniExample();
		jniExample.throwExceptionFromJni();
	}

	//@Test
	public void jniCallPrompt() {
		JniExample jniExample = new JniExample();
		System.out.println(jniExample.prompt("Hi all"));
	}

}