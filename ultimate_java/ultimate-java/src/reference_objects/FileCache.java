package reference_objects;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;

public class FileCache {

	private ReferenceQueue<byte[]> collected = new ReferenceQueue<>();
	private HashMap<Path, FileSoftReference> map = new HashMap<>();

	public InputStream get(Path path) throws IOException {
		removeAllCollected();
		byte[] fileContent = null;
		SoftReference<byte[]> fileContentRef = map.get(path);
		if (fileContentRef != null)
			fileContent = fileContentRef.get();
		if (fileContent == null) {
			fileContent = loadFile(path);
			map.put(path, new FileSoftReference(path, fileContent, collected));
		}
		return new ByteArrayInputStream(fileContent);
	}

	private byte[] loadFile(Path path) throws IOException {
		try (InputStream in = Files.newInputStream(path, StandardOpenOption.READ)) {
			ByteArrayOutputStream out = new ByteArrayOutputStream(in.available());
			copy(in, out);
			return out.toByteArray();
		}
	}

	private void removeAllCollected() {
		FileSoftReference curr = (FileSoftReference) collected.poll();
		while (curr != null) {
			map.remove(curr.path);
			curr = (FileSoftReference) collected.poll();
		}
	}

	private static class FileSoftReference extends SoftReference<byte[]> {
		private Path path;

		public FileSoftReference(Path path, byte[] referent, ReferenceQueue<? super byte[]> q) {
			super(referent, q);
			this.path = path;
		}
	}

	private static void copy(InputStream from, OutputStream to) throws IOException {
		byte[] buff = new byte[1024];
		int count = from.read(buff);
		while (count != -1) {
			to.write(buff, 0, count);
			count = from.read(buff);
		}
	}
}