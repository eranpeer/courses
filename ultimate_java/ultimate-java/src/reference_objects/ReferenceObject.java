package reference_objects;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import fish.YellowFish;

public class ReferenceObject {

	private static void triggerOutOfMemoryError() {
		ArrayList<byte[]> v = new ArrayList<>();
		while (true) {
			v.add(new byte[(int) Runtime.getRuntime().freeMemory()]);
		}
	}

	@Test
	public void softReference() throws InterruptedException {
		YellowFish fish1 = new YellowFish();
		YellowFish fish2 = new YellowFish();
		fish2.myFriend = fish1;

		SoftReference<YellowFish> fish1Ref = new SoftReference<>(fish1);
		System.gc();
		Thread.sleep(1000);
		Assert.assertSame(fish1, fish1Ref.get());

		fish1 = null;
		System.gc();
		Thread.sleep(1000);
		Assert.assertNotNull("fish1 is a friend of fish2", fish1Ref.get());

		fish2.myFriend = null;
		System.gc();
		Thread.sleep(1000);
		Assert.assertNotNull("has no reason to collect fish1", fish1Ref.get());

		try {
			triggerOutOfMemoryError();
		} catch (OutOfMemoryError e) {
			Assert.assertNull("fish1Ref must be cleared at this point", fish1Ref.get());
		}
	}

	@Test
	public void weakReference() throws InterruptedException {
		final StringBuilder log = new StringBuilder();
		YellowFish fish1 = new YellowFish()
		{
			@Override
			protected void finalize() throws Throwable {
				log.append("fish1 was finalized");
			}
		}
		;
		YellowFish fish2 = new YellowFish();
		fish2.myFriend = fish1;

		ReferenceQueue<Object> q = new ReferenceQueue<>();
		WeakReference<YellowFish> fish1Ref = new WeakReference<>(fish1, q);

		System.gc();
		Thread.sleep(1000);
		Assert.assertSame(fish1, fish1Ref.get());

		fish1 = null;
		System.gc();
		Thread.sleep(1000);
		Assert.assertNotNull("fish1 is a friend of fish2", fish1Ref.get());

		// make fish1 weakly reachable
		fish2.myFriend = null;

		System.gc();
		Thread.sleep(1000);
		
		Assert.assertEquals("fish1 was finalized",log.toString());
		Assert.assertNull("fish1Ref must be cleared at this point", fish1Ref.get());
		Assert.assertSame("fish1Ref must be in the queue at this point", fish1Ref, q.poll());
	}

	@Test
	public void phantomReference() throws InterruptedException {
		final List<Object> l =new ArrayList<>();
		final StringBuilder log = new StringBuilder();
		YellowFish fish1 = new YellowFish() {
			@Override
			protected void finalize() throws Throwable {
				log.append("fish1 was finalized");
				l.add(this);
			}
		};

		YellowFish fish2 = new YellowFish();
		fish2.myFriend = fish1;

		ReferenceQueue<Object> q = new ReferenceQueue<>();

		PhantomReference<YellowFish> fish1Ref = new PhantomReference<>(fish1, q);
		System.gc();
		Thread.sleep(1000);
		Assert.assertNull(q.poll());

		fish1 = null;
		System.gc();
		Thread.sleep(1000);
		Assert.assertNull("fish1 is a friend of fish2", q.poll());

		fish2.myFriend = null;

		// This should trigger the finalize() of fish1.
		System.gc();
		Thread.sleep(1000);
		Assert.assertEquals("fish1 was finalized", log.toString());

		// it takes 2 GCs to reclaim the space of an object with finalize().
		// A PhantomReference will only be added to the queue after the second GC.
		// This is to make sure the object is not revoked.
		Assert.assertNull("the phantom reference object must be in the queue", q.poll());

		System.gc();
		Thread.sleep(1000);
		Assert.assertSame("the phantom reference object must be in the queue", fish1Ref, q.poll());

		// fish1 memory will not be reclaimed until the application explicitly calls clear() or
		// until the reference itself becomes unreachable.
		// It is the responsibility of the application to call clear().
		fish1Ref.clear();
		System.gc();
		Thread.sleep(1000);
		// At this point fish1 memory must have been reclaimed.
	}
}