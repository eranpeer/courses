package reference_objects;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.ArrayDeque;
import java.util.Deque;

public class Cache<T> {

	private ReferenceQueue<T> collected = new ReferenceQueue<>();
	private Deque<SoftReference<T>> buff = new ArrayDeque<>();

	protected T create() {
		return null;
	}

	public T get() {
		removeAllCollected();
		while (!buff.isEmpty()) {
			SoftReference<T> candidate = buff.removeFirst();
			T obj = candidate.get();
			if (obj != null)
				return obj;
		}
		return create();
	}

	private void removeAllCollected() {
		Reference<? extends T> curr = collected.poll();
		while (curr != null) {
			buff.remove(curr);
			curr = collected.poll();
		}
	}

	public void put(T obj) {
		buff.add(new SoftReference<T>(obj, collected));
	}
}