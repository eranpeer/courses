package reference_objects;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.ArrayList;

import org.junit.Test;

public class ReferenceObjectsDemo {

	private static void triggerOutOfMemoryError() {
		ArrayList<byte[]> v = new ArrayList<>();
		while (true) {
			v.add(new byte[(int) Runtime.getRuntime().freeMemory()]);
		}
	}

	@Test
	public void testSoftReference() throws InterruptedException {
		final ArrayList<Object> list = new ArrayList<>();
		Object o = new Object() {
			@Override
			protected void finalize() throws Throwable {
				list.add(this);
				System.out.println("finalize1");
				Thread.sleep(10000);
				System.out.println("finalize2");
			}
		};
		ReferenceQueue<? super Object> q = new ReferenceQueue<>();
		SoftReference<Object> oRef = new SoftReference<Object>(o, q){
			@Override
			public String toString() {
				return "ref object to "+ get();
			}
		};
		o = null;
		System.out.println(oRef.get());

		System.gc();
		Thread.sleep(100);
		System.out.println(oRef.get());
		System.out.println(q.poll());

		try {
			triggerOutOfMemoryError();
		} catch (OutOfMemoryError e) {
		}

		System.out.println(q.poll() + " was found in quque");

		try {
			triggerOutOfMemoryError();
		} catch (OutOfMemoryError e) {
		}
		
	}
}
