package performance_and_monitoring;

import java.util.ArrayList;

public class Demo {

	public static class MyException extends Exception {
		
		public final static MyException instance = new MyException();
		
		private MyException(){
		}
		
		@Override
		public synchronized Throwable fillInStackTrace() {
			return this;
		}		
	}
	
	
	static ArrayList<Object> list = new ArrayList<>();
	static Object o1 = "O1";
	static Object o2 = "O2";

	public static void main(String[] args) throws InterruptedException {
		Thread.sleep(10000);

		new Thread(new Runnable() {

			@Override
			public void run() {

				synchronized (o1) {
					synchronized (o2) {
						try {
							o2.wait();
						} catch (InterruptedException e) {
						}
					}
				}
			}
		}, "Thread A").start();
		new Thread(new Runnable() {
			@Override
			public void run() {
				synchronized (o2) {
					synchronized (o1) {
						try {
							o1.wait();
						} catch (InterruptedException e) {
						}
					}
				}
			}
		}, "Thread B").start();

		new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					try {
						Thread.sleep(1);
						list.add(new String("" + System.currentTimeMillis()));
					} catch (InterruptedException e) {
					}
				}
			}
		}, "Thread C").start();
	}

}
