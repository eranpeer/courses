package performance_and_monitoring;
/*
 * Try with and without. 
 * Since java 1.7 EscapeAnalysis is on by default. Use -XX:-DoEscapeAnalysis
 * to turn it off.
 */
public class AllocateObjects {

	public static void main(String[] args) throws InterruptedException {
		while (true){
			Thread.sleep(1);
			Object o = new Object();
			foo(o);
		}
	}

	private static long count;
	private static void foo(Object o) {
		count++;
	}
}
