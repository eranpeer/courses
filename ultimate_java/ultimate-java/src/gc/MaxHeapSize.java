package gc;

import java.util.ArrayList;

public class MaxHeapSize {

	// Question: After how many iterations will the program terminate ?
	// 1,2, more ?
	public static void main(String[] args) {
		ArrayList<byte[]> v = new ArrayList<>();
		while (true) {
			long size = Runtime.getRuntime().freeMemory();
			System.out.println("Total memory = " + Runtime.getRuntime().totalMemory() + ", free memory = " + size);
			byte[] buffer = new byte[(int) size];
			v.add(buffer);
		}
	}

}