package gc;

import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;

import org.junit.Assert;
import org.junit.Test;

public class Heap {

	/**
	 * -XX:+UseSerialGC
	 * 		Serial Copy collector for young gen + serial MarkSweepCompact collector for old gen.
	 * -XX:+UseParNewGC
	 * 		Copy Collector for Young Gen that parallelizes the copying collection over multiple threads +
	 * 		serial MarkSweepCompact collector for old gen.
	 * -XX:+UseParallelGC
	 * 		Parallel Scavenging Collector for Young Gen + 
	 * 		By default use Parallel Compacting Collector for old Gen. (JDK 6 and later)
	 * -XX:+UseParallelOldGC
	 * 		Parallel Compacting Collector for old Gen (on by default with ParallelGC in JDK 6 and later)
	 * 		will use the Parallel Scavenging Collector for Young Gen by default.
	 * -XX:+UseConcMarkSweepGC
	 * 		Concurrent Mark-Sweep (CMS) Collector for old gen.
	 * 		will use UseParNewGC for young gen by defualt.
	 * -XX:+UseConcMarkSweepGC -XX:+UseParNewGC
	 * 		Concurrent (old Gen) and Parallel (Young Gen) Collectors
	 * -XX:+UseG1GC
	 * 		The new G1 Collector as of Java SE 6 Update 14. Officially, supported since JDK 1.7 update 4.
	 * 		NOT the default GC yet.
	 *
	 * -XX:+PrintGC
	 * -XX:+PrintGCDetails
	 * -XX:+PrintGCTimeStamps
	 * -XX:+PrintHeapAtGC
	 * -XX:+PrintTenuringDistribution
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		printHeapData();
		printGCCounters();
		printGenerationalGCData();
	}

	public static void printHeapData() {
		System.out.println("current heap size:" + Runtime.getRuntime().totalMemory());
		System.out.println("max heap size:" + Runtime.getRuntime().maxMemory());
		System.out.println("free heap size:" + Runtime.getRuntime().freeMemory());
		System.out.println("used heap size:" + usedHeapSize());
	}

	public static void printGCCounters() {
		for (GarbageCollectorMXBean gcBean : ManagementFactory.getGarbageCollectorMXBeans()) {
			System.out.printf("Name: %s, Collection count: %s, Collection time: %s\n", gcBean.getName(), gcBean.getCollectionCount(), gcBean.getCollectionTime());
		}
	}

	public static void printGenerationalGCData() {
		for (MemoryPoolMXBean mpBean : ManagementFactory.getMemoryPoolMXBeans()) {
			System.out.printf("Name: %s, %s\n", mpBean.getName(), mpBean.getUsage());
		}
	}

	@Test
	public void usedPlusFreeMustEqualTotalHeapSize() {
		long totalHeapSize = Runtime.getRuntime().totalMemory();
		long freeHeapSize = Runtime.getRuntime().freeMemory();
		Assert.assertEquals("used + free must be total heap size", totalHeapSize, usedHeapSize() + freeHeapSize);
	}

	@Test
	public void trigerMinorGC() throws InterruptedException {
		Thread.sleep(1000 * 10);
		ArrayList<Object> list = new ArrayList<>();
		printHeapData();
		printGCCounters();
		long start = System.currentTimeMillis();
		for (int j = 1; j < 1000000; j++) {
			for (int i = 1; i < 100000; i++) {
				list.add(new byte[1]);
				list.clear();
				//printGenerationalGCData();
				//printGCCounters();
			}
		}
		System.out.println(System.currentTimeMillis() - start);
		
		printHeapData();
		printGCCounters();
	}

	@Test
	public void trigerMajorGC() throws InterruptedException {
//		Thread.sleep(8000);
		ArrayDeque<Object> list = new ArrayDeque<>();
		printHeapData();
		long start = System.currentTimeMillis();

		int countOutOfMemoryExceptions = 0;
		int countAllocations = 0;
		while (countOutOfMemoryExceptions < 3) {
			try {
				countAllocations ++;
				list.add(new byte[1]);
				list.add(new byte[1]);
				list.removeLast(); // To be collected by minor GC
			} catch (OutOfMemoryError e) {
				removeEverySecondElement(list);
				countOutOfMemoryExceptions++;
				System.out.println("countOutOfMemoryExceptions:" + countOutOfMemoryExceptions);
			} finally {
			}
		}
		System.out.println(System.currentTimeMillis() - start);
		System.out.println("countAllocations:" + countAllocations);
		printGenerationalGCData();
		printGCCounters();
		System.out.println("countOutOfMemoryExceptions:" + countOutOfMemoryExceptions);
	}

	public void removeEverySecondElement(Deque<Object> list) {
		int initialSize = list.size();
		while(list.size() > initialSize/2){
			list.removeFirst();
			list.removeLast();
		}
	}

	private static long usedHeapSize() {
		return Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
	}
}
