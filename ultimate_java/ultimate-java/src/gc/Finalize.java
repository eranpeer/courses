package gc;

public class Finalize {

	protected Object objectRef;	
	
	public Finalize() {
		objectRef = new TestClass(this);
	}

	protected void finalize() throws Throwable {
		System.out.println("Finalizing GarbageTest");
	}

	static class TestClass {

		Object testref;

		public TestClass(Object objref) {
			testref = objref;
		}

		protected void finalize() throws Throwable {
			System.out.println("Finalizing TestClass");
		}
	}

	
	
	
	
	// Finalize may not be invoked since gc may not be invoked. 
	public static class ItIsNotPromisedThatFinalizeWillBeInvoked{
		public static void main(String[] args) {
			Finalize gt = new Finalize();
			System.out.println("Exiting main()");
		}
	}

	// Finalize may not be invoked since gc may not be invoked.
	public static class ItIsNotPromisedThatFinalizeWillBeInvoked2{
		public static void main(String[] args) {
			Finalize gt = new Finalize();
			gt = null;
			System.out.println("Exiting main()");
		}
	}

	// since java 1.3 gc() MMUST collect ALL unreachable objects and run finalizers.
	public static class GcMustCollectAndRunTheFinalizers{
		public static void main(String[] args) {
			Finalize gt = new Finalize();
			gt = null;
			System.gc();
			System.out.println("Exiting main()");
		}
	}

}