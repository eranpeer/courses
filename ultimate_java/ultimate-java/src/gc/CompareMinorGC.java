package gc;

import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;

/**
 * -XX:+UseSerialGC
 * 		Serial Copy collector for young gen + serial MarkSweepCompact collector for old gen.
 * -XX:+UseParNewGC
 * 		Copy Collector for Young Gen that parallelizes the copying collection over multiple threads +
 * 		serial MarkSweepCompact collector for old gen.
 * -XX:+UseParallelGC
 * 		Parallel Scavenging Collector for Young Gen + 
 * 		By default use Parallel Compacting Collector for old Gen. (JDK 6 and later)
 * -XX:+UseG1GC
 * 		The new G1 Collector as of Java SE 6 Update 14. Officially, supported since JDK 1.7 update 4.
 * 		NOT the default GC yet.
 *
 * -XX:+PrintGC
 * -XX:+PrintGCDetails
 * -XX:+PrintGCTimeStamps
 * -XX:+PrintHeapAtGC
 * -XX:+PrintTenuringDistribution
 *
 * @param args
 */

public class CompareMinorGC {

	public static void printGCCounters() {
		for (GarbageCollectorMXBean gcBean : ManagementFactory.getGarbageCollectorMXBeans()) {
			System.out.printf("Name: %s, Collection count: %s, Collection time: %s\n", gcBean.getName(), gcBean.getCollectionCount(), gcBean.getCollectionTime());
		}
	}

	private static long num = 0;
	static ArrayList<Object> list = new ArrayList<>();
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		while(System.currentTimeMillis() - start < 10 * 1000){
			list.clear();
			Object o = new Object(){
				{num++;}
			};
			list.add(o);
		}
		System.err.println((System.currentTimeMillis() - start) + ":" + num);
		printGCCounters();
	}
}
