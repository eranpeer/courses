package proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class DynamicProxyDemo {

	public static interface Service {
		int foo();

		String bar();
	}

	@org.junit.Test
	public void dynamicProxy() {
		Service p = (Service) Proxy.newProxyInstance(Service.class.getClassLoader(), new Class<?>[] { Service.class },
				new InvocationHandler() {
					@Override
					public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
						if (method.getName().equals("bar"))
							return "ERAN";
						return 1;
					}
				});
		System.out.println(p.bar());
	}
}