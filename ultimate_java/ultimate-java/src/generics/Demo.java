package generics;

import org.junit.Test;

public class Demo {

	public static <T> T duplicate(T t){
		return null;
	}
	
	void g(){
		duplicate(new Integer(5));
	}
	
	public static class Box<T> {
				
		private T o;

		public void put(T o){
			this.o = o;
		}
		
		public T take() {
			T tmp = o;
			o = null;
			return tmp;
		}
		
		public void copyTo(Box<? super T> target){
			
		}
		
		public void copyFrom(Box<? extends T> source){
			
		}
	}
	
	
	
	@Test
	public void testBox(){
		Number n;
		Integer i = new Integer(5);
		n = i;
		Box<Number> nBox = null;
		Box<Object> oBox = new Box<>();
		Box<Integer> iBox = new Box<>();
		Box<Double> dBox = new Box<>();
		oBox.put("eran");
		iBox.copyTo(oBox);
		Box box = nBox;
//		oBox.copyTo(iBox);
//		foo (oBox);
//		bar (iBox);
		//		nBox = iBox;
//		sBox.put(1);
//		Box box = sBox;
//		box.put("eran");
//		String s = (String) box.take();
	}
}
