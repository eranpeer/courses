package generics;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicIntegerArray;

import org.junit.Test;

public class CollectionsDemo {

	@Test
	public void replaceAll() {
		List<Integer> intList = Arrays.asList(1, 2);
		List<Number> numberList = Arrays.<Number> asList(1, 2);
		Collections.fill(intList, 3);
		Collections.fill(numberList, 3);
		System.out.println(intList);
		System.out.println(numberList);
	}

	@Test
	public void sort() {
		List<Integer> intList1 = Arrays.asList(1, 3, 2);
		List<Integer> intList2 = Arrays.asList(1, 3, 2);
		Collections.sort(intList1);

		Collections.sort(intList2, new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return o1.intValue() - o2.intValue();
			}
		});

		Collections.sort(intList2, new Comparator<Number>() {
			@Override
			public int compare(Number o1, Number o2) {
				return o2.intValue() - o1.intValue();
			}
		});

		System.out.println(intList1);
		System.out.println(intList2);
	}

	@Test
	public void sortArray() {
		int[] intArray = new int[] { 1, 3, 2 };
		Arrays.sort(intArray);
		System.out.println(Arrays.toString(intArray)); // print array
	}

	@Test
	public void concurrent() {
		ConcurrentHashMap<Integer, String> map = new ConcurrentHashMap<>(16);
	}

	@Test
	public void copyOnWrite() {
		CopyOnWriteArrayList<Integer> list = new CopyOnWriteArrayList<>();
		list.add(1);
	}

	@Test
	public void atomicArray() {
		AtomicIntegerArray array = new AtomicIntegerArray(100);
		array.set(0, 1);
		int val = array.getAndAdd(0, 5);
		val = array.getAndAdd(0, 5);
	}

	@Test
	public void priorityQueue() {
		PriorityQueue<String> q1 = new PriorityQueue<>();
		q1.add("C");
		q1.add("A");
		q1.add("B");
		
		System.out.println(q1); // this is a little surprising
		// this will be ordered
		System.out.println(q1.remove());
		System.out.println(q1.remove());
		System.out.println(q1.remove());
		
		PriorityQueue<String> q2 = new PriorityQueue<>(10, new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				return o2.charAt(0) - o1.charAt(0);
			}
		});
		
		q2.add("B");
		q2.add("A");
		q2.add("C");
		System.out.println(q2);

		// this will be reverse ordered
		System.out.println(q2.remove());
		System.out.println(q2.remove());
		System.out.println(q2.remove());
	}
}