package generics;

import java.util.List;

import org.junit.Test;

public class GenericsDemo {

	public static class Box<T> {
		private T val;

		public void put(T val) {
			this.val = val;
		}

		public T peek() {
			return val;
		}

		public void copyTo(List<? super T> b) {
			b.add(val);
		}

		public void copyTo(Box<? super T> b) {
			b.put(val);
		}
	}


	@Test
	public void foo() {
		Box<Integer> intBox = new Box<>();
		Box<Number> numBox = new Box<>();
		Box<Object> objBox = new Box<>();
		Box<?> anyBox = new Box<>();
		intBox.put(1);
		intBox.copyTo(numBox);
		intBox.copyTo(objBox);
		// intBox.copyTo(anyBox);
	}
}
