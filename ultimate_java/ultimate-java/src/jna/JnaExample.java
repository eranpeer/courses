package jna;

import org.junit.Test;

import com.sun.jna.Library;
import com.sun.jna.Native;

public class JnaExample {

	public interface MathUtils extends Library {

		MathUtils INSTANCE = (MathUtils) Native.loadLibrary("MathUtils", MathUtils.class);

		int sum(int i1, int i2);

		int sumArray(int arr[], int count);
				
	}

	@Test
	public void tryJna() {
		int sumArray = MathUtils.INSTANCE.sumArray(new int[] { 1, 2 }, 2);
		System.out.println(sumArray);
	}
}