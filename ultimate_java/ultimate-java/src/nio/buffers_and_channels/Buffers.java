package nio.buffers_and_channels;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;

import org.junit.Assert;
import org.junit.Test;

public class Buffers {

	public void  allocatingBuffers(){
		ByteBuffer buff1 = ByteBuffer.wrap(new byte[10]);
		ByteBuffer buff2 = ByteBuffer.allocate(10);
		ByteBuffer buff3 = ByteBuffer.allocateDirect(10);

		CharBuffer cBuff1 = CharBuffer.wrap(new char[10]);
		CharBuffer cBuff2 = CharBuffer.allocate(10);
	}
	
	@Test
	public void  bufferUsage(){
		ByteBuffer buff = ByteBuffer.wrap(new byte[10]);
		buff.put((byte) 1);
		buff.put((byte) 2);
		Assert.assertEquals(2, buff.position());
		buff.flip();
		Assert.assertEquals(0, buff.position());
		Assert.assertEquals(1, buff.get());
		Assert.assertEquals(2, buff.get());
		Assert.assertEquals(2, buff.position());

		// if we flip again we can do the same read again.
		buff.flip();
		Assert.assertEquals(0, buff.position());
		Assert.assertEquals(1, buff.get());
		Assert.assertEquals(2, buff.get());

		// ignore data. set position to 0 and limit to capacity.
		buff.clear();
	}

	@Test
	public void  encoding_deoding() throws CharacterCodingException{
		Charset charset = Charset.forName("US-ASCII");
		CharsetDecoder decoder = charset.newDecoder();
		CharsetEncoder encoder = charset.newEncoder();

		ByteBuffer byteBuff = encoder.encode(CharBuffer.wrap("hello world"));
		// send(buff)
		
		CharBuffer charBuff = decoder.decode(byteBuff);
		Assert.assertEquals("hello world", charBuff.toString());
	}
}