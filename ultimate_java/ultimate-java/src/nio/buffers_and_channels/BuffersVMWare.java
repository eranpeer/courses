package nio.buffers_and_channels;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.ShortBuffer;

import junit.framework.Assert;

import org.junit.Test;

public class BuffersVMWare {

	@Test
	public void allocate(){
		 ByteBuffer buff = ByteBuffer.allocate(9);
		 ByteBuffer buff2 = ByteBuffer.wrap(new byte[10]);
//		 buff.put((byte)1);
//		 buff.putInt(5);
//		 Assert.assertEquals(5,buff.position());
//		 Assert.assertEquals(10,buff.limit());
		 ShortBuffer c = buff.asShortBuffer();
		 int p = c.limit();
		 buff.flip();
		 Assert.assertEquals(5,buff.limit());
		 CharBuffer cBuff = CharBuffer.allocate(10);
	}
}
