package nio;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import org.junit.Test;

public class CopyBenchmark {

	public static void oldIoCopy(InputStream from, OutputStream to) throws IOException {
		byte[] buff = new byte[1024 * 1024];
		int count = from.read(buff);
		while (count != -1) {
			to.write(buff, 0, count);
			count = from.read(buff);
		}
	}	
	
	public static void copy(ReadableByteChannel from, WritableByteChannel to, ByteBuffer buff) throws IOException {
		int count = from.read(buff);
		while (count != -1) {
			buff.flip();
			to.write(buff);
			buff.clear();
			count = from.read(buff);
		}
	}

	public static void main(String[] args) throws IOException {
		Path infilePath = Paths.get("bigfile.dat");
		Path outFilePath = Files.createTempFile("copy_of_bigfile", "dat");

		long start = System.currentTimeMillis();
//		copyWithStreams(infilePath, outFilePath);
//		copyWithChannels(infilePath, outFilePath);
//		copyWithChannelsUsingDirectBuffer(infilePath, outFilePath);
		fastCopy(infilePath, outFilePath);
		long end = System.currentTimeMillis();

		System.out.println(end - start);
	}

	public static void copyWithStreams(Path infilePath, Path outFilePath) throws IOException {
//		try (InputStream in = new FileInputStream(infilePath.toFile());
//				OutputStream out = new FileOutputStream(outFilePath.toFile(), false)) {
//			oldIoCopy(in, out);
//		}
		try (InputStream in = Files.newInputStream(infilePath);
				OutputStream out = Files.newOutputStream(outFilePath, StandardOpenOption.CREATE, StandardOpenOption.WRITE)) {
			oldIoCopy(in, out);
		}
	}

	public static void copyWithChannels(Path infilePath, Path outFilePath) throws IOException {
		try (SeekableByteChannel in = Files.newByteChannel(infilePath);
				SeekableByteChannel out = Files.newByteChannel(outFilePath, StandardOpenOption.CREATE, StandardOpenOption.WRITE)) {
			copy(in, out, ByteBuffer.allocate(1024 * 1024));
		}
	}

	public static void copyWithChannelsUsingDirectBuffer(Path infilePath, Path outFilePath) throws IOException {
		try (SeekableByteChannel in = Files.newByteChannel(infilePath);
				SeekableByteChannel out = Files.newByteChannel(outFilePath, StandardOpenOption.CREATE, StandardOpenOption.WRITE)) {
			copy(in, out, ByteBuffer.allocateDirect(1024 * 1024));
		}
	}

	public static void fastCopy(Path infilePath, Path outFilePath) throws IOException {
		try (SeekableByteChannel in = Files.newByteChannel(infilePath);
				FileChannel out = (FileChannel) Files.newByteChannel(outFilePath, StandardOpenOption.CREATE,
						StandardOpenOption.WRITE)) {
			out.transferFrom(in, 0, in.size());
		}
		System.out.println(outFilePath);
	}

}