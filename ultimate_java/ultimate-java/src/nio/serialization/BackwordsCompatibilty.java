package nio.serialization;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectInputStream.GetField;
import java.io.ObjectOutputStream;
import java.io.ObjectOutputStream.PutField;
import java.io.ObjectStreamField;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;


public class BackwordsCompatibilty {
//	private static final ObjectStreamField[] serialPersistentFields = new ObjectStreamField[]{
//	new ObjectStreamField("name", String.class),
//	new ObjectStreamField("age", String.class)
//};

	
//	public static class Person implements Serializable {
//		private static final long serialVersionUID = 1L;
//
//		public String name;
//		public int age;
//
//		public Person(String name, int age) {
//			this.name = name;
//			this.age = age;
//		}
//
//		@Override
//		public String toString() {
//			return "Person [name=" + name + ", age=" + age + "]";
//		}	
//	}

	public static class Person implements Serializable {
		private static final long serialVersionUID = 1L;

		
		public String firstName;
		public String lastName;
		public int age;

		public Person(String firstName, String lastName, int age) {
			this.firstName = firstName;
			this.lastName = lastName;
			this.age = age;
		}

		private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
			GetField fields = in.readFields();
			String fullname = (String) fields.get("name", "a b");
			String[] tokens = fullname.split(" ");
			firstName = tokens[0];
			lastName = tokens[1];
			age= fields.get("age", 0);
		}

//		private void writeObject(java.io.ObjectOutputStream out) throws IOException {
//			PutField fields = out.putFields();
//			fields.put("name", firstName + " " + lastName);
//			fields.put("age", age);
//			out.writeFields();
//		}
		
		@Override
		public String toString() {
			return "Person1 [firstName=" + firstName + ", lastName=" + lastName + ", age=" + age + "]";
		}
	}

	public static void main(String[] args) throws IOException, ClassNotFoundException {
//		Person obj = new Person("eran peer", 40);
//		writeToFile(obj);
		System.out.println(readFromFile());
	}

	public static void writeToFile(Person obj) throws IOException {
		OutputStream out = Files.newOutputStream(Paths.get("a.tmp"), StandardOpenOption.CREATE);
		ObjectOutputStream objOut = new ObjectOutputStream(out);
		objOut.writeObject(obj);
		objOut.close();
	}

	public static Person readFromFile() throws IOException, ClassNotFoundException {
		InputStream in = Files.newInputStream(Paths.get("a.tmp"));
		ObjectInputStream objIn = new ObjectInputStream(in);
		return (Person) objIn.readObject();
	}
}