package nio.serialization;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;

import junit.framework.Assert;

import org.junit.Test;

public class ReadReplaceAndWriteReplaceDemo {

	public static class Gender implements Serializable {

		public final static Gender MALE = new Gender("Male");
		public final static Gender FEMALE = new Gender("Female");

		private String name;

		private Gender(String name) {
			this.name = name;
		}

		Object writeReplace() throws ObjectStreamException {
			if (this.equals(MALE)) {
				return SerializedForm.MALE_FORM;
			} else {
				return SerializedForm.FEMALE_FORM;
			}
		}

		public String getName() {
			return name;
		}

		@SuppressWarnings("serial")
		private static class SerializedForm implements Serializable {

			final static SerializedForm MALE_FORM = new SerializedForm(0);
			final static SerializedForm FEMALE_FORM = new SerializedForm(1);

			private int value;

			SerializedForm(int value) {
				this.value = value;
			}

			Object readResolve() throws ObjectStreamException {
				if (value == MALE_FORM.value) {
					return Gender.MALE;
				} else {
					return Gender.FEMALE;
				}
			}
		}
	}

	private static Object serializeAndDeserialize(Object obj) throws IOException, ClassNotFoundException {
		ByteArrayOutputStream outBuff = new ByteArrayOutputStream();
		ObjectOutputStream out = new ObjectOutputStream(outBuff);
		out.writeObject(obj);
		out.close();

		ByteArrayInputStream inBuff = new ByteArrayInputStream(outBuff.toByteArray());
		ObjectInputStream in = new ObjectInputStream(inBuff);
		Object o = in.readObject();
		in.close();
		return o;
	}

	@Test
	public void serializeGender() throws ClassNotFoundException, IOException {
		Assert.assertSame(Gender.FEMALE, serializeAndDeserialize(Gender.FEMALE));
	}

}