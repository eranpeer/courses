package nio.serialization;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;

import junit.framework.Assert;

import org.junit.Test;

public class SerializationDemo {

	public static class Person implements Serializable {
		private static final long serialVersionUID = 1L;

		public String name;
		public int age;

		public Person(String name, int age) {
			this.name = name;
			this.age = age;
		}

		@Override
		public String toString() {
			return "Person [name=" + name + ", age=" + age + "]";
		}
		
	}

	public static class PersonWithTransientFields implements Serializable {
		private static final long serialVersionUID = 1L;

		private String name;
		private transient int age;

		public PersonWithTransientFields(String name, int age) {
			this.name = name;
			this.age = age;
		}

		@Override
		public String toString() {
			return "Person [name=" + name + ", age=" + age + "]";
		}
	}

	public static class PersonWithReadWriteMethods extends Person {

		private static final long serialVersionUID = 1L;

		private int height;

		public PersonWithReadWriteMethods(String name, int age, int height) {
			super(name, age);
			this.height = height;
		}

		private void writeObject(java.io.ObjectOutputStream out) throws IOException {
			out.writeInt(height);
			out.writeLong(System.currentTimeMillis());
		}

		private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
			height = in.readInt();
			@SuppressWarnings("unused")
			long writeTime = in.readLong();
		}
		
	}

	public static class ExternalizablePerson implements Externalizable {
		private static final long serialVersionUID = 1L;

		public String name;
		public int age;

		public ExternalizablePerson() {
		}

		public ExternalizablePerson(String name, int age) {
			this.name = name;
			this.age = age;
		}

		@Override
		public String toString() {
			return "Person [name=" + name + ", age=" + age + "]";
		}

		@Override
		public void writeExternal(ObjectOutput out) throws IOException {
			out.writeUTF(name);
			out.writeInt(age);
		}

		@Override
		public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
			name = in.readUTF();
			age = in.readInt();
		}
	}

	@Test
	public void simpleWriteAndRead() throws IOException, ClassNotFoundException {
		Person me = new Person("Eran", 40);
		Person myClone = (Person) serializeAndDeserialize(me);
		Assert.assertEquals("Eran", myClone.name);
		Assert.assertEquals(40, myClone.age);
	}

	@Test
	public void simpleWriteAndReadWithTransient() throws IOException, ClassNotFoundException {
		PersonWithTransientFields me = new PersonWithTransientFields("Eran", 40);
		PersonWithTransientFields myClone = (PersonWithTransientFields) serializeAndDeserialize(me);
		Assert.assertEquals("Eran", myClone.name);
		Assert.assertEquals(0, myClone.age); // Age in a transient field, it was not serialized.
	}

	@Test
	public void useWriteObjectAndReadObject() throws IOException, ClassNotFoundException {
		PersonWithReadWriteMethods me = new PersonWithReadWriteMethods("Eran", 40, 180);
		PersonWithReadWriteMethods myClone = (PersonWithReadWriteMethods) serializeAndDeserialize(me);
		Assert.assertEquals("Eran", myClone.name);
		Assert.assertEquals(40, myClone.age);
		Assert.assertEquals(180, myClone.height);
	}

	@Test
	public void readAndWriteExtenalizable() throws IOException, ClassNotFoundException {
		ExternalizablePerson me = new ExternalizablePerson("Eran", 40);
		ExternalizablePerson myClone = (ExternalizablePerson) serializeAndDeserialize(me);
		Assert.assertEquals("Eran", myClone.name);
		Assert.assertEquals(40, myClone.age);
	}

	private static Object serializeAndDeserialize(Object obj) throws IOException, ClassNotFoundException {
		ByteArrayOutputStream outBuff = new ByteArrayOutputStream();
		ObjectOutputStream out = new ObjectOutputStream(outBuff);
		out.writeObject(obj);
		out.close();

		ByteArrayInputStream inBuff = new ByteArrayInputStream(outBuff.toByteArray());
		ObjectInputStream in = new ObjectInputStream(inBuff);
		Object o = in.readObject();
		in.close();
		return o;
	}

}