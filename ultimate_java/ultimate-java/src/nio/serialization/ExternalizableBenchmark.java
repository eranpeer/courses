package nio.serialization;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.junit.Test;

public class ExternalizableBenchmark {

	public static class ExternalizablePerson implements Externalizable {

		private static final long serialVersionUID = 1L;
		public String name;
		public String street;
		public String city;
		public String country;
		public int zip;
		public int age;

		public ExternalizablePerson() {
		}

		public ExternalizablePerson(String name, String street, String city, String country, int zip, int age) {
			this.name = name;
			this.street = street;
			this.city = city;
			this.country = country;
			this.zip = zip;
			this.age = age;
		}

		@Override
		public void writeExternal(ObjectOutput out) throws IOException {
			out.writeUTF(name);
			out.writeUTF(street);
			out.writeUTF(city);
			out.writeUTF(country);
			out.writeInt(zip);
			out.writeInt(age);
		}

		@Override
		public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
			name = in.readUTF();
			street = in.readUTF();
			city = in.readUTF();
			country = in.readUTF();
			zip = in.readInt();
			age = in.readInt();
		}

	}

	public static class SerializablePerson implements Serializable {

		private static final long serialVersionUID = 1L;
		public String name;
		public String street;
		public String city;
		public String country;
		public int zip;
		public int age;

		public SerializablePerson() {
		}

		public SerializablePerson(String name, String street, String city, String country, int zip, int age) {
			this.name = name;
			this.street = street;
			this.city = city;
			this.country = country;
			this.zip = zip;
			this.age = age;
		}
	}

	@Test
	public void extenalizableBenchmark() throws IOException, ClassNotFoundException {
		long start = System.currentTimeMillis();
		serializeAndDeserialize(1000000);
		System.out.println(System.currentTimeMillis() - start);
	}

	private static void serializeAndDeserialize(int times) throws IOException, ClassNotFoundException {
		ByteArrayOutputStream outBuff = new ByteArrayOutputStream(1024 * 1024);
		ObjectOutputStream out = new ObjectOutputStream(outBuff);
		for (int i = 0; i < times; i++) {
//			ExternalizablePerson obj = new ExternalizablePerson("Eran" + i, "shazar" + i, "hod hasharon" + i, "israel" + i, i, 40);
//			SerializablePerson obj = new SerializablePerson("Eran" + i, "shazar" + i, "hod hasharon" + i, "israel" + i, i, 40);
//			ExternalizablePerson obj = new ExternalizablePerson("Eran", "shazar" , "hod hasharon" , "israel" , i, 40);
			SerializablePerson obj = new SerializablePerson("Eran" , "shazar" , "hod hasharon" + i, "israel" + i, i, 40);
			out.writeObject(obj);
		}
		out.close();

		ByteArrayInputStream inBuff = new ByteArrayInputStream(outBuff.toByteArray());
		ObjectInputStream in = new ObjectInputStream(inBuff);
		for (int i = 0; i < times; i++) {
			Object o = in.readObject();
		}
		in.close();
	}

}