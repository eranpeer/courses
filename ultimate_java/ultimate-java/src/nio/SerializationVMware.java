package nio;

import java.io.Externalizable;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectInputStream.GetField;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import org.junit.Test;

public class SerializationVMware {

	// public static class Person implements Serializable {
	//
	// private static final long serialVersionUID = 1L;
	//
	// private String name;
	// private int age;
	//
	// public Person() {
	// }
	//
	// public Person(String name, int age) {
	// super();
	// this.name = name;
	// this.age = age;
	// }
	//
	// public int getAge() {
	// return age;
	// }
	//
	// public String getName() {
	// return name;
	// }
	//
	// @Override
	// public String toString() {
	// return "Person [name=" + name + ", age=" + age + "]";
	// }
	//
	// // private void readObject(java.io.ObjectInputStream in) throws IOException,
	// // ClassNotFoundException {
	// // GetField fields = in.readFields();
	// // name = ((String) fields.get("name", "aaa")) + "sdfsa" ;
	// // }
	//
	// // private void writeObject(java.io.ObjectOutputStream out) throws IOException {
	// // PutField fields = out.putFields();
	// // fields.put("namea", this.name);
	// // fields.put("age", this.age);
	// // out.writeFields();
	// // }
	// }

	public static class SerializablePerson implements Serializable {

		private static final long serialVersionUID = 1L;

		private String firstName;
		private String lastName;
		private int age;

		public SerializablePerson(int age, String firstName, String lastName) {
			this.firstName = firstName;
			this.lastName = lastName;
			this.age = age;
		}

		public int getAge() {
			return age;
		}

		public String getFirstName() {
			return firstName;
		}

		public String getLastName() {
			return lastName;
		}

		@Override
		public String toString() {
			return "Person [firstName=" + firstName + ", lastName=" + lastName + ", age=" + age + "]";
		}

		private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
			GetField fields = in.readFields();
			int a = 0;
			String name = (String)fields.get("name", "old name");
			age = fields.get("age", -1);
			String[] tokens = name.split(" ");
			firstName = tokens [0];
			if (tokens.length > 1){
				lastName = tokens [1];
			}
			a++;
		}

		// private void writeObject(java.io.ObjectOutputStream out) throws IOException {
		// PutField fields = out.putFields();
		// fields.put("namea", this.name);
		// fields.put("age", this.age);
		// out.writeFields();
		// }
	}

	
	
	public static class ExternalizablePerson implements Externalizable {

		private static final long serialVersionUID = 1L;

		private String firstName;
		private String lastName;
		private int age;

		public ExternalizablePerson(int age, String firstName, String lastName) {
			this.firstName = firstName;
			this.lastName = lastName;
			this.age = age;
		}

		public int getAge() {
			return age;
		}

		public String getFirstName() {
			return firstName;
		}

		public String getLastName() {
			return lastName;
		}

		@Override
		public String toString() {
			return "Person [firstName=" + firstName + ", lastName=" + lastName + ", age=" + age + "]";
		}

		@Override
		public void writeExternal(ObjectOutput out) throws IOException {
			out.writeUTF(firstName);
			out.writeUTF(lastName);
			out.writeInt(age);
		}

		@Override
		public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
			firstName = in.readUTF();
			lastName = in.readUTF();
			age = in.readInt();
		}

	}

	@Test
	public void test() throws IOException, ClassNotFoundException {
		// Person eran = new Person("eran", 40);
//		Path path = Paths.get("a.tmp");
//		// writeToFile(eran, path);
//
//		Person p = readFromFile(path);
//		System.out.println(p);
	}

//	public void writeToFile(Person eran, Path path) throws IOException {
//		OutputStream out = Files.newOutputStream(path, StandardOpenOption.CREATE);
//		ObjectOutputStream oos = new ObjectOutputStream(out);
//		oos.writeObject(eran);
//		oos.flush();
//	}

	public Object readFromFile(Path path) throws IOException, ClassNotFoundException {
		InputStream in = Files.newInputStream(path);
		ObjectInputStream ois = new ObjectInputStream(in);
		SerializablePerson p = (SerializablePerson) ois.readObject();
		return p;
	}
}
