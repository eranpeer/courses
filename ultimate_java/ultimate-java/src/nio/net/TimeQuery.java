package nio.net;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.regex.Pattern;

public class TimeQuery {

	private static int port = 8013;

	private static Charset charset = Charset.forName("US-ASCII");
	private static CharsetDecoder decoder = charset.newDecoder();

	private static ByteBuffer dbuf = ByteBuffer.allocateDirect(1024);

	private static void query(String host) throws IOException {
		InetSocketAddress isa = new InetSocketAddress(InetAddress.getByName(host), port);
		try (SocketChannel sc = SocketChannel.open()) {
			sc.connect(isa);

			dbuf.clear();
			sc.read(dbuf);

			dbuf.flip();
			CharBuffer cb = decoder.decode(dbuf);
			System.out.print(isa + " : " + cb);
		}
	}

	public static void main(String[] args) {
		if (args.length < 1) {
			System.err.println("Usage: java TimeQuery [port] host...");
			return;
		}

		int firstArg = 0;
		if (Pattern.matches("[0-9]+", args[0])) {
			port = Integer.parseInt(args[0]);
			firstArg = 1;
		}

		for (int i = firstArg; i < args.length; i++) {
			String host = args[i];
			try {
				query(host);
			} catch (IOException x) {
				System.err.println(host + ": " + x);
			}
		}
	}

}