package nio.net;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.CharBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

public class SelectorTimeServer {

	private static final int DEFAULT_TIME_PORT = 8900;

	private static Charset charset = Charset.forName("US-ASCII");
	private static CharsetEncoder encoder = charset.newEncoder();

	public SelectorTimeServer() throws Exception {
		acceptConnections(DEFAULT_TIME_PORT);
	}

	public SelectorTimeServer(int... ports) throws Exception {
		acceptConnections(ports);
	}

	private static void acceptConnections(int... ports) throws Exception {
		Selector acceptSelector = SelectorProvider.provider().openSelector();

		for (int port : ports) {
			ServerSocketChannel severSocketChannel = ServerSocketChannel.open();
			severSocketChannel.configureBlocking(false);
			severSocketChannel.socket().bind(new InetSocketAddress(InetAddress.getLocalHost(), port));
			SelectionKey acceptKey = severSocketChannel.register(acceptSelector, SelectionKey.OP_ACCEPT);
		}

		while (acceptSelector.select() > 0) {

			// Someone is ready for I/O, get the ready keys
			Set<SelectionKey> readyKeys = acceptSelector.selectedKeys();
			Iterator<SelectionKey> i = readyKeys.iterator();

			// Walk through the ready keys collection and process date requests.
			while (i.hasNext()) {

				SelectionKey sk = (SelectionKey) i.next();
				i.remove();
				ServerSocketChannel nextReady = (ServerSocketChannel) sk.channel();

				try (SocketChannel sc = nextReady.accept()) {
					String now = new Date().toString();
					sc.write(encoder.encode(CharBuffer.wrap(now)));
					System.out.println("Sending " + sc.socket().getInetAddress() + " : " + now);
				}
			}
		}
	}

	public static void main(String[] args) {
		try {
			SelectorTimeServer nbt = new SelectorTimeServer(8900, 8901, 8902);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}