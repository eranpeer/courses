package chat.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.SocketChannel;

import chat.MessageTypes.CreateRoomReq;
import chat.MessageTypes.JoinRoomReq;
import chat.MessageTypes.LoginReq;
import chat.MessageTypes.NewLineNotification;
import chat.MessageTypes.SendReq;
import chat.util.BlockingCahnnel2InputStreamAdapter;
import chat.util.BlockingChannel2OutputStreamAdapter;
import chat.util.MessageUtils;

public class ChatSessionProxy {

	public static interface Listener {
		void onNewLine(String user, String text);
	}

	private class ServerNotificationsHandler implements Runnable {
		private InputStream inStream;

		public ServerNotificationsHandler(InputStream in) {
			inStream = in;
		}

		public void run() {
			try {
				while (true) {
					NewLineNotification req = (NewLineNotification) MessageUtils.readObject(inStream);
					listener.onNewLine(req.getUser(), req.getText());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private OutputStream out;
	private ChatSessionProxy.Listener listener;

	public ChatSessionProxy(SocketChannel serverSocket, ChatSessionProxy.Listener listener) throws IOException {
		this.listener = listener;
		out = new BlockingChannel2OutputStreamAdapter(serverSocket);
		new Thread(new ServerNotificationsHandler(new BlockingCahnnel2InputStreamAdapter(serverSocket))).start();
	}

	public void say(String text) throws IOException {
		out.write(MessageUtils.createMessage(new SendReq(text)));
		out.flush();
	}

	public void joinRoom(String room) throws IOException {
		out.write(MessageUtils.createMessage(new JoinRoomReq(room)));
		out.flush();
	}

	public void createRoom(String room, String description) throws IOException {
		out.write(MessageUtils.createMessage(new CreateRoomReq(room, description)));
		out.flush();
	}

	public void login(String user) throws IOException {
		out.write(MessageUtils.createMessage(new LoginReq(user)));
		out.flush();
	};

}