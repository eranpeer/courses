package chat.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import chat.MessageTypes;
import chat.server.ChatServer.ChatSession;

public class InetChatSession extends ChatSession implements Runnable {

	static void logResult(String line) {
		if (line != null)
			System.out.println(line);
	}

	private final Socket clientSocket;
	private DataInputStream in;
	private DataOutputStream out;

	public InetChatSession(ChatServer server, Socket clientConnection) throws IOException {
		super(server);
		this.clientSocket = clientConnection;
		in = new DataInputStream(clientSocket.getInputStream());
		out = new DataOutputStream(clientSocket.getOutputStream());
	}

	public void processCommands(DataInputStream in) throws IOException {
		while (true) {
			int messageType = in.readInt();
			switch (messageType) {
			case MessageTypes.CREATE_ROOM: {
				String roomName = in.readUTF();
				logResult(createRoom(roomName));
				break;
			}
			case MessageTypes.JOIN_ROOM: {
				String roomName = in.readUTF();
				logResult(join(roomName));
				break;
			}
			case MessageTypes.PUBLISH: {
				String text = in.readUTF();
				logResult(say(text));
				break;
			}
			default:
				break;
			}
		}
	}

	public void run() {
		try {
			if (!login(in))
				return;
			processCommands(in);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			logout();
			try {
				clientSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public synchronized void sendLineToClient(String user, String text) {
		try {
			out.writeInt(MessageTypes.NEW_LINE_NOTIFICATION);
			out.writeUTF(user);
			out.writeUTF(text);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean login(DataInputStream in) throws IOException {
		int messageType = in.readInt();
		if (messageType != MessageTypes.LOGIN) {
			return false;
		}
		String user = in.readUTF();
		System.out.println("got message login " + user);
		String loginResult = login(user);
		logResult(loginResult);
		return loginResult == null;
	}

}