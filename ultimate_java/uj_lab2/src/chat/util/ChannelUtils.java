package chat.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.WritableByteChannel;

public class ChannelUtils {
	public static InputStream newInputStream(final SocketChannel sc){
		return Channels.newInputStream(new ReadableByteChannel() {
			public int read(ByteBuffer dst) throws IOException {
				return sc.read(dst);
			}
			public void close() throws IOException {
				sc.close();
			}
			public boolean isOpen() {
				return sc.isOpen();
			}
		});
	}
	public static OutputStream newOutputStream(final SocketChannel sc){
		return Channels.newOutputStream(new WritableByteChannel() {
			
			public void close() throws IOException {
				sc.close();
			}
			public boolean isOpen() {
				return sc.isOpen();
			}
			@Override
			public int write(ByteBuffer src) throws IOException {
				return sc.write(src);
			}
		});
	}		
}