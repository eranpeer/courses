package chat;

import java.io.Serializable;

public interface MessageTypes {

	public static class LoginReq implements Serializable {
		
		private static final long serialVersionUID = 1L;

		private String user;

		public LoginReq(String user) {
			this.user = user;
		}

		public String getUser() {
			return user;
		}
	}

	public static class CreateRoomReq implements Serializable {
		
		private static final long serialVersionUID = 1L;
		private String roomName;

		public CreateRoomReq(String roomName) {
			this.roomName = roomName;
		}

		public String getRoomName() {
			return roomName;
		}
	}

	public static class JoinRoomReq implements Serializable {
		private static final long serialVersionUID = 1L;
		private String roomName;

		public JoinRoomReq(String roomName) {
			this.roomName = roomName;
		}

		public String getRoomName() {
			return roomName;
		}
	}

	public static class SendReq implements Serializable {
		private static final long serialVersionUID = 1L;
		private String text;

		public SendReq(String text) {
			this.text = text;
		}

		public String getText() {
			return text;
		}
	}

	public static class NewLineNotification implements Serializable {
		private static final long serialVersionUID = 1L;

		private String user;
		private String text;

		public NewLineNotification(String user, String text) {
			this.user = user;
			this.text = text;
		}

		public String getUser() {
			return user;
		}

		public String getText() {
			return text;
		}
	}

}