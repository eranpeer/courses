package chat.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import chat.MessageTypes;
import chat.MessageTypes.CreateRoomReq;
import chat.MessageTypes.LoginReq;
import chat.MessageTypes.NewLineNotification;
import chat.MessageTypes.SendReq;

public class ChatSessionProxy {

	public static interface Listener {
		void onNewLine(String user, String text);
	}

	private class ServerNotificationsHandler implements Runnable {
		private InputStream inStream;

		public ServerNotificationsHandler(InputStream in) {
			this.inStream = in;
		}

		public void run() {
			try {
				ObjectInputStream in = new ObjectInputStream(inStream);
				while (true) {
					NewLineNotification req = (NewLineNotification) in.readObject();
					listener.onNewLine(req.getUser(), req.getText());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private ObjectOutputStream out;
	private ChatSessionProxy.Listener listener;

	public ChatSessionProxy(Socket serverSocket, ChatSessionProxy.Listener listener) throws IOException {
		out = new ObjectOutputStream(serverSocket.getOutputStream());
		this.listener = listener;
		new Thread(new ServerNotificationsHandler(serverSocket.getInputStream())).start();
	}

	public void say(String text) throws IOException {
		out.writeObject(new SendReq(text));
	}

	public void joinRoom(String room) throws IOException {
		out.writeObject(new MessageTypes.JoinRoomReq(room));
	}

	public void createRoom(String room) throws IOException {
		out.writeObject(new CreateRoomReq(room));
	}

	public void login(String user) throws IOException {
		out.writeObject(new LoginReq(user));
	}

}