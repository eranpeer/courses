package bowling;

import thirdparty.StageDevice;

public class PlayerGame {

	private int rolls[] = new int[21];
	private int countRolls = 0;
	private String player;

	public PlayerGame(String player) {
		this.player = player;
	}

	public String getName() {
		return player;
	}

	private void roll(int pins) {
		rolls[countRolls] = pins;
		countRolls++;
	}

	public void printScore() {
		int score = 0;
		for (int i = 0; i < 20; i += 2) {
			if (rolls[i] == 10) {
				score += 10;
				score += rolls[i + 1] + rolls[i + 2];
				i--;
			} else {
				if (rolls[i] + rolls[i + 1] == 10) {
					score += 10;
					score += rolls[i + 2];
				} else
					score += rolls[i] + rolls[i + 1];
			}
		}
		Console.getInstance().writeLine(player + "'s score is " + score);
	}

	public int score() {
		int score = 0;
		for (int i = 0; i < 20; i += 2) {
			if (rolls[i] == 10) {
				score += 10;
				score += rolls[i + 1] + rolls[i + 2];
				i--;
			} else {
				if (rolls[i] + rolls[i + 1] == 10) {
					score += 10;
					score += rolls[i + 2];
				} else
					score += rolls[i] + rolls[i + 1];
			}
		}
		return score;
	}

	public void play(int frame) {
		int knockedDownThisFrame = 0;
		try {
			StageDevice.prepareForRoll(true);
			while (!StageDevice.isReadyForNewRoll()) {
				Thread.sleep(200);
			}
			Console.getInstance().writeLine("Press Enter to throw first roll!");
			while (!StageDevice.isRollDone()) {
				Thread.sleep(200);
			}
			int numOfFallingPins = StageDevice.readFallingPins();
			Console.getInstance().writeLine("You scored " + numOfFallingPins);
			roll(numOfFallingPins);

			knockedDownThisFrame += numOfFallingPins;
			if (knockedDownThisFrame < 10) {
				// not a strike, we have another roll
				StageDevice.prepareForRoll(false);
				while (!StageDevice.isReadyForNewRoll()) {
					Thread.sleep(200);
				}
				Console.getInstance().writeLine("Press Enter to throw second roll!");
				while (!StageDevice.isRollDone()) {
					Thread.sleep(200);
				}

				numOfFallingPins = StageDevice.readFallingPins();
				Console.getInstance().writeLine("You scored " + numOfFallingPins);
				roll(numOfFallingPins);

				knockedDownThisFrame += numOfFallingPins;
				if (knockedDownThisFrame == 10) {
					Console.getInstance().writeLine("Spare!!!");
				}

				if (knockedDownThisFrame == 10 && frame == 10) {
					// spare on last frame. Throw one more bonus roll.
					StageDevice.prepareForRoll(true);
					while (!StageDevice.isReadyForNewRoll()) {
						Thread.sleep(200);
					}
					Console.getInstance().writeLine("Press Enter to throw bonus roll!");
					while (!StageDevice.isRollDone()) {
						Thread.sleep(200);
					}

					numOfFallingPins = StageDevice.readFallingPins();
					Console.getInstance().writeLine("You scored " + numOfFallingPins);
					roll(numOfFallingPins);
				}
			} else {
				Console.getInstance().writeLine("Strike!!!");
				if (frame == 10) {
					// strike on last frame
					StageDevice.prepareForRoll(true);
					while (!StageDevice.isReadyForNewRoll()) {
						Thread.sleep(200);
					}
					Console.getInstance().writeLine("Press Enter to throw first bonus roll!");
					while (!StageDevice.isRollDone()) {
						Thread.sleep(200);
					}

					numOfFallingPins = StageDevice.readFallingPins();
					Console.getInstance().writeLine("You scored " + numOfFallingPins);
					roll(numOfFallingPins);

					if (numOfFallingPins == 10) {
						StageDevice.prepareForRoll(true);
					} else {
						StageDevice.prepareForRoll(false);
					}

					while (!StageDevice.isReadyForNewRoll()) {
						Thread.sleep(200);
					}
					Console.getInstance().writeLine("Press Enter to throw second bonus roll!");
					while (!StageDevice.isRollDone()) {
						Thread.sleep(200);
					}

					numOfFallingPins = StageDevice.readFallingPins();
					Console.getInstance().writeLine("You scored " + numOfFallingPins);
					roll(numOfFallingPins);
				}
			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}