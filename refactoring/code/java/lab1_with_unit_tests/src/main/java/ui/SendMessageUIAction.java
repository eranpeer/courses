package ui;

import domain.AddressBook;
import domain.Email;
import domain.Mailer;
import domain.Message;
import domain.Text;
import domain.Voice;

public class SendMessageUIAction {
	private AddressBook addressBook;
	private Console console;

	
	public SendMessageUIAction(Console console, AddressBook addressBook) {
		this.console = console;
		this.addressBook = addressBook;
	}


	public void sendMessage() {
		Message newMessage = null;
		String messageType = null;
	
		messageType = console.readLine("Enter type of message:");
		if (!messageType.equals("text") && !messageType.equals("email") && !messageType.equals("voice")) {
			console.show("invalid message type: " + messageType);
			return;
		}
	
		if (messageType.equals("email")) {
			newMessage = new Email();
			EmailEditor editor = new EmailEditor(addressBook);
			if (!editor.edit((Email) newMessage))
				return;
		}
	
		if (messageType.equals("text")) {
			newMessage = new Text();
			TextEditor editor = new TextEditor(addressBook);
			if (!editor.edit((Text) newMessage))
				return;
		}
	
		if (messageType.equals("voice")) {
			newMessage = new Voice();
			VoiceEditor editor = new VoiceEditor(addressBook);
			if (!editor.edit((Voice) newMessage))
				return;
		}
	
		new Mailer(addressBook).deliver(newMessage);
		console.show("New " + messageType + " for " + newMessage.getDestination() + "!");
	}
	
}