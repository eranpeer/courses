package ui;

import domain.AddressBook;
import domain.Mailbox;

public class Application {

	public Application() {
	}

	public void start() {

		Mailbox eranMbx = new Mailbox("eran");
		Mailbox ronenMbx = new Mailbox("dan");

		AddressBook addressBook = new AddressBook();

		addressBook.addMailbox(eranMbx);
		addressBook.addMailbox(ronenMbx);

		MessagingComponent messagingComponent = new MessagingComponent(
				addressBook);

		new Desktop(this, messagingComponent).start();
	}

	public void stop() {
		System.exit(0);
	}

	public static void main(String[] args) {
		new Application().start();
	}
}