package ui;
import domain.AddressBook;
import domain.Mailbox;

public class ListMailboxUIAction {
	private AddressBook addressBook;
	private final Console console;

	public ListMailboxUIAction(Console console, AddressBook addressBook) {
		super();
		this.console = console;
		this.addressBook = addressBook;
	}

	public void listMailbox() {
		String address = console.readLine("Enter mailbox address:");
		Mailbox mbx = addressBook.getMailbox(address.toLowerCase());
		if (mbx == null) {
			console.show("invalid mailbox address");
			return;
		}
		mbx.show(console);
	}
}