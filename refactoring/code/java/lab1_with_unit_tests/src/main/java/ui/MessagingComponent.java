package ui;

import domain.AddressBook;

public class MessagingComponent {

	private ListMailboxUIAction listMailboxUIAction;
	SendMessageUIAction sendMessageUIAction;
	DeleteMessageUIAction deleteMessageUIAction;

	private Console console = new Console();

	public MessagingComponent(AddressBook addressBook) {
		listMailboxUIAction = new ListMailboxUIAction(console, addressBook);
		sendMessageUIAction = new SendMessageUIAction(console, addressBook);
		deleteMessageUIAction = new DeleteMessageUIAction(console, addressBook);
	}

	public void listMailbox() {
		listMailboxUIAction.listMailbox();
	}

	public void sendMessage() {
		sendMessageUIAction.sendMessage();
	}

	void deleteMessage() {
		deleteMessageUIAction.deleteMessage();
	}

}