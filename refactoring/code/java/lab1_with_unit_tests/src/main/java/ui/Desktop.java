package ui;

public class Desktop {

	private MessagingComponent messagingComponent;

	private Application application;

	public Desktop(Application app, MessagingComponent messagingComponent) {
		this.application = app;
		this.messagingComponent = messagingComponent;
	}

	class SendMenuItem extends MenuItem {

		public SendMenuItem() {
			super("Send Message");
		}

		public void click() {
			messagingComponent.sendMessage();
		}
	}

	class ListMessagesMenuItem extends MenuItem {

		public ListMessagesMenuItem() {
			super("List Messages");
		}

		public void click() {
			messagingComponent.listMailbox();
		}
	}

	class DeleteMessageMenuItem extends MenuItem {

		public DeleteMessageMenuItem() {
			super("Delete Message");
		}

		public void click() {
			messagingComponent.deleteMessage();
		}
	}

	class ExitMenuItem extends MenuItem {

		public ExitMenuItem() {
			super("Exit");
		}

		public void click() {
			application.stop();
		}
	}

	public void start() {
		Menu mainMenu = new Menu(new Console(), "Select an option:");

		mainMenu.addItem(new SendMenuItem());
		mainMenu.addItem(new DeleteMessageMenuItem());
		mainMenu.addItem(new ListMessagesMenuItem());
		mainMenu.addItem(new ExitMenuItem());

		while (true) {
			mainMenu.show();
		}
	}

}