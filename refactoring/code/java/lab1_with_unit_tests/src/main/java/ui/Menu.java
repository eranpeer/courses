package ui;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Enumeration;
import java.util.Vector;

import org.junit.Test;

public class Menu {

	private final Console console;
	private Vector<MenuItem> items;
	private String name;

	public Menu(Console console, String name) {
		this.console = console;
		items = new Vector<MenuItem>();
		this.name = name;
	}

	public void addItem(MenuItem item) {
		items.addElement(item);
	}

	public void show() {
		console.show(name);
		Enumeration<MenuItem> e = items.elements();
		int i = 1;
		while (e.hasMoreElements()) {
			console.show(i + ". " + ((MenuItem) e.nextElement()).getName());
			i++;
		}
		try {
			String option = console.readLine("Select Option:");
			Integer intOption = new Integer(option);
			MenuItem item = (MenuItem) items.elementAt(intOption.intValue() - 1);
			item.click();
		} catch (Exception ex) {
			console.show("Invalid Option !!");
		}
	}

	public static class UnitTest {
		
		@Test
		public void showMenu() {
			Console console = mock(Console.class);
			MenuItem item =  mock(MenuItem.class);
			when(item.getName()).thenReturn("item1");
			Menu m = new Menu(console,"title");
			m.addItem(item);
			m.show();
			verify(console).show("title");
			verify(console).show("1. item1");
			verify(console).readLine("Select Option:");			
		}

		@Test
		public void invalidOptionSelection() {
			Console console = mock(Console.class);
			Menu m = new Menu(console,"title");
			when(console.readLine("Select Option:")).thenReturn("1");			
			m.show();
			verify(console).show("Invalid Option !!");			
		}

		@Test
		public void validOptionSelection() {
			Console console = mock(Console.class);
			MenuItem item =  mock(MenuItem.class);
			when(item.getName()).thenReturn("item1");
			Menu m = new Menu(console,"title");
			m.addItem(item);
			when(console.readLine("Select Option:")).thenReturn("1");			
			m.show();
			verify(item).click();			
		}
		
	}

}