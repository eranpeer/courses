package ui;

import domain.AddressBook;
import domain.Mailbox;
import domain.Message;

/**
 * 
 */

public class MessageEditingUtils {

	public static boolean editAddresses(Message msg, AddressBook addressBook,
			Console console) {
		String destinationAddress;
		String senderAddress;
		Mailbox destinationMailbox;
		senderAddress = console.readLine("Enter sender's address:");
		if (!senderAddress.trim().equals("")) {
			if (addressBook.getMailbox(senderAddress.toLowerCase()) == null) {
				console.show("invalid sender mailbox");
				return false;
			}
			msg.setOrigin(senderAddress);
		}

		destinationAddress = console.readLine("Enter destination address:");
		if (!destinationAddress.trim().equals("")) {
			destinationMailbox = addressBook.getMailbox(destinationAddress);
			if (destinationMailbox == null) {
				console.show("invalid destination mailbox");
				return false;
			}
			msg.setDestination(destinationAddress);
		}
		return true;
	}

}