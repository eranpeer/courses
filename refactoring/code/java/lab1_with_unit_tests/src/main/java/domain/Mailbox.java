package domain;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import ui.Console;

public class Mailbox implements Iterable<Message> {

	private String address;

	private List<Message> messages;

	public Mailbox(String address) {
		this.address = address;
		messages = new ArrayList<Message>();
	}

	public void add(Message msg) {
		messages.add(msg);
	}

	public Message delete(int msgId) {
		Message m = (Message) messages.get(msgId);
		messages.remove(msgId);
		return m;
	}

	public void delete(Message msg) {
		messages.remove(msg);
	}

	public String getAddress() {
		return address;
	}

	public int count() {
		return messages.size();
	}

	@Override
	public Iterator<Message> iterator() {
		return messages.iterator();
	}

	public void show(Console console) {

		if (count() == 0) {
			console.show("Mailbox " + getAddress() + " is empty");
			return;
		}

		console.show("Content of mailbox " + getAddress() + ":");
		int msgNum = 0;
		for (Message m : this) {
			msgNum++;
			String title = null;
			if (m instanceof Email) {
				Email msg = (Email) m;
				title = "Email: " + msg.getSubject();
			}
			if (m instanceof Text) {
				Text msg = (Text) m;
				title = "Text: " + msg.getText();
			}
			if (m instanceof Voice) {
				Voice msg = (Voice) m;
				title = "Voice: from " + msg.getOrigin();
			}
			title = msgNum + ": " + title;
			console.show(title);
		}
	}

	public static class UnitTest {

		@Test
		public void showEmptyMbxDisplaysEmptyMbxMsg() {
			Console console = mock(Console.class);
			Mailbox mbx = new Mailbox("MBX");
			mbx.show(console);
			verify(console).show("Mailbox MBX is empty");
		}

		@Test
		public void showMessagesByType() {
			Console console = mock(Console.class);
			Mailbox mbx = new Mailbox("MBX");
			Email l = new Email();
			l.setSubject("subj");

			Text c = new Text();
			c.setText("content");

			Voice p = new Voice();
			p.setOrigin("sender");

			mbx.add(l);
			mbx.add(c);
			mbx.add(p);

			mbx.show(console);
			verify(console).show("Content of mailbox MBX:");
			verify(console).show("1: Email: subj");
			verify(console).show("2: Text: content");
			verify(console).show("3: Voice: from sender");
		}

		@Test
		public void canIterateMessageAfterAdd() {
			Message m = mock(Message.class);
			Mailbox mbx = new Mailbox("MBX");
			mbx.add(m);
			Assert.assertEquals(m, mbx.iterator().next());
		}

		@Test
		public void canDeleteMessgaeByIdAfterAdd() {
			Message m = mock(Message.class);
			Mailbox mbx = new Mailbox("MBX");
			mbx.add(m);
			mbx.delete(0);
			Assert.assertFalse(mbx.iterator().hasNext());
		}

		@Test
		public void canDeleteMessgaeAfterAdd() {
			Message m = mock(Message.class);
			Mailbox mbx = new Mailbox("MBX");
			mbx.add(m);
			mbx.delete(m);
			Assert.assertFalse(mbx.iterator().hasNext());
		}
	}
}