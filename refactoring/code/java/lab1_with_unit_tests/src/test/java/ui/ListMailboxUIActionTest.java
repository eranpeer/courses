package ui;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import domain.AddressBook;
import domain.Mailbox;

@RunWith(org.mockito.junit.MockitoJUnitRunner.class)
public class ListMailboxUIActionTest {

	@Mock
	AddressBook addressBook;

	@Mock
	Console console;

	@Mock
	Mailbox mbx;
	
	@Test
	public void verifyShowInvalidMbxAddressMsgOnBadInputAddress() {
		when(console.readLine("Enter mailbox address:")).thenReturn("invalid address");
		when(addressBook.getMailbox("invalid address")).thenReturn(null);
		ListMailboxUIAction listAction = new ListMailboxUIAction(console, addressBook);
		listAction.listMailbox();
		verify(console).show("invalid mailbox address");
	}

	@Test
	public void verifyShowMailboxOnValidInputAddress() {
		when(console.readLine("Enter mailbox address:")).thenReturn("valid address");
		when(addressBook.getMailbox("valid address")).thenReturn(mbx);
		ListMailboxUIAction listAction = new ListMailboxUIAction(console, addressBook);
		listAction.listMailbox();
		verify(mbx).show(console);
	}
}