package thirdparty;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

public class StageDevice {

	static private int numOfFallingPins = -1;
	static private int numOfStandingPins = 0;

	static private volatile boolean isReadyForNewRoll = false;
	static private volatile boolean isRollDone = false;

	static private ArrayList<Boolean> bottles = new ArrayList<>();

	private StageDevice() {
	}

	public static void prepareForRoll(final boolean isNewFrame) {
		isReadyForNewRoll = false;
		isRollDone = false;

		final Timer timer = new java.util.Timer();
		final TimerTask animationTask = new TimerTask() {

			@Override
			public void run() {
				System.out.print(".");
			}
		};

		TimerTask endPrepareTask = new TimerTask() {

			@Override
			public void run() {
				timer.cancel();
				if (isNewFrame) {
					numOfStandingPins = 10;
					initBottles();
				}
				removeFallingBattles();
				numOfFallingPins = -1;
				System.out.println();
				showStage(false);
				clearTypeaheadBuffer();
				isReadyForNewRoll = true;

				readAnyKey();
				rollIsOnTheWay();
			}
		};
		timer.schedule(animationTask, 0, 200);
		timer.schedule(endPrepareTask, 1000);
	}

	private static void dropBottles(int numOfFallingPins) {
		ArrayList<Integer> list = new ArrayList<>();
		for (int i = 0; i < 10 ; i++) {
			Boolean b = bottles.get(i);
			if (b != null) {
				list.add(i);
			}
		}
		Collections.shuffle(list);
		int dropped = 0;
		while (dropped < numOfFallingPins){
			bottles.set(list.get(dropped), false);
			dropped++;
		}		
	}

	private static void initBottles() {
		bottles.clear();
		while (bottles.size() < 10) {
			bottles.add(true);
		}
	}

	static public boolean isReadyForNewRoll() {
		return isReadyForNewRoll;
	}

	static public boolean isRollDone() {
		return isRollDone;
	}

	/**
	 * Return the number of falling pins or -1 if roll is not done.
	 */
	static public int readFallingPins() {
		return numOfFallingPins;
	}

	/**
	 * Return the number of falling pins
	 */
	static public int readStandingPins() {
		return numOfStandingPins;
	}

	static private void rollIsOnTheWay() {
		final Timer timer = new java.util.Timer();
		final TimerTask animationTask = new TimerTask() {

			@Override
			public void run() {
				System.out.print(".");
			}
		};
		timer.schedule(animationTask, 0, 200);
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				timer.cancel();
				numOfFallingPins = (int) Math.round((Math.random() * numOfStandingPins));
				numOfStandingPins -= numOfFallingPins;
				dropBottles(numOfFallingPins);
				System.out.println();
				showStage(true);
				isReadyForNewRoll = false;
				isRollDone = true;
			}
		}, 1000);
	}

	private static void showStage(boolean showFalling) {
		for (int i = 0; i < bottles.size(); i++) {
			Boolean b = bottles.get(i);
			if (b == null) {
				System.out.print(' ');
			} else if (b) {
				System.out.print('*');
			} else {
				System.out.print('-');
			}
		}
		System.out.println();
	}

	private static void removeFallingBattles() {
		for (int i = 0; i < bottles.size(); i++) {
			if (!bottles.get(i)) {
				bottles.set(i, null);
			}
		}
	}

	private static void clearTypeaheadBuffer() {
		try {
			while (System.in.available() > 0)
				readAnyKey();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private static void readAnyKey() {
		try {
			System.in.read();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}