package bowling;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class Console {

	private static final Console instance = new Console();

	public static final Console getInstance() {
		return instance;
	}

	private Console() {
	}

	private BufferedReader reader = new BufferedReader(new InputStreamReader(
			System.in));

	public String readLine(String prompt) {
		String line = null;
		System.out.print(prompt);
		try {
			line = reader.readLine();
		} catch (IOException e) {
		}
		return line;
	}

	public void writeLine(String str) {
		System.out.println(str);
	}

	public void write(String str) {
		System.out.print(str);
	}
}