package bowling;

import java.util.ArrayList;
import java.util.List;

public class BowlingGame {

	private List<PlayerGame> players = new ArrayList<>();

	public void addPlayer(String name) {
		players.add(new PlayerGame(name));
	}

	public void printScores() {
		for (PlayerGame player : players) {
			player.printScore();
		}
	}

	public void play() {
		for (int i = 0; i < 10; i++) {
			if (i > 0)
				Console.getInstance().writeLine("");
			Console.getInstance().writeLine("Frame " + (i + 1));
			for (PlayerGame player : players) {
				Console.getInstance().writeLine(player.getName() + "'s turn");
				player.play(i + 1);
				player.printScore();
			}
		}
		Console.getInstance().writeLine("Game Over");
		printScores();
	}

	public static void main(String[] args) {
		BowlingGame game = new BowlingGame();
		game.addPlayer("Eran");
		game.addPlayer("Ronen");
		game.play();
	}
}
