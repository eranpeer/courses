package bowling;

import thirdparty.StageDevice;

public class PlayerGame {

	private int rolls[] = new int[21];
	private int countRolls = 0;
	private String player;

	public PlayerGame(String player) {
		this.player = player;
	}

	private void addRoll(int pins) {
		rolls[countRolls] = pins;
		countRolls++;
	}

	private void addRollToScore(int numOfFallingPins) {
		showRollScoreMsg(numOfFallingPins);
		addRoll(numOfFallingPins);
	}

	private boolean allPinsAreDown(int knockedDownPins) {
		return knockedDownPins == 10;
	}

	public String getName() {
		return player;
	}

	private boolean isLastFrame(int frame) {
		return allPinsAreDown(frame);
	}

	private boolean isSpare(int i) {
		return simpleFrameScore(i) == 10;
	}

	private boolean isStrike(int i) {
		return rolls[i] == 10;
	}

	public void play(int frame) {
		int knockedDownThisFrame = playRoll(true, "first");
		if (allPinsAreDown(knockedDownThisFrame)) {
			showStrikeMsg();
			if (isLastFrame(frame)) {
				playStrikeOnLastFrame();
			}
			return;
		}

		knockedDownThisFrame += playRoll(false, "second");
		if (allPinsAreDown(knockedDownThisFrame)) {
			showSpareMsg();
			if (isLastFrame(frame)) {
				playRoll(true, "bonus");
			}
		}
	}

	private int playRoll(boolean isNewFrame, String rollName) {
		int numOfFallingPins;
		numOfFallingPins = readRoll(isNewFrame, rollName);
		addRollToScore(numOfFallingPins);
		return numOfFallingPins;
	}

	private void playStrikeOnLastFrame() {
		int knockedDownPins = playRoll(true, "first bonus");
		playRoll(allPinsAreDown(knockedDownPins), "second bonus");
	}

	public void printScore() {
		String player = this.player;
		showScoreMsg(player, score());
	}

	private int readRoll(boolean isNewFrame, String rollName) {
		int numOfFallingPins;
		StageDevice.prepareForRoll(isNewFrame);
		waitForStageReady();
		showAskToThrowARollMsg(rollName);
		waitForRollDone();
		numOfFallingPins = StageDevice.readFallingPins();
		return numOfFallingPins;
	}

	private int score() {
		int score = 0;
		int firstRollInFrame = 0;
		for (int frame = 0; frame < 10; frame++) {
			if (isStrike(firstRollInFrame)) {
				score += 10 + strikeBonus(firstRollInFrame);
				firstRollInFrame += 1;
			} else if (isSpare(firstRollInFrame)) {
				score += 10 + spareBonus(firstRollInFrame);
				firstRollInFrame += 2;
			} else {
				score += simpleFrameScore(firstRollInFrame);
				firstRollInFrame += 2;
			}
		}
		return score;
	}

	private void showAskToThrowARollMsg(String rollName) {
		Console.getInstance().writeLine("Press Enter to throw " + rollName + " roll!");
	}

	private void showRollScoreMsg(int numOfFallingPins) {
		Console.getInstance().writeLine("You scored " + numOfFallingPins);
	}

	private void showScoreMsg(String player, int score) {
		Console.getInstance().writeLine(player + "'s score is " + score);
	}

	private void showSpareMsg() {
		Console.getInstance().writeLine("Spare!!!");
	}

	private void showStrikeMsg() {
		Console.getInstance().writeLine("Strike!!!");
	}

	private int simpleFrameScore(int firstRollInFrame) {
		return rolls[firstRollInFrame] + rolls[firstRollInFrame + 1];
	}

	private int spareBonus(int firstRollInFrame) {
		return rolls[firstRollInFrame + 2];
	}

	private int strikeBonus(int firstRollInFrame) {
		return rolls[firstRollInFrame + 1] + spareBonus(firstRollInFrame);
	}

	private void waitForRollDone() {
		try {
			while (!StageDevice.isRollDone()) {
				Thread.sleep(200);
			}
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	private void waitForStageReady() {
		try {
			while (!StageDevice.isReadyForNewRoll()) {
				Thread.sleep(200);
			}
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}
}