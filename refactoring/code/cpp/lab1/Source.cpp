#include <thread>
#include <chrono>
#include <vector>
#include "StageDevice.h"
#include "BowlingGame.h"

using namespace std;
int main(){

	BowlingGame game;
	game.addPlayer("Eran");
	game.addPlayer("Ronen");
	game.play();
	return 0;
}
