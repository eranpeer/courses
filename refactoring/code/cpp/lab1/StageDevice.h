#ifndef StageDevice_h__
#define StageDevice_h__

#include <atomic>
#include <vector>

class StageDevice {
public:
	void prepareForRoll(bool isNewFrame);
	bool getIsReadyForRoll(){ return this->isReadyForRoll; }
	bool getIsRollDone(){ return this->isRollDone; }
	int getNumOfStandingPins(){ return this->numOfStandingPins; }
	int getNumOfKnockedDownPins(){ return this->numOfKnockedDownPins; }
private:
	enum BottleState { UP, DOWN, EMPTY };
	std::vector<BottleState> bottles;
	std::atomic<bool> isReadyForRoll;
	std::atomic<bool> isRollDone;
	std::atomic<int> numOfStandingPins;
	std::atomic<int> numOfKnockedDownPins;
	void initBottles();
	void removeFallingBattles();
	void showStage(bool showKnockedDownPins);
	void rollIsOnTheWay();
	void dropBottles(int numOfFallingPins);
};
#endif // StageDevice_h__