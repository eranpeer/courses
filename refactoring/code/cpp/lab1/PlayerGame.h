#ifndef PlayerGame_h__
#define PlayerGame_h__
#include <string>
#include <array>
#include "StageDevice.h"

class PlayerGame
{
public:
	PlayerGame(StageDevice & stage, std::string player);
	~PlayerGame();

	const std::string& getName(){ return player; }
	void roll(int pins);

	int score();

	void printScore();

	void play(int frame);

private:
	std::string player;
	int countRolls;
	std::array<int, 21> rolls;
	StageDevice& stage;
};
#endif // PlayerGame_h__


