#include "PlayerGame.h"

#include <iostream>
#include <thread>
#include <chrono>

PlayerGame::PlayerGame(StageDevice & stage, std::string player) : stage(stage), player(player), countRolls(0), rolls()
{
}


PlayerGame::~PlayerGame()
{
}

void PlayerGame::roll(int pins)
{
	rolls[countRolls++] = pins;
}

int PlayerGame::score()
{
	int score = 0;
	for (int i = 0; i < 20; i += 2) {
		if (rolls[i] == 10) {
			score += 10;
			score += rolls[i + 1] + rolls[i + 2];
			i--;
		}
		else {
			if (rolls[i] + rolls[i + 1] == 10) {
				score += 10;
				score += rolls[i + 2];
			}
			else
				score += rolls[i] + rolls[i + 1];
		}
	}
	return score;
}

void PlayerGame::printScore()
{
	int score = 0;
	for (int i = 0; i < 20; i += 2) {
		if (rolls[i] == 10) {
			score += 10;
			score += rolls[i + 1] + rolls[i + 2];
			i--;
		}
		else {
			if (rolls[i] + rolls[i + 1] == 10) {
				score += 10;
				score += rolls[i + 2];
			}
			else
				score += rolls[i] + rolls[i + 1];
		}
	}
	std::cout << player << "'s score is " << score << std::endl;
}

void PlayerGame::play(int frame)
{
	int knockedDownThisFrame = 0;
	stage.prepareForRoll(true);
	while (!stage.getIsReadyForRoll()) {
		std::this_thread::sleep_for(std::chrono::milliseconds(200));
	}
	std::cout << "Press Enter to throw first roll!" << std::endl;
	while (!stage.getIsRollDone()) {
		std::this_thread::sleep_for(std::chrono::milliseconds(200));
	}
	int numOfFallingPins = stage.getNumOfKnockedDownPins();
	std::cout << "You scored " << numOfFallingPins << std::endl;
	roll(numOfFallingPins);

	knockedDownThisFrame += numOfFallingPins;
	if (knockedDownThisFrame < 10) {
		// not a strike, we have another roll
		stage.prepareForRoll(false);
		while (!stage.getIsReadyForRoll()) {
			std::this_thread::sleep_for(std::chrono::milliseconds(200));
		}
		std::cout << "Press Enter to throw second roll!" << std::endl;
		while (!stage.getIsRollDone()) {
			std::this_thread::sleep_for(std::chrono::milliseconds(200));
		}

		numOfFallingPins = stage.getNumOfKnockedDownPins();
		std::cout << "You scored " << numOfFallingPins << std::endl;
		roll(numOfFallingPins);

		knockedDownThisFrame += numOfFallingPins;
		if (knockedDownThisFrame == 10) {
			std::cout << "Spare!!!" << std::endl;
		}

		if (knockedDownThisFrame == 10 && frame == 10) {
			// spare on last frame. Throw one more bonus roll.
			stage.prepareForRoll(true);
			while (!stage.getIsReadyForRoll()) {
				std::this_thread::sleep_for(std::chrono::milliseconds(200));
			}
			std::cout << "Press Enter to throw bonus roll!" << std::endl;
			while (!stage.getIsRollDone()) {
				std::this_thread::sleep_for(std::chrono::milliseconds(200));
			}

			numOfFallingPins = stage.getNumOfKnockedDownPins();
			std::cout << "You scored " << numOfFallingPins << std::endl;
			roll(numOfFallingPins);
		}
	}
	else {
		std::cout << "Strike!!!" << std::endl;
		if (frame == 10) {
			// strike on last frame
			stage.prepareForRoll(true);
			while (!stage.getIsReadyForRoll()) {
				std::this_thread::sleep_for(std::chrono::milliseconds(200));
			}
			std::cout << "Press Enter to throw first bonus roll!" << std::endl;
			while (!stage.getIsRollDone()) {
				std::this_thread::sleep_for(std::chrono::milliseconds(200));
			}

			numOfFallingPins = stage.getNumOfKnockedDownPins();
			std::cout << "You scored " << numOfFallingPins << std::endl;
			roll(numOfFallingPins);

			if (numOfFallingPins == 10) {
				stage.prepareForRoll(true);
			}
			else {
				stage.prepareForRoll(false);
			}

			while (!stage.getIsReadyForRoll()) {
				std::this_thread::sleep_for(std::chrono::milliseconds(200));
			}
			std::cout << "Press Enter to throw second bonus roll!" << std::endl;
			while (!stage.getIsRollDone()) {
				std::this_thread::sleep_for(std::chrono::milliseconds(200));
			}

			numOfFallingPins = stage.getNumOfKnockedDownPins();
			std::cout << "You scored " << numOfFallingPins << std::endl;
			roll(numOfFallingPins);
		}
	}

}
