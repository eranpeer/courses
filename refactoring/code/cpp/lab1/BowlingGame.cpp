#include "BowlingGame.h"
#include <iostream>


BowlingGame::BowlingGame()
{
}

BowlingGame::~BowlingGame()
{
	for (PlayerGame *player : players) {
		delete player;
	}
	players.clear();
}

void BowlingGame::printScores()
{
	for (PlayerGame *player : players) {
		player->printScore();
	}
}

void BowlingGame::addPlayer(std::string name)
{
	players.push_back(new PlayerGame(stage,name));
}

void BowlingGame::play()
{
	for (int i = 0; i < 10; i++) {
		if (i > 0)
			std::cout<<std::endl;
		std::cout << "Frame " << (i + 1) << std::endl;
		for (PlayerGame* player : players) {
			std::cout << player->getName() + "'s turn" << std::endl;
			player->play(i + 1);
			player->printScore();
		}
	}
	std::cout << "Game Over" << std::endl;
	printScores();
}
