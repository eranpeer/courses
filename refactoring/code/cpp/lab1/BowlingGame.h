
#ifndef BowlingGame_h__
#define BowlingGame_h__

#include <vector>
#include <string>
#include "PlayerGame.h"
#include "StageDevice.h"

class BowlingGame
{
public:
	BowlingGame();
	~BowlingGame();

	void addPlayer(std::string name);
	void play();

private:
	std::vector<PlayerGame*> players;
	StageDevice stage;

	void printScores();
};
#endif // BowlingGame_h__

