#pragma once
#include "StageDevice.h"
#include <chrono>
#include <thread>
#include <atomic>
#include <iostream>
#include <random>
#include <algorithm>

using namespace std;

void waitForThrow()
{
	char buff[1024];
	std::cin.getline(buff, 256, '\n');
}

void animate()
{
	for (int i = 0; i < 5; i++){
		std::cout << ".";
		std::this_thread::sleep_for(std::chrono::milliseconds(200));
	}
}

void StageDevice::prepareForRoll(bool isNewFrame) {
	this->isReadyForRoll = false;
	this->isRollDone = false;
	std::thread([this,isNewFrame](){
		animate();
		if (isNewFrame) {
			numOfStandingPins = 10;
			initBottles();
		}
		removeFallingBattles();
		numOfKnockedDownPins = -1;
		cout << std::endl;
		showStage(false);
		cin.clear();
		this->isReadyForRoll = true;
		waitForThrow();
		rollIsOnTheWay();
	}).detach();
}

void StageDevice::initBottles()
{
	bottles.clear();
	while (bottles.size() < 10) {
		bottles.push_back(BottleState::UP);
	}
}

void StageDevice::removeFallingBattles()
{
	for (unsigned int i = 0; i < bottles.size(); i++) {
		if (!bottles[i] == UP) {
			bottles[i] = EMPTY;
		}
	}
}

void StageDevice::showStage(bool showKnockedDownPins)
{
	for (unsigned int i = 0; i < bottles.size(); i++) {
		BottleState b = bottles[i];
		if (b == EMPTY) {
			cout<<' ';
		}
		else if (b == UP) {
			cout << '*';
		}
		else {
			cout << '-';
		}
	}
	cout << std::endl;
}

int getRandomKnockedDown(int numOfStandingPins)
{
	std::random_device rd;
	std::uniform_int_distribution<int> dist(0, numOfStandingPins);
	return dist(rd);
}

void StageDevice::rollIsOnTheWay()
{
	std::uniform_int_distribution<int> num;
	this->isRollDone = false;
	std::thread([this](){
		animate();
		numOfKnockedDownPins = getRandomKnockedDown(numOfStandingPins);
		numOfStandingPins -= numOfKnockedDownPins;
		dropBottles(numOfKnockedDownPins);
		cout << std::endl;
		showStage(true);
		isReadyForRoll = false;
		isRollDone = true;
	}).detach();
}

void shuffle(std::vector<int> &list)
{
	std::random_device rd;
	std::mt19937 g(rd());
	std::shuffle(list.begin(), list.end(), g);
}

void StageDevice::dropBottles(int numOfFallingPins)
{
	std::vector<int> list;
	for (int i = 0; i < 10; i++) {
		BottleState b = bottles[i];
		if (b == UP) {
			list.push_back(i);
		}
	}
	shuffle(list);
	int dropped = 0;
	while (dropped < numOfFallingPins){
		bottles[list[dropped]] = DOWN;
		dropped++;
	}
}