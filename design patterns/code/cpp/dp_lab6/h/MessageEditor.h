#pragma once

#include "Message.h"

class MessageEditor {
public:
	virtual ~MessageEditor(){}
	virtual bool edit(Message * msg) = 0;
};
