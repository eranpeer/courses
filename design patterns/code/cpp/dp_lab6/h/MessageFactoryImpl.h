#pragma once

#include "MessageFactory.h"
#include <string>

class MessageFactoryImpl: public MessageFactory {
public:
	Message* createMessage(const string& messageType);
};
