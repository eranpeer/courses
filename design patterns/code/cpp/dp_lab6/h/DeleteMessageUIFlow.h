#pragma once

#include "AddressBook.h"
#include "Console.h"

class DeleteMessageUIFlow {
public:
	DeleteMessageUIFlow(AddressBook & addressBook, Console& console);
	~DeleteMessageUIFlow();

	void deleteMessage();
private:
	AddressBook & addressBook;
	Console& console;
};
