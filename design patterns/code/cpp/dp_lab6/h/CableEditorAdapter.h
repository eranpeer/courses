#pragma once

#include "MessageEditor.h"
#include "CableEditor.h"

class CableEditorAdapter: private CableEditor, public MessageEditor {
public:
	CableEditorAdapter(AddressBook & anAddressBook, Console& console) :
			CableEditor(anAddressBook, console) {
	}
	virtual ~CableEditorAdapter() {
	}

	bool edit(Message* m);
};
