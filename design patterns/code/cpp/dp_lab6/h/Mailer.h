#pragma once

#include "AddressBook.h"
#include "Message.h"

class Mailer {
public:
	Mailer(AddressBook& anAddressBook);
	~Mailer();

	void deliver(Message * m) const;
private:
	AddressBook &addressBook;
};
