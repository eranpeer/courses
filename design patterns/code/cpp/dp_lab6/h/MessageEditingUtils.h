#pragma once

#include "AddressBook.h"
#include "Message.h"

class MessageEditingUtils {
public:
	static int EditAddresses(Message *m, AddressBook & anAddressBook);
};
