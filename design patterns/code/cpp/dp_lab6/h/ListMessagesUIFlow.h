#pragma once

#include "AddressBook.h"
#include "Console.h"

class ListMessagesUIFlow {
public:
	ListMessagesUIFlow(AddressBook& addressBook, Console& console);
	~ListMessagesUIFlow();

	void listMessages();
private:
	AddressBook& addressBook;
	Console& console;
};
