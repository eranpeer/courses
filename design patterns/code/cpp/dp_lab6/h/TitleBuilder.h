#pragma once

#include "Console.h"
#include "MessageVisitor.h"
#include <string>

class TitleBuilder: public MessageVisitor {
public:
	TitleBuilder(Console& console);
	~TitleBuilder();

	void visit(Letter* msg);
	void visit(Cable* msg);
	void visit(Postcard* msg);

	std::string getTitle() const;
private:
	std::string title;
	Console& console;
};
