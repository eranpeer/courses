#include "AddressBook.h"
#include "Mailbox.h"

AddressBook::AddressBook() {
	countMailboxes = 0;
}

void AddressBook::addMailbox(Mailbox *mbx) {
	if (countMailboxes >= N_MBX)
		return;
	mailboxes[countMailboxes++] = mbx;
}

Mailbox* AddressBook::getMailbox(const string address) const {
	for (int i = 0; i < countMailboxes; i++)
		if (mailboxes[i]->getAddress() == address)
			return mailboxes[i];
	return 0;
}
