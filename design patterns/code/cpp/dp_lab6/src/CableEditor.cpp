#include "CableEditor.h"
#include "MessageEditingUtils.h"
#include <string>

using namespace std;

CableEditor::CableEditor(AddressBook & anAddressBook, Console& console) :
		addressBook(anAddressBook), console(console) {
}

CableEditor::~CableEditor() {
}

bool CableEditor::edit(Cable *m) const {
	if (!MessageEditingUtils::EditAddresses(m, addressBook))
		return false;
	string messageContent = console.ReadLine(string("Enter cable's content:"),
			m->getContent());
	m->setContent(messageContent);
	return true;
}
