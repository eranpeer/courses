#include "Desktop.h"
#include "MenuItem.h"
#include "Menu.h"

using namespace thirdparty;

class CommandMenuItem: public MenuItem {
public:
	CommandMenuItem(Desktop::Command& command) :
			MenuItem(command.getDisplayName()), command(command) {
	}
	virtual void click() {
		command.execute();
	}
private:
	Desktop::Command& command;
};

class MenuCommand: public Desktop::Command {
public:
	MenuCommand(std::string displayName) :
			menu(displayName) {
	}
	virtual ~MenuCommand() {
	}

	virtual void execute() {
		menu.show();
	}

	std::string getDisplayName() {
		return menu.getName();
	}

	bool addCommand(Desktop::Command& c) {
		menu.add(new CommandMenuItem(c));
		return true;
	}
private:
	thirdparty::Menu menu;
};

Desktop::Desktop() :
		isActive(false), mainMenu("Select an option") {
}

Desktop::~Desktop() {
}

void Desktop::addDesktopCommand(Desktop::Command& command) {
	mainMenu.add(new CommandMenuItem(command));
}

Desktop::Command* Desktop::createMenuCommand(std::string displayName) {
	return new MenuCommand(displayName);
}

void Desktop::start() {
	isActive = true;
	while (isActive) {
		mainMenu.show();
	}
}

void Desktop::stop() {
	isActive = false;
}
