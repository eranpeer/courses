#include <stdlib.h>

#include "Application.h"
#include "MessageEditorFactoryImpl.h"
#include "MessageFactoryImpl.h"
#include "AddressBook.h"
#include "Mailbox.h"
#include "Desktop.h"
#include "SendMessageUIFlow.h"
#include "ListMessagesUIFlow.h"
#include "DeleteMessageUIFlow.h"
#include "SendCommand.h"
#include "ListMessagesCommand.h"
#include "DeleteMessageCommand.h"
#include "ExitCommand.h"
#include <memory>

Application::Application(void) {
	editorFactory.reset(new MessageEditorFactoryImpl(addressBook, console));
	messageFactory.reset(new MessageFactoryImpl());
}

Application::~Application(void) {
}

void Application::start(void) {
	Mailbox eranMbx("eran");
	Mailbox ronenMbx("ronen");

	addressBook.addMailbox(&eranMbx);
	addressBook.addMailbox(&ronenMbx);

	SendCommand sendLetterCommand(*this, "letter");
	SendCommand sendCableCommand(*this, "cable");
	SendCommand sendPostcardCommand(*this, "postcard");
	std::auto_ptr<Desktop::Command> sendCommand(
			desktop.createMenuCommand("Send"));

	sendCommand->addCommand(sendLetterCommand);
	sendCommand->addCommand(sendCableCommand);
	sendCommand->addCommand(sendPostcardCommand);

	DeleteMessageCommand deleteCommand(*this);
	ListMessagesCommand listCommand(*this);
	ExitCommand exitCommand(*this);

	desktop.addDesktopCommand(*sendCommand);
	desktop.addDesktopCommand(deleteCommand);
	desktop.addDesktopCommand(listCommand);
	desktop.addDesktopCommand(exitCommand);

	desktop.start();
}

void Application::stop(void) {
	desktop.stop();
}

int main() {
	Application app;
	app.start();
	return 0;
}

AddressBook& Application::getAddessBook() {
	return addressBook;
}

Console& Application::getConsole() {
	return console;
}

MessageEditorFactory& Application::getEditorFactory() const {
	return *editorFactory.get();
}

MessageFactory& Application::getMessageFactory() const {
	return *messageFactory.get();
}
