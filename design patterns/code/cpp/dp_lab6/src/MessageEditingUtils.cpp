#include "MessageEditingUtils.h"
#include "Mailbox.h"
#include "AddressBook.h"
#include "Console.h"

#include <string>

int MessageEditingUtils::EditAddresses(Message *m, AddressBook & addressBook) {
	Mailbox *destinationMailbox = 0;

	Console console;
	string senderAddress = console.ReadLine(string("Enter sender's address:"),
			m->getOrigin());

	if (!(addressBook.getMailbox(senderAddress))) {
		console.Write("invalid sender mailbox\n");
		return 0;
	}

	string destinationAddress = console.ReadLine(
			string("Enter destination address:"), m->getDestination());
	destinationMailbox = addressBook.getMailbox(destinationAddress);
	if (!destinationMailbox) {
		console.Write("invalid destination mailbox\n");
		return 0;
	}

	m->setOrigin(senderAddress);
	m->setDestination(destinationAddress);
	return 1;
}
