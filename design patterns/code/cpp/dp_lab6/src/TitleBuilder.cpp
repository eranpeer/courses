#include "TitleBuilder.h"
#include "Letter.h"
#include "Cable.h"
#include "Postcard.h"

TitleBuilder::TitleBuilder(Console& console) :
		console(console) {
}

TitleBuilder::~TitleBuilder() {
}

std::string TitleBuilder::getTitle() const {
	return title;
}

void TitleBuilder::visit(Letter* msg) {
	title = "Letter: ";
	title = title + msg->getSubject();
}

void TitleBuilder::visit(Cable* msg) {
	title = "Cable: ";
	title = title + msg->getContent();
}

void TitleBuilder::visit(Postcard* msg) {
	title = "Postcard: from ";
	title = title + msg->getOrigin();
}
