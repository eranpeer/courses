#include "SendMessageUIFlow.h"
#include "Mailer.h"
#include "Message.h"
#include "Letter.h"
#include "Cable.h"
#include "Postcard.h"
#include "MessageEditor.h"
#include "MessageEditorFactoryImpl.h"
#include <memory>

using namespace std;

SendMessageUIFlow::SendMessageUIFlow(AddressBook& addressBook, Console& console,
		MessageEditorFactory & editorFactory, MessageFactory& messageFactory) :
		addressBook(addressBook), console(console), editorFactory(
				editorFactory), messageFactory(messageFactory) {
}

SendMessageUIFlow::~SendMessageUIFlow() {
}

void SendMessageUIFlow::sendNewMessage() {
	string messageType = console.ReadLine("Enter type of message:");
	sendNewMessage(messageType);
}

void SendMessageUIFlow::sendNewMessage(std::string messageType) {

	if (messageType != "cable" && messageType != "letter"
			&& messageType != "postcard") {
		console.Write("invalid message type !\n");
		return;
	}

	Message* newMessage = messageFactory.createMessage(messageType);

	auto_ptr<MessageEditor> editor(editorFactory.createEditor(messageType));
	if (!editor->edit(newMessage))
		return;

	Mailer(addressBook).deliver(newMessage);
	console.Write(
			string("A new ") + messageType + " for "
					+ newMessage->getDestination() + "\n");
}
