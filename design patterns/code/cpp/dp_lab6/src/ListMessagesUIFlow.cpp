#include "ListMessagesUIFlow.h"
#include "Mailbox.h"
#include "Message.h"
#include "Letter.h"
#include "Cable.h"
#include "Postcard.h"
#include "Text.h"
#include "Console.h"
#include "TitleBuilder.h"

#include <string.h>

ListMessagesUIFlow::ListMessagesUIFlow(AddressBook& addressBook,
		Console& console) :
		addressBook(addressBook), console(console) {
}

ListMessagesUIFlow::~ListMessagesUIFlow() {
}

void ListMessagesUIFlow::listMessages() {
	string address = console.ReadLine("Enter mailbox address:");
	Mailbox* mbx = addressBook.getMailbox(address);
	if (!mbx) {
		console.WriteLine("invalid destination mailbox");
		return;
	}

	TitleBuilder v(console);
	console.WriteLine(string("Content of mailbox ") + mbx->getAddress() + ":");
	Message* currMsg = 0;
	for (int i = 0; (currMsg = mbx->getMessage(i)); i++) {
		currMsg->accept(v);
		console.WriteLine(Text::format(i + 1) + ": " + v.getTitle());
	}
}
