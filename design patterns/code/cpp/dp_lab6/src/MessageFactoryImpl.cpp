#include "MessageFactoryImpl.h"
#include "Letter.h"
#include "Cable.h"
#include "Postcard.h"

Message* MessageFactoryImpl::createMessage(const string& messageType) {
	if (messageType == "letter") {
		return new Letter();
	}
	if (messageType == "cable") {
		return new Cable();
	}
	if (messageType == "postcard") {
		return new Postcard();
	}
	return 0;
}
