#pragma once

#include "Application.h"
#include <string>

class SendCommand: public Desktop::Command {
public:
	SendCommand(Application& app, const std::string messageType);
	virtual ~SendCommand();

	void execute();
	std::string getDisplayName();
private:
	Application & app;
	std::string messageType;
};
