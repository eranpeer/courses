#pragma once
#include "Application.h"
#include "DeleteMessageUIFlow.h"

class DeleteMessageCommand: public Desktop::Command {
public:
	DeleteMessageCommand(Application& app);

	virtual ~DeleteMessageCommand();

	void execute();

	std::string getDisplayName();

private:
	Application & app;
};
