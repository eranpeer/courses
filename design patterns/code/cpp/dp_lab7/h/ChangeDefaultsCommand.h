#pragma once

#include "Application.h"
#include <string>

class ChangeDefaultsCommand: public Desktop::Command {
public:
	ChangeDefaultsCommand(Application& app, const std::string messageType);

	virtual ~ChangeDefaultsCommand();

	void execute();

	std::string getDisplayName();
private:
	Application & app;
	std::string messageType;
};
