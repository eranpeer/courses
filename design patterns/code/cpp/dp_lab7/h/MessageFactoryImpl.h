#pragma once

#include "MessageFactory.h"
#include <string>
#include <map>

class MessageFactoryImpl: public MessageFactory {
public:
	MessageFactoryImpl();
	Message* createMessage(const string& messageType);

	void setPrototype(const string& messageType, Message* prototype);
	Message* getPrototype(const string& messageType);

private:
	std::map<std::string, Message*> prototypes;
};
