#pragma once

#include "Desktop.h"
#include "Application.h"

class ExitCommand: public Desktop::Command {
public:
	ExitCommand(Application& application);

	virtual ~ExitCommand();

	virtual void execute();

	std::string getDisplayName();

private:
	Application &application;
};
