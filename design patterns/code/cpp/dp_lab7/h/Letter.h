#pragma once

#include <string>
#include "Message.h"
#include "MessageVisitor.h"

using namespace std;

class Letter: public Message {
public:
	Letter();
	virtual ~Letter();

	string getContent() const;
	void setContent(const string& content);

	string getSubject() const;
	void setSubject(const string& subject);

	void accept(MessageVisitor& v);

	virtual Letter* clone();

private:
	string content;
	string subject;
};
