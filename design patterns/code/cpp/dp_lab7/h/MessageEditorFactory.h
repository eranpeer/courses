#pragma once

#include "MessageEditor.h"

class MessageEditorFactory {
public:
	virtual ~MessageEditorFactory(){}
	virtual MessageEditor* createEditor(const string& messageType) = 0;
};
