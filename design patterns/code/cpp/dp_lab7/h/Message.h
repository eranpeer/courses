#pragma once

#include "MessageVisitor.h"
#include <string>

using namespace std;

class Message {
public:
	Message();
	virtual ~Message();

	string getOrigin() const;
	void setOrigin(const string& origin);

	string getDestination() const;
	void setDestination(const string& destination);

	virtual Message * clone() = 0;

	virtual void accept(MessageVisitor& v) = 0;

private:
	string origin;
	string destination;
};
