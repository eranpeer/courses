#pragma once

#include "MessageEditor.h"
#include "PostcardEditor.h"

class PostcardEditorAdapter: private PostcardEditor, public MessageEditor {
public:
	PostcardEditorAdapter(AddressBook & anAddressBook, Console& console) :
			PostcardEditor(anAddressBook, console) {
	}
	~PostcardEditorAdapter() {
	}

	bool edit(Message* m);
};
