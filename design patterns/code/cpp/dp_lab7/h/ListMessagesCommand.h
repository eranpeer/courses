#pragma once

#include "Application.h"

class ListMessagesCommand: public Desktop::Command {
public:
	ListMessagesCommand(Application& app);
	virtual ~ListMessagesCommand();
	void execute();
	std::string getDisplayName();
private:
	Application & app;
};
