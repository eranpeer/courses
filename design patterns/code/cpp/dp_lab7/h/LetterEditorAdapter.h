#pragma once

#include "MessageEditor.h"
#include "LetterEditor.h"

class LetterEditorAdapter: private LetterEditor, public MessageEditor {
public:
	LetterEditorAdapter(AddressBook& anAddressBook, Console& console);
	~LetterEditorAdapter();

	bool edit(Message* m);
};
