#include "Application.h"
#include "MessageEditorFactoryImpl.h"
#include "MessageFactoryImpl.h"
#include "AddressBook.h"
#include "Mailbox.h"
#include "Desktop.h"
#include "Letter.h"
#include "Cable.h"
#include "Postcard.h"
#include "SendCommand.h"
#include "ListMessagesCommand.h"
#include "DeleteMessageCommand.h"
#include "ChangeDefaultsCommand.h"
#include "ExitCommand.h"

#include <memory>

Application::Application(void) {
	editorFactory.reset(new MessageEditorFactoryImpl(addressBook, console));
	MessageFactoryImpl* mf = new MessageFactoryImpl();
	mf->setPrototype("letter", new Letter());
	mf->setPrototype("cable", new Cable());
	mf->setPrototype("postcard", new Postcard());
	messageFactory.reset(mf);
}

Application::~Application(void) {
}

void Application::start(void) {
	Mailbox eranMbx("eran");
	Mailbox ronenMbx("ronen");

	addressBook.addMailbox(&eranMbx);
	addressBook.addMailbox(&ronenMbx);

	SendCommand sendLetterCommand(*this, "letter");
	SendCommand sendCableCommand(*this, "cable");
	SendCommand sendPostcardCommand(*this, "postcard");
	std::auto_ptr<Desktop::Command> sendCommand(
			desktop.createMenuCommand("Send"));

	sendCommand->addCommand(sendLetterCommand);
	sendCommand->addCommand(sendCableCommand);
	sendCommand->addCommand(sendPostcardCommand);

	ChangeDefaultsCommand changeLetterDefaultsCommand(*this, "letter");
	ChangeDefaultsCommand changeCableDefaultsCommand(*this, "cable");
	ChangeDefaultsCommand changePostcardDefaultsCommand(*this, "postcard");
	std::auto_ptr<Desktop::Command> changeDefaultsCommand(
			desktop.createMenuCommand("Change defaults"));
	changeDefaultsCommand->addCommand(changeLetterDefaultsCommand);
	changeDefaultsCommand->addCommand(changeCableDefaultsCommand);
	changeDefaultsCommand->addCommand(changePostcardDefaultsCommand);

	DeleteMessageCommand deleteCommand(*this);
	ListMessagesCommand listCommand(*this);
	ExitCommand exitCommand(*this);

	desktop.addDesktopCommand(*sendCommand);
	desktop.addDesktopCommand(deleteCommand);
	desktop.addDesktopCommand(listCommand);
	desktop.addDesktopCommand(*changeDefaultsCommand);
	desktop.addDesktopCommand(exitCommand);

	desktop.start();
}

void Application::stop(void) {
	desktop.stop();
}

int main() {
	Application app;
	app.start();
	return 0;
}

AddressBook& Application::getAddessBook() {
	return addressBook;
}

Console& Application::getConsole() {
	return console;
}

MessageEditorFactory& Application::getEditorFactory() const {
	return *editorFactory.get();
}

MessageFactory& Application::getMessageFactory() const {
	return *messageFactory.get();
}
