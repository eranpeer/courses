#include "Postcard.h"

Postcard* Postcard::clone() {
	return new Postcard(*this);
}

Postcard::Postcard() {
}

string Postcard::getText() const {
	return text;
}

void Postcard::setText(const string& text) {
	this->text = text;
}

void Postcard::accept(MessageVisitor& v) {
	v.visit(this);
}
