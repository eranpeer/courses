#include "Cable.h"

Cable* Cable::clone() {
	return new Cable(*this);
}

Cable::Cable() {
}

string Cable::getContent() const {
	return content;
}

void Cable::setContent(const string& content) {
	this->content = content;
}

void Cable::accept(MessageVisitor& v) {
	v.visit(this);
}
