#include "Console.h"
#include <iostream>
#include <stdlib.h>

Console::Console(void) {
}

Console::~Console(void) {
}

void Console::Write(const string& text) {
	cout << text;
}

void Console::Write(const char* text) {
	cout << text;
}

void Console::WriteLine(const char* text) {
	cout << text << '\n';
}

void Console::WriteLine(const string& text) {
	cout << text << '\n';
}

string ReadLineImpl(const string& prompt) {
	char buff[100];
	cout << prompt << "\n";
	cin.getline(buff, 100);
	string line = buff;
	return line;
}

string Console::ReadLine(const string& prompt, const string& defaultValue) {
	string fullPrompt;
	fullPrompt.append(prompt);
	if (!defaultValue.empty()) {
		fullPrompt.append("[");
		fullPrompt.append(defaultValue);
		fullPrompt.append("]");
	}

	string line = ReadLineImpl(fullPrompt);

	if (line.empty())
		line.append(defaultValue);
	return line;
}

string Console::ReadLine(const string& prompt) {
	string noDefaultValue;
	return ReadLine(prompt, noDefaultValue);
}

string Console::ReadLine(const char* prompt) {
	string stringPrompt(prompt);
	return ReadLine(stringPrompt);
}
