#include "ChangeDefaultsCommand.h"
#include "Message.h"
#include "MessageEditor.h"
#include "MessageFactoryImpl.h"

ChangeDefaultsCommand::ChangeDefaultsCommand(Application& app,
		const std::string messageType) :
		app(app), messageType(messageType) {
}

ChangeDefaultsCommand::~ChangeDefaultsCommand() {
}

void ChangeDefaultsCommand::execute() {
	MessageFactoryImpl& prototypeFactory =
			(MessageFactoryImpl&) ((app.getMessageFactory()));
	Message* prototype = prototypeFactory.getPrototype(messageType);
	prototype = prototype->clone();
	MessageEditor* editor = app.getEditorFactory().createEditor(messageType);
	if (editor->edit(prototype)) {
		prototypeFactory.setPrototype(messageType, prototype);
	}
}

std::string ChangeDefaultsCommand::getDisplayName() {
	return string("Change ").append(messageType).append(" defaults");
}
