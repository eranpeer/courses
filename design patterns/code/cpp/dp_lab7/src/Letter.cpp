#include "Letter.h"

Letter* Letter::clone() {
	return new Letter(*this);
}

Letter::Letter() {
}

Letter::~Letter() {
}

string Letter::getContent() const {
	return content;
}

void Letter::setContent(const string& content) {
	this->content = content;
}

string Letter::getSubject() const {
	return subject;
}

void Letter::setSubject(const string& subject) {
	this->subject = subject;
}

void Letter::accept(MessageVisitor& v) {
	v.visit(this);
}
