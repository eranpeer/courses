#include "MessageFactoryImpl.h"

MessageFactoryImpl::MessageFactoryImpl() {
}

Message* MessageFactoryImpl::createMessage(const string& messageType) {
	Message* prototype = getPrototype(messageType);
	if (prototype == 0)
		return 0;
	return prototype->clone();
}

void MessageFactoryImpl::setPrototype(const string& messageType,
		Message* prototype) {
	prototypes[messageType] = prototype;
}

Message* MessageFactoryImpl::getPrototype(const string& messageType) {
	map<string, Message*>::iterator it = prototypes.find(messageType);
	if (it == prototypes.end())
		return 0;
	return it->second;
}
