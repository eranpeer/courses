#include "ExitCommand.h"

ExitCommand::ExitCommand(Application& application) :
		application(application) {
}

ExitCommand::~ExitCommand() {
}

void ExitCommand::execute() {
	application.stop();
}

std::string ExitCommand::getDisplayName() {
	return "Exit";
}
