#include "ListMessagesCommand.h"
#include "ListMessagesUIFlow.h"

ListMessagesCommand::ListMessagesCommand(Application& app) :
		app(app) {
}

ListMessagesCommand::~ListMessagesCommand() {
}

void ListMessagesCommand::execute() {
	ListMessagesUIFlow flow(app.getAddessBook(), app.getConsole());
	flow.listMessages();
}

std::string ListMessagesCommand::getDisplayName() {
	return "List";
}
