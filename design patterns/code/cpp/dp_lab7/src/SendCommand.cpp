#include "SendCommand.h"
#include "SendMessageUIFlow.h"

SendCommand::SendCommand(Application& app, const std::string messageType) :
		app(app), messageType(messageType) {
}

SendCommand::~SendCommand() {
}

void SendCommand::execute() {
	SendMessageUIFlow flow(app.getAddessBook(), app.getConsole(),
			app.getEditorFactory(), app.getMessageFactory());
	flow.sendNewMessage(messageType);
}

std::string SendCommand::getDisplayName() {
	return string("Send ").append(messageType);
}
