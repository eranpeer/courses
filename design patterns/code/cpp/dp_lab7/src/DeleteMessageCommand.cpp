#include "DeleteMessageCommand.h"

DeleteMessageCommand::DeleteMessageCommand(Application& app) :
		app(app) {
}

DeleteMessageCommand::~DeleteMessageCommand() {
}

void DeleteMessageCommand::execute() {
	DeleteMessageUIFlow flow(app.getAddessBook(), app.getConsole());
	flow.deleteMessage();
}

std::string DeleteMessageCommand::getDisplayName() {
	return "Delete";
}
