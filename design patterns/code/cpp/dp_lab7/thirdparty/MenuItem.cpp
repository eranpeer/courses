#include "MenuItem.h"

namespace thirdparty {

MenuItem::MenuItem(string name) :
		name(name) {
}

MenuItem::~MenuItem() {
}

void MenuItem::click() {
}

string MenuItem::getName() const {
	return name;
}

void MenuItem::setName(const string name) {
	this->name = name;
}

}
