#include "Menu.h"
#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>

namespace thirdparty {

std::string format(int num) {
	std::stringstream stream;
	stream << num;
	return stream.str();
}

int parseInt(std::string strNum) {
	return std::atoi(strNum.data());
}

Menu::Menu(const string name) :
		size(0), name(name) {
}

void Menu::add(MenuItem* m) {
	if (size >= 10)
		return;
	items[size++] = m;
}

string Menu::getName() const {
	return name;
}

void Menu::setName(const string name) {
	this->name = name;
}

string ReadLine(const string prompt) {
	char buff[100];
	cout << prompt;
	cin.getline(buff, 100);
	string line = buff;
	return line;
}

void Menu::show() {
	int option = 0;
	cout << (name + "\n");
	for (int i = 0; i < size; i++) {
		cout << format(i + 1) << ":" << items[i]->getName() << "\n";
	}
	option = parseInt(ReadLine("==>"));
	if (option > 0 && option <= size)
		items[option - 1]->click();
}

}
