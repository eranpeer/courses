#pragma once

#include <string>
using namespace std;

#define N_MBX 20

class Mailbox;

class AddressBook {
private:
	Mailbox *mailboxes[N_MBX];
	int countMailboxes;
public:
	AddressBook();
	void addMailbox(Mailbox * mbx);
	Mailbox* getMailbox(const string address) const;
};

