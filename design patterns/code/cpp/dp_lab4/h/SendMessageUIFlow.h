#pragma once
#include <string.h>
#include "AddressBook.h"
#include "Console.h"
#include "MessageFactory.h"
#include "MessageEditorFactory.h"

class SendMessageUIFlow {
public:
	SendMessageUIFlow(AddressBook& addressBook, Console& console,
			MessageEditorFactory & editorFactory, MessageFactory& messageFactory);
	virtual ~SendMessageUIFlow();

	void sendNewMessage();
private:
	AddressBook& addressBook;
	Console& console;
	MessageEditorFactory& editorFactory;
	MessageFactory& messageFactory;
};
