#pragma once

class Application;

class Desktop {
public:
	Desktop(Application& application);
	~Desktop();

	void start();
	void stop();
private:
	Application& application;
	bool isActive;
};
