#pragma once

#include <string>
#include "Message.h"

class MessageFactory {
public:
	virtual ~MessageFactory() {
	}
	virtual Message* createMessage(const string& messageType) = 0;
};
