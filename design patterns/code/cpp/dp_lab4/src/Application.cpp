#include <stdlib.h>

#include "Application.h"
#include "MessageEditorFactoryImpl.h"
#include "MessageFactoryImpl.h"
#include "AddressBook.h"
#include "Mailbox.h"
#include "Desktop.h"

Application::Application(void) :
		desktop(*this) {
	editorFactory.reset(new MessageEditorFactoryImpl(addressBook, console));
	messageFactory.reset(new MessageFactoryImpl());
}

Application::~Application(void) {
}

void Application::start(void) {
	Mailbox eranMbx("eran");
	Mailbox ronenMbx("ronen");

	addressBook.addMailbox(&eranMbx);
	addressBook.addMailbox(&ronenMbx);

	desktop.start();
}

void Application::stop(void) {
	desktop.stop();
}

int main() {
	Application app;
	app.start();
	return 0;
}

AddressBook& Application::getAddessBook() {
	return addressBook;
}

Console& Application::getConsole() {
	return console;
}

MessageEditorFactory& Application::getEditorFactory() const {
	return *editorFactory.get();
}

MessageFactory& Application::getMessageFactory() const {
	return *messageFactory.get();
}
