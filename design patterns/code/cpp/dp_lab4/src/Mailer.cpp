#include "Mailer.h"

#include "Message.h"
#include <string>
#include "Mailbox.h"
#include "AddressBook.h"

Mailer::Mailer(AddressBook& anAddressBook) :
		addressBook(anAddressBook) {
}

Mailer::~Mailer() {
}

void Mailer::deliver(Message * m) const {
	string destination = m->getDestination();
	Mailbox *mbx = addressBook.getMailbox(destination);
	mbx->add(m);
}
