#include "MessageEditorFactoryImpl.h"
#include "LetterEditorAdapter.h"
#include "CableEditorAdapter.h"
#include "PostcardEditorAdapter.h"

MessageEditorFactoryImpl::MessageEditorFactoryImpl(AddressBook& addressBook,
		Console& console) :
		addressBook(addressBook), console(console) {
}

MessageEditor* MessageEditorFactoryImpl::createEditor(
		const string& messageType) {
	if (messageType == "letter") {
		return new LetterEditorAdapter(addressBook, console);
	}
	if (messageType == "cable") {
		return new CableEditorAdapter(addressBook, console);
	}
	if (messageType == "postcard") {
		return new PostcardEditorAdapter(addressBook, console);
	}
	return 0;
}
