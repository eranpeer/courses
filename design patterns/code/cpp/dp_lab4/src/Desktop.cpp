#include "Desktop.h"
#include "MenuItem.h"
#include "Menu.h"
#include "Application.h"
#include "SendMessageUIFlow.h"
#include "ListMessagesUIFlow.h"
#include "DeleteMessageUIFlow.h"

using namespace thirdparty;

class SendMenuItem: public MenuItem {
public:
	SendMenuItem(Application & app) :
			MenuItem("Send"), app(app) {
	}
	virtual void click() {
		SendMessageUIFlow flow(app.getAddessBook(), app.getConsole(), app.getEditorFactory(), app.getMessageFactory());
		flow.sendNewMessage();
	}
private:
	Application & app;
};

class ListMessagesMenuItem: public MenuItem {
public:
	ListMessagesMenuItem(Application & app) :
			MenuItem("List"), app(app) {
	}

	virtual void click() {
		ListMessagesUIFlow flow(app.getAddessBook(), app.getConsole());
		flow.listMessages();
	}
private:
	Application & app;
};

class DeleteMessageMenuItem: public MenuItem {
public:
	DeleteMessageMenuItem(Application & app) :
			MenuItem("Delete"), app(app) {
	}

	virtual void click() {
		DeleteMessageUIFlow flow(app.getAddessBook(), app.getConsole());
		flow.deleteMessage();
	}
private:
	Application & app;
};

class ExitMenuItem: public MenuItem {
public:
	ExitMenuItem(Application &application) :
			MenuItem("Exit"), application(application) {
	}

	virtual void click() {
		application.stop();
	}
private:
	Application &application;
};

Desktop::Desktop(Application &application) :
		application(application), isActive(false) {
}

Desktop::~Desktop() {
}

void Desktop::start() {
	Menu mainMenu("Select an option");
	mainMenu.add(new SendMenuItem(application));
	mainMenu.add(new DeleteMessageMenuItem(application));
	mainMenu.add(new ListMessagesMenuItem(application));
	mainMenu.add(new ExitMenuItem(application));

	isActive = true;;
	while (isActive) {
		mainMenu.show();
	}
}

void Desktop::stop() {
	isActive = false;
}
