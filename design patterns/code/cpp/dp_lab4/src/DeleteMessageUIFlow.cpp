#include "DeleteMessageUIFlow.h"
#include "Message.h"
#include "Letter.h"
#include "Cable.h"
#include "Postcard.h"
#include "Mailbox.h"
#include "TitleBuilder.h"
#include "Text.h"

DeleteMessageUIFlow::DeleteMessageUIFlow(AddressBook & addressBook,
		Console& console) :
		addressBook(addressBook), console(console) {
}

DeleteMessageUIFlow::~DeleteMessageUIFlow() {
}

void DeleteMessageUIFlow::deleteMessage() {
	string mbxAddress = console.ReadLine("Delete message from Mailbox:");
	Mailbox *mbx = addressBook.getMailbox(mbxAddress);
	if (!mbx) {
		console.Write(mbxAddress + " is not a valid address !\n");
		return;
	}
	int messageId = Text::parseInt(console.ReadLine("Enter message id:"));
	Message *theMsg = 0;
	if (!(theMsg = mbx->removeMsg(messageId)))
		console.Write("invalid message id !\n");
	else {
		TitleBuilder tb(console);
		theMsg->accept(tb);

		string confirmationLine;
		confirmationLine += "Message '";
		confirmationLine += tb.getTitle();
		confirmationLine += "' was deleted from ";
		confirmationLine += mbx->getAddress();
		confirmationLine += "'s mailbox";
		console.WriteLine(confirmationLine);

		delete (theMsg);
	}

}
