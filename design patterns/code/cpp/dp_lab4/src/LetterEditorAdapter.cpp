#include "LetterEditorAdapter.h"

LetterEditorAdapter::LetterEditorAdapter(AddressBook& anAddressBook,
		Console& console) :
		LetterEditor(anAddressBook, console) {
}

LetterEditorAdapter::~LetterEditorAdapter() {
}

bool LetterEditorAdapter::edit(Message* m) {
	Letter* l = dynamic_cast<Letter*>(m);
	if (l)
		return LetterEditor::edit(l);

	return false;
}
