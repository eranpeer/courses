#pragma once

#include "MenuItem.h"

namespace thirdparty {

class Menu {
public:
	Menu(string name);
	~Menu() {
	}

	string getName() const;
	void setName(const string name);

	void add(MenuItem* m);
	void show();

private:
	MenuItem *items[10];
	int size;
	string name;
};

}
