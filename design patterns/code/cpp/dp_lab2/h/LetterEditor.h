#pragma once

#include "AddressBook.h"
#include "Letter.h"
#include "Console.h"

class LetterEditor {
public:
	LetterEditor(AddressBook & anAddressBook, Console& console);
	virtual ~LetterEditor();

	bool edit(Letter *m) const;
private:
	AddressBook& addressBook;
	Console& console;
};
