#include <string>
using namespace std;

#pragma once

class Console {
public:
	string ReadLine(const string& prompt, const string& defaultValue);
	string ReadLine(const string& prompt);
	string ReadLine(const char * prompt);

	void Write(const string& text);
	void Write(const char* text);

	void WriteLine(const string& text);
	void WriteLine(const char* text);

	Console(void);
	~Console(void);
};
