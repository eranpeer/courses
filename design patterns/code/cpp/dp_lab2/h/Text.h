#pragma once
#include <string>

class Text {
public:
	static std::string format(int num);
	static int parseInt(std::string strNum);
private:
	Text(void){}
};
