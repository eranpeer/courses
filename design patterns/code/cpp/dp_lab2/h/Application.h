#pragma once

#include "AddressBook.h"
#include "Console.h"
#include "Desktop.h"

class Application {
public:
	Application(void);
	~Application(void);
	void start(void);
	void stop(void);

	AddressBook& getAddessBook();
	Console& getConsole();
private:
	AddressBook addressBook;
	Console console;
	Desktop desktop;
};
