#pragma once

#include <string>
using namespace std;

class Message {
public:
	Message();
	virtual ~Message();

	string getOrigin() const;
	void setOrigin(const string& origin);

	string getDestination() const;
	void setDestination(const string& destination);

private:
	string origin;
	string destination;
};
