#pragma once

#include <string>
#include "Message.h"

using namespace std;

class Postcard: public Message {
public:
	Postcard();
	~Postcard() {
	}

	string getText() const;
	void setText(const string& text);
private:
	string text;
};
