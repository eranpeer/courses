#pragma once

#include "Message.h"

#define MAX_MSG  100 

class Mailbox {
private:
	string address;
	int countMessages;
	Message* messages[MAX_MSG];

public:
	Mailbox(string const address);
	~Mailbox();

	void add(Message *msg);
	string getAddress() const;

	Message* removeMsg(int msgIndex);
	Message* getMessage(int index) const;
};
