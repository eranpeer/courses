#pragma once

#include "AddressBook.h"
#include "Postcard.h"
#include "Console.h"

class PostcardEditor {
public:
	PostcardEditor(AddressBook & anAddressBook, Console& console);
	virtual ~PostcardEditor();

	bool edit(Postcard *m) const ;
private:
	AddressBook& addressBook;
	Console& console;
};
