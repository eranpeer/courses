#include "PostcardEditor.h"
#include "MessageEditingUtils.h"
#include <string>

using namespace std;

PostcardEditor::PostcardEditor(AddressBook & addressBook, Console& console) :
		addressBook(addressBook), console(console) {
}

PostcardEditor::~PostcardEditor() {

}

bool PostcardEditor::edit(Postcard *m) const {
	if (!MessageEditingUtils::EditAddresses(m, addressBook))
		return 0;
	string messageText = console.ReadLine(string("Enter postcard's text:"),
			m->getText());
	m->setText(messageText);
	return 1;
}
