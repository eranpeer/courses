#include "SendMessageUIFlow.h"
#include "Mailer.h"
#include "Message.h"
#include "Letter.h"
#include "Cable.h"
#include "Postcard.h"
#include "LetterEditor.h"
#include "CableEditor.h"
#include "PostcardEditor.h"

SendMessageUIFlow::SendMessageUIFlow(AddressBook& addressBook, Console& console) :
		addressBook(addressBook), console(console) {
}

SendMessageUIFlow::~SendMessageUIFlow() {
}

void SendMessageUIFlow::sendNewMessage() {
	string messageType = console.ReadLine("Enter type of message:");

	if (messageType != "cable" && messageType != "letter"
			&& messageType != "postcard") {
		console.Write("invalid message type !\n");
		return;
	}

	Message* newMessage = 0;
	if (messageType == "letter") {
		newMessage = new Letter();
		LetterEditor editor(addressBook, console);
		if (!editor.edit((Letter*) (((newMessage)))))
			return;
	}
	if (messageType == "cable") {
		newMessage = new Cable();
		CableEditor editor(addressBook, console);
		if (!editor.edit((Cable*) (((newMessage)))))
			return;
	}
	if (messageType == "postcard") {
		newMessage = new Postcard();
		PostcardEditor editor(addressBook, console);
		if (!editor.edit((Postcard*) (((newMessage)))))
			return;
	}

	Mailer(addressBook).deliver(newMessage);
	console.Write(
			string("A new ") + messageType + " for "
					+ newMessage->getDestination() + "\n");
}
