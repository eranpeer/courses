#include "ListMessagesUIFlow.h"
#include "Mailbox.h"
#include "Message.h"
#include "Letter.h"
#include "Cable.h"
#include "Postcard.h"
#include "Text.h"
#include "Console.h"
#include <string.h>

ListMessagesUIFlow::ListMessagesUIFlow(AddressBook& addressBook,
		Console& console) :
		addressBook(addressBook), console(console) {
}

ListMessagesUIFlow::~ListMessagesUIFlow() {
}

void ListMessagesUIFlow::listMessages() {
	string address = console.ReadLine("Enter mailbox address:");
	Mailbox* mbx = addressBook.getMailbox(address);
	if (!mbx) {
		console.WriteLine("invalid destination mailbox");
		return;
	}

	console.WriteLine(string("Content of mailbox ") + mbx->getAddress() + ":");
	Message* currMsg = 0;
	for (int i = 0; (currMsg = mbx->getMessage(i)); i++) {
		string title;
		if (Letter* msg = dynamic_cast<Letter*>(currMsg)) {
			title = "Letter: ";
			title = title + msg->getSubject();
		}
		if (Cable* msg = dynamic_cast<Cable*>(currMsg)) {
			title = "Cable: ";
			title = title + msg->getContent();
		}
		if (Postcard* msg = dynamic_cast<Postcard*>(currMsg)) {
			title = "Postcard: from ";
			title = title + msg->getOrigin();
		}
		console.WriteLine(Text::format(i + 1) + ": " + title);
	}
}
