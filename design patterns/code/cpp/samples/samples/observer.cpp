#include "stdafx.h"
#include "CppUnitTest.h"
#include <list>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace samples
{

	class Subject;
	class Observer {
	public:
		virtual ~Observer() {};
		virtual void update(const Subject& aSubject) = 0;
	protected:
		Observer() {};
	};

	class Subject {
	public:
		virtual ~Subject() {};
		virtual void attach(Observer*);
		virtual void detach(Observer*);
		virtual void notify(); 
	protected:
		Subject() {};
	private:
		typedef std::list<Observer*> ObsList;
		typedef ObsList::iterator ObsListIter;
		ObsList mObservers;
	};

	void Subject::attach(Observer* obs) {
		mObservers.push_back(obs);
	}

	void Subject::detach(Observer* obs) {
		mObservers.remove(obs);
	}

	void Subject::notify() {
		for (ObsListIter i(mObservers.begin()); i != mObservers.end(); ++i) {
			(*i)->update(*this);
		}
	}

	class ClockTimer : public Subject {
	public:
		virtual int getHour() const { return 0; };
		virtual int getMinute() const { return 0; };
		virtual int getSecond() const { return 0; };
		void tick();
	};

	void ClockTimer::tick() {
		// update internal time-keeping state
		notify();
	}

	// Widget is a GUI class, with virtual function named draw
	class Widget { 
		virtual void draw() {/*...*/}
	};

	class DigitalClock : public Widget, public Observer {
	public:
		explicit DigitalClock(ClockTimer&);
		virtual ~DigitalClock();
		virtual void update(const Subject&) override;;
		virtual void draw() override;;
	private:
		ClockTimer& mSubject;
	};

	DigitalClock::DigitalClock(ClockTimer& subject) : mSubject(subject) {
		mSubject.attach(this);
	}

	DigitalClock::~DigitalClock() {
		mSubject.detach(this);
	}

	void DigitalClock::update(const Subject& theChangedSubject) {
		if (&theChangedSubject == &mSubject) {
			draw();
		}
	}

	void DigitalClock::draw() {
		int hour{ mSubject.getHour() };
		int minute{ mSubject.getMinute() };
		// draw ...
	}


	class AnalogClock : public Widget, public Observer {
	public:
		AnalogClock(ClockTimer& mSubject);
		virtual void update(const Subject&) override;
		virtual void draw() override;;
	private:		
		ClockTimer& mSubject;
	};

	AnalogClock::AnalogClock(ClockTimer& subject) : mSubject{subject} {
		mSubject.attach(this);
	}

	void AnalogClock::update(const Subject&)
	{
	}

	void AnalogClock::draw()
	{
	}




	TEST_CLASS(ObserverDemo)
	{
	public:
		
		TEST_METHOD(TestObserver)
		{
			ClockTimer timer;
			AnalogClock analogClock(timer);
			DigitalClock digitalClock(timer);
		}

	};



}