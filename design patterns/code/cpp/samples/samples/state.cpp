#include "stdafx.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace samples
{
	class VehState;

	class Vehicle {
	public:
		Vehicle();
		void turnOn();
		void engageGear();
		void disengageGear();
		void turnOff();
	private:
		friend class VehState;
		void changeState(VehState* newState);
	private:
		VehState* mState;
	};

	class VehState {
	public:
		virtual void turnOn(Vehicle&);
		virtual void engageGear(Vehicle&); 
		virtual void disengageGear(Vehicle&);
		virtual void turnOff(Vehicle&);
	protected:
		void changeState(Vehicle& veh, VehState* newState) { veh.changeState(newState); }
	};

	class MovingState : public VehState {
	public:
		static MovingState& theMovingState();
		virtual void disengageGear(Vehicle& veh);
	};

	class IdleState : public VehState {
	public:
		static IdleState& theIdleState(); 
		virtual void engageGear(Vehicle& veh) { changeState(veh, &MovingState::theMovingState()); }
	};

	class OffState : public VehState {
	public:
		static OffState& theOffState(); 
		virtual void turnOn(Vehicle& veh) { changeState(veh, &IdleState::theIdleState()); }
	};

	Vehicle::Vehicle() :
		mState(&OffState::theOffState()) { }
	
	void Vehicle::turnOn() {
		mState->turnOn(*this);
	}


	void Vehicle::engageGear() {
		mState->engageGear(*this);
	}

	void Vehicle::turnOff() {
		mState->turnOff(*this);
	}


	void Vehicle::changeState(VehState* newState) {
		mState = newState;
	}

	TEST_CLASS(StateDemo)
	{
	public:

		TEST_METHOD(TestState)
		{
		}
	};
}