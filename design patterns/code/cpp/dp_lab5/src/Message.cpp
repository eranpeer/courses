#include "Message.h"

Message::Message() {
}

Message::~Message() {
}

string Message::getOrigin() const {
	return origin;
}

string Message::getDestination() const {
	return destination;
}

void Message::setOrigin(const string& origin) {
	this->origin = origin;
}

void Message::setDestination(const string& destination) {
	this->destination = destination;
}
