#include <stdlib.h>

#include "Application.h"
#include "MessageEditorFactoryImpl.h"
#include "MessageFactoryImpl.h"
#include "AddressBook.h"
#include "Mailbox.h"
#include "Desktop.h"
#include "SendMessageUIFlow.h"
#include "ListMessagesUIFlow.h"
#include "DeleteMessageUIFlow.h"

class SendCommand: public Desktop::Command {
public:
	SendCommand(Application & app) :
			app(app) {
	}

	virtual ~SendCommand() {
	}

	void execute() {
		SendMessageUIFlow flow(app.getAddessBook(), app.getConsole(),
				app.getEditorFactory(), app.getMessageFactory());
		flow.sendNewMessage();
	}

	std::string getDisplayName() {
		return "Send";
	}
private:
	Application & app;
};

class ListMessagesCommand: public Desktop::Command {
public:
	ListMessagesCommand(Application & app) :
			app(app) {
	}

	virtual ~ListMessagesCommand() {
	}

	void execute() {
		ListMessagesUIFlow flow(app.getAddessBook(), app.getConsole());
		flow.listMessages();
	}

	std::string getDisplayName() {
		return "List";
	}

private:
	Application & app;
};

class DeleteMessageCommand: public Desktop::Command {
public:
	DeleteMessageCommand(Application & app) :
			app(app) {
	}

	virtual ~DeleteMessageCommand() {
	}

	void execute() {
		DeleteMessageUIFlow flow(app.getAddessBook(), app.getConsole());
		flow.deleteMessage();
	}

	std::string getDisplayName() {
		return "Delete";
	}

private:
	Application & app;
};

class ExitCommand: public Desktop::Command {
public:
	ExitCommand(Application &application) :
			application(application) {
	}

	virtual ~ExitCommand() {
	}

	virtual void execute() {
		application.stop();
	}

	std::string getDisplayName() {
		return "Exit";
	}

private:
	Application &application;
};

Application::Application(void) {
	editorFactory.reset(new MessageEditorFactoryImpl(addressBook, console));
	messageFactory.reset(new MessageFactoryImpl());
}

Application::~Application(void) {
}

void Application::start(void) {
	Mailbox eranMbx("eran");
	Mailbox ronenMbx("ronen");

	addressBook.addMailbox(&eranMbx);
	addressBook.addMailbox(&ronenMbx);

	SendCommand sendCommand(*this);
	DeleteMessageCommand deleteCommand(*this);
	ListMessagesCommand listCommand(*this);
	ExitCommand exitCommand(*this);

	desktop.addDesktopCommand(sendCommand);
	desktop.addDesktopCommand(deleteCommand);
	desktop.addDesktopCommand(listCommand);
	desktop.addDesktopCommand(exitCommand);

	desktop.start();
}

void Application::stop(void) {
	desktop.stop();
}

int main() {
	Application app;
	app.start();
	return 0;
}

AddressBook& Application::getAddessBook() {
	return addressBook;
}

Console& Application::getConsole() {
	return console;
}

MessageEditorFactory& Application::getEditorFactory() const {
	return *editorFactory.get();
}

MessageFactory& Application::getMessageFactory() const {
	return *messageFactory.get();
}
