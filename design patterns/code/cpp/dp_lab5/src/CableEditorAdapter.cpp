#include "CableEditorAdapter.h"

bool CableEditorAdapter::edit(Message* m) {
	Cable* c = dynamic_cast<Cable*>(m);
	if (c)
		return CableEditor::edit(c);

	return false;
}
