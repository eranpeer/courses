#include "Desktop.h"
#include "MenuItem.h"
#include "Menu.h"

using namespace thirdparty;

class CommandMenuItem: public MenuItem {
public:
	CommandMenuItem(Desktop::Command& command) :
			MenuItem(command.getDisplayName()), command(command) {
	}
	virtual void click() {
		command.execute();
	}
private:
	Desktop::Command& command;
};

Desktop::Desktop() :
		isActive(false), mainMenu("Select an option") {
}

Desktop::~Desktop() {
}

void Desktop::addDesktopCommand(Desktop::Command& command) {
	mainMenu.add(new CommandMenuItem(command));
}

void Desktop::start() {
	isActive = true;
	while (isActive) {
		mainMenu.show();
	}
}

void Desktop::stop() {
	isActive = false;
}
