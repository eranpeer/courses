#include "Mailbox.h"

Mailbox::Mailbox(string address) {
	countMessages = 0;
	this->address = address;
	messages[0] = 0;
}

Mailbox::~Mailbox() {
}

void Mailbox::add(Message *msg) {
	messages[countMessages++] = msg;
	messages[countMessages] = 0;
}

string Mailbox::getAddress() const {
	return address;
}

Message* Mailbox::removeMsg(int msgIndex) {
	Message *msg = getMessage(msgIndex);
	if (msg == 0)
		return 0;

	for (; msgIndex < countMessages; msgIndex++)
		messages[msgIndex] = messages[msgIndex + 1];

	if (countMessages)
		messages[--countMessages] = 0;

	return msg;
}

Message* Mailbox::getMessage(int index) const {
	if ((-1 < index) && (index < countMessages))
		return messages[index];
	return 0;
}
