#include "PostcardEditorAdapter.h"

bool PostcardEditorAdapter::edit(Message* m) {
	Postcard* p = dynamic_cast<Postcard*>(m);
	if (p)
		return PostcardEditor::edit(p);

	return false;
}
