#pragma once

#include "AddressBook.h"
#include "Console.h"
#include "MessageFactory.h"
#include "MessageEditorFactory.h"
#include "Desktop.h"
#include <memory>

class Application {
public:
	Application(void);
	~Application(void);

	void start(void);
	void stop(void);

	AddressBook& getAddessBook();
	Console& getConsole();
	MessageEditorFactory& getEditorFactory() const;
	MessageFactory& getMessageFactory() const;

private:
	AddressBook addressBook;
	Console console;
	Desktop desktop;
	std::auto_ptr<MessageEditorFactory> editorFactory;
	std::auto_ptr<MessageFactory> messageFactory;
};
