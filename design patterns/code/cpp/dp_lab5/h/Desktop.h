#pragma once

#include <string>
#include "Menu.h"

class Desktop {
public:

	class Command {
	public:
		~Command() {
		}
		virtual std::string getDisplayName() = 0;
		virtual void execute() = 0;
	};

	Desktop();
	~Desktop();

	void addDesktopCommand(Desktop::Command& command);

	void start();
	void stop();
private:
	bool isActive;
	thirdparty::Menu mainMenu;
};
