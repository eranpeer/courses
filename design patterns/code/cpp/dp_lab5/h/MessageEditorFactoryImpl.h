#pragma once

#include "MessageEditorFactory.h"
#include "AddressBook.h"
#include "Console.h"

class MessageEditorFactoryImpl: public MessageEditorFactory {
public:
	MessageEditorFactoryImpl(AddressBook& addressBook, Console& console);
	MessageEditor* createEditor(const string& messageType);
private:
	AddressBook& addressBook;
	Console& console;
};
