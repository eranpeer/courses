#pragma once

#include <string>
#include "Message.h"

using namespace std;

class Cable: public Message {
public:
	Cable();
	virtual ~Cable(){}

	string getContent() const;
	void setContent(const string& content);
private:
	string content;
};
