#pragma once

#include "AddressBook.h"
#include "Desktop.h"

class Application {
public:
	Application(void);
	~Application(void);
	void start(void);
	void stop(void);
	AddressBook& getAddessBook();
private:
	AddressBook addressBook;
	Desktop desktop;
};
