#pragma once

#include "AddressBook.h"
#include "Cable.h"
#include "Console.h"

class CableEditor {
public:
	CableEditor(AddressBook & anAddressBook);
	~CableEditor();

	bool edit(Cable *m) const;

private:
	AddressBook & addressBook;
};
