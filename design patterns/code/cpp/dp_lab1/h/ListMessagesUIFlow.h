#pragma once

#include "AddressBook.h"

class ListMessagesUIFlow {
public:
	ListMessagesUIFlow(AddressBook& addressBook);
	~ListMessagesUIFlow();

	void listMessages();

private:
	AddressBook & addressBook;
};
