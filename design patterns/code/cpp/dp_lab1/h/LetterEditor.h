#pragma once

#include "AddressBook.h"
#include "Letter.h"

class LetterEditor {
public:
	LetterEditor(AddressBook & anAddressBook);
	virtual ~LetterEditor();

	bool edit(Letter *m) const;
private:
	AddressBook & addressBook;
};
