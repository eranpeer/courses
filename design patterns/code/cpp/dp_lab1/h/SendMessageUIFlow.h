#pragma once
#include <string.h>
#include "AddressBook.h"

class SendMessageUIFlow {
public:
	SendMessageUIFlow(AddressBook& addressBook);
	virtual ~SendMessageUIFlow();

	void sendNewMessage();
private:
	AddressBook& addressBook;
};
