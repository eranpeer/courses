#pragma once

#include "AddressBook.h"

class DeleteMessageUIFlow {
public:
	DeleteMessageUIFlow(AddressBook & addressBook);
	~DeleteMessageUIFlow();

	void deleteMessage();
private:
	AddressBook & addressBook;
};
