#pragma once

#include "AddressBook.h"
#include "Postcard.h"

class PostcardEditor {
public:
	PostcardEditor(AddressBook & anAddressBook);
	virtual ~PostcardEditor();

	bool edit(Postcard *m) const;
private:
	AddressBook & addressBook;
};
