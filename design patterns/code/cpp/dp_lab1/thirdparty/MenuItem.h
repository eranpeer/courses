#pragma once

#include <string>
using namespace std;

namespace thirdparty {

class MenuItem {
public:
	MenuItem(string name);

	virtual ~MenuItem();

	virtual void click();

	string getName() const;

	void setName(const string name);
private:
	string name;
};

}
