#include "DeleteMessageUIFlow.h"
#include "Message.h"
#include "Letter.h"
#include "Cable.h"
#include "Postcard.h"
#include "Mailbox.h"
#include "Console.h"
#include "Text.h"

DeleteMessageUIFlow::DeleteMessageUIFlow(AddressBook & addressBook) :
		addressBook(addressBook) {
}

DeleteMessageUIFlow::~DeleteMessageUIFlow() {
}

void DeleteMessageUIFlow::deleteMessage() {
	Console console;
	string mbxAddress = console.ReadLine("Delete message from Mailbox:");
	Mailbox *mbx = addressBook.getMailbox(mbxAddress);
	if (!mbx) {
		console.Write(mbxAddress + " is not a valid address !\n");
		return;
	}
	int messageId = Text::parseInt(console.ReadLine("Enter message id:"));
	Message *theMsg = 0;
	if (!(theMsg = mbx->removeMsg(messageId)))
		console.Write("invalid message id !\n");
	else {
		string deleteMessage;
		string title;
		if (Letter *msg = dynamic_cast<Letter*>(theMsg)) {
			title = "Letter: ";
			title = title + msg->getSubject();
		}

		if (Cable *msg = dynamic_cast<Cable*>(theMsg)) {
			title = "Cable: ";
			title = title + msg->getContent();
		}

		if (Postcard *msg = dynamic_cast<Postcard*>(theMsg)) {
			title = "Postcard: from ";
			title = title + msg->getOrigin();
		}

		deleteMessage += "Message '";
		deleteMessage += title;
		deleteMessage += "' was deleted from ";
		deleteMessage += mbx->getAddress();
		deleteMessage += "'s mailbox\n";

		delete (theMsg);

		console.Write(deleteMessage);
	}

}
