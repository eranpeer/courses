#include "Text.h"
#include <sstream>
#include <string>
#include <cstdlib>

std::string Text::format(int num) {
	std::stringstream stream;
	stream << num;
	return stream.str();
}

int Text::parseInt(std::string strNum) {
	return std::atoi(strNum.data());
}
