#include "CableEditor.h"
#include "MessageEditingUtils.h"
#include "Console.h"
#include <string>

using namespace std;

CableEditor::CableEditor(AddressBook & anAddressBook) :
		addressBook(anAddressBook) {
}

CableEditor::~CableEditor() {
}

bool CableEditor::edit(Cable *m) const {
	if (!MessageEditingUtils::EditAddresses(m, addressBook))
		return false;
	Console console;
	string messageContent = console.ReadLine(string("Enter cable's content:"),
			m->getContent());
	m->setContent(messageContent);
	return true;
}
