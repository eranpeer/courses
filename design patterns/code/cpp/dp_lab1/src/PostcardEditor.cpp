#include "PostcardEditor.h"
#include "MessageEditingUtils.h"
#include "Console.h"
#include <string>

using namespace std;

PostcardEditor::PostcardEditor(AddressBook & addressBook) :
		addressBook(addressBook) {
}

PostcardEditor::~PostcardEditor() {

}

bool PostcardEditor::edit(Postcard *m) const {
	if (!MessageEditingUtils::EditAddresses(m, addressBook))
		return false;
	Console console;
	string messageText = console.ReadLine(string("Enter postcard's text:"),
			m->getText());
	m->setText(messageText);
	return true;
}
