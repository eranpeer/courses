#include "LetterEditor.h"
#include "MessageEditingUtils.h"
#include "Console.h"
#include <string>

LetterEditor::LetterEditor(AddressBook & anAddressBook) :
		addressBook(anAddressBook) {
}
LetterEditor::~LetterEditor() {
}

bool LetterEditor::edit(Letter *m) const {
	if (!MessageEditingUtils::EditAddresses(m, addressBook))
		return false;

	Console console;
	string messageSubject = console.ReadLine(string("Enter letter's subject:"),
			m->getSubject());
	m->setSubject(messageSubject);
	string messageContent = console.ReadLine(string("Enter letter's content:"),
			m->getContent());
	m->setContent(messageContent);

	return true;
}
