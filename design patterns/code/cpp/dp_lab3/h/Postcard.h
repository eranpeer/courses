#pragma once

#include <string>
#include "Message.h"
#include "MessageVisitor.h"

using namespace std;

class Postcard: public Message {
public:
	Postcard();
	~Postcard() {
	}

	string getText() const;
	void setText(const string& text);

	void accept(MessageVisitor& v);

private:
	string text;
};
