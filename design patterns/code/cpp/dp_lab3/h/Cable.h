#pragma once

#include "Message.h"
#include "MessageVisitor.h"

#include <string>

using namespace std;

class Cable: public Message {
public:
	Cable();
	virtual ~Cable(){}

	string getContent() const;
	void setContent(const string& content);

	void accept(MessageVisitor& v);

private:
	string content;
};
