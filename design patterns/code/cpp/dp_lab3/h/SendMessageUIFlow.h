#pragma once
#include <string.h>
#include "AddressBook.h"
#include "Console.h"

class SendMessageUIFlow {
public:
	SendMessageUIFlow(AddressBook& addressBook, Console& console);
	virtual ~SendMessageUIFlow();

	void sendNewMessage();
private:
	AddressBook& addressBook;
	Console& console;
};
