#pragma once

class Letter;
class Cable;
class Postcard;

class MessageVisitor {
public:
	virtual ~MessageVisitor();
	virtual void visit(Letter* msg) = 0;
	virtual void visit(Cable* msg)= 0;
	virtual void visit(Postcard* msg)= 0;
};
