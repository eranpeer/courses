#include <stdlib.h>

#include "Application.h"
#include "AddressBook.h"
#include "Mailbox.h"

Application::Application(void) :
		desktop(*this) {
}

Application::~Application(void) {
}

void Application::start(void) {
	Mailbox eranMbx("eran");
	Mailbox ronenMbx("ronen");

	addressBook.addMailbox(&eranMbx);
	addressBook.addMailbox(&ronenMbx);

	desktop.start();
}

void Application::stop(void) {
	desktop.stop();
}

int main() {
	Application app;
	app.start();
	return 0;
}

AddressBook& Application::getAddessBook() {
	return addressBook;
}

Console& Application::getConsole() {
	return console;
}
