package thirdparty;

public class MenuItem {
	private String name;

	public MenuItem(String name) {
		this.name = name;
	}

	public void click() {
	}

	public String getName() {
		return name;
	}
}
