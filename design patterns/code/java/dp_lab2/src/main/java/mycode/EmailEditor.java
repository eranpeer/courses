package mycode;

public class EmailEditor {

	private AddressBook addressBook;
	private Console console;

	public EmailEditor(AddressBook addressBook, Console console) {
		this.addressBook = addressBook;
		this.console = console;
	}

	public boolean edit(Email msg) {
		if (!MessageEditingUtils.editAddresses(msg, addressBook, console))
			return false;
		msg.setSubject(console.readLine("Enter email's subject:", msg.getSubject()));
		msg.setContent(console.readLine("Enter email's content:", msg.getContent()));
		return true;
	}

}
