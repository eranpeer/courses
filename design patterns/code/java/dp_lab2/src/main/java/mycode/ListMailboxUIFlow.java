package mycode;

public class ListMailboxUIFlow {

	private AddressBook addressBook;
	private Console console;

	public ListMailboxUIFlow(Console console, AddressBook addressBook) {
		this.console = console;
		this.addressBook = addressBook;
	}

	public void listMailbox() {

		Mailbox mbx = null;
		String address = console.readLine("Enter mailbox address:");
		mbx = addressBook.getMailbox(address);
		if (mbx == null) {
			System.out.println("invalid sender mailbox");
			return;
		}

		// list the content of the mailbox.
		Message messages[] = mbx.getMessages();
		console.show("Content of mailbox " + mbx.getAddress() + ":");
		for (int i = 0; i < messages.length; i++) {
			String title = null;
			if (messages[i] instanceof Email) {
				Email msg = (Email) messages[i];
				title = "Email: " + msg.getSubject();
			}
			if (messages[i] instanceof Text) {
				Text msg = (Text) messages[i];
				title = "Text: " + msg.getContent();
			}
			if (messages[i] instanceof Voice) {
				Voice msg = (Voice) messages[i];
				title = "Voice: from " + msg.getOrigin();
			}
			title = (i + 1) + ": " + title;
			console.show(title);
		}
	}
}