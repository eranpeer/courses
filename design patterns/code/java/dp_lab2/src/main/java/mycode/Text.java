package mycode;

public class Text extends Message {

	private String content = "";

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		if (content == null)
			throw new IllegalArgumentException();
		this.content = content;
	}
}
