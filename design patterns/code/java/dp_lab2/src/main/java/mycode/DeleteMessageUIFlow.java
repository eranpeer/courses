package mycode;

public class DeleteMessageUIFlow {

	private AddressBook addressBook;
	private Console console;

	public DeleteMessageUIFlow(Console console, AddressBook addressBook) {
		this.console = console;
		this.addressBook = addressBook;
	}

	private String buildTitle(Message message) {
		String title = null;
		if (message instanceof Email) {
			Email msg = (Email) message;
			title = "Email: " + msg.getSubject();
		}
		if (message instanceof Text) {
			Text msg = (Text) message;
			title = "Text: " + msg.getContent();
		}
		if (message instanceof Voice) {
			Voice msg = (Voice) message;
			title = "Voice: from " + msg.getOrigin();
		}
		return title;
	}

	public void deleteMessage() {

		String mbxAddress = console.readLine("Delete message from mailbox:");
		Mailbox mbx = addressBook.getMailbox(mbxAddress);

		if (mbx == null) {
			console.show(mbxAddress + " is not a valid mailbox address !");
			return;
		}

		try {
			Integer messageId = new Integer(console.readLine("Enter message id:"));
			Message deletedMsg = mbx.delete(messageId.intValue());
			if (deletedMsg != null)
				console.show(buildTitle(deletedMsg) + " was deleted from mailbox " + mbxAddress);
			else {
				console.show("invalid message id!");
			}
		} catch (NumberFormatException e) {
			console.show("invalid message id!");
		}
	}

}