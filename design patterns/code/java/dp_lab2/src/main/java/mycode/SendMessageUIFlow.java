package mycode;

import java.util.Arrays;

public class SendMessageUIFlow {

	private AddressBook addressBook;
	private Console console;

	public SendMessageUIFlow(Console console, AddressBook addressBook) {
		this.console = console;
		this.addressBook = addressBook;
	}

	public void sendMessage() {
		String messageType = console.readLine("Enter type of message:");
		if (!Arrays.asList("email", "text", "voice").contains(messageType)) {
			console.show("invalid message type !");
			return;
		}

		Message newMessage = null;
		if (messageType.equals("email")) {
			newMessage = new Email();
			EmailEditor editor = new EmailEditor(addressBook, console);
			if (!editor.edit((Email) newMessage))
				return;
		}

		if (messageType.equals("text")) {
			newMessage = new Text();
			TextEditor editor = new TextEditor(addressBook, console);
			if (!editor.edit((Text) newMessage))
				return;
		}

		if (messageType.equals("voice")) {
			newMessage = new Voice();
			VoiceEditor editor = new VoiceEditor(addressBook, console);
			if (!editor.edit((Voice) newMessage))
				return;
		}

		new Mailer(addressBook).deliver(newMessage);
		console.show("New " + messageType + " for " + newMessage.getDestination() + "!");
	}

}