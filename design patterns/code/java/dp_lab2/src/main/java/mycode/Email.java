package mycode;

public class Email extends Message {

	private String content = "";

	private String subject = "";

	public String getContent() {
		return content;
	}

	public String getSubject() {
		return subject;
	}

	void setContent(String content) {
		if (content == null)
			throw new IllegalArgumentException();
		this.content = content;
	}

	void setSubject(String subject) {
		if (subject == null)
			throw new IllegalArgumentException();
		this.subject = subject;
	}
}