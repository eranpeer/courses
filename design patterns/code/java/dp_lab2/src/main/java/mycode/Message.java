package mycode;

public abstract class Message {

	private String destination = "";
	private String origin = "";

	public String getDestination() {
		return destination;
	}

	public String getOrigin() {
		return origin;
	}

	public void setDestination(String address) {
		if (address == null)
			throw new IllegalArgumentException();
		destination = address;
	}

	public void setOrigin(String address) {
		if (address == null)
			throw new IllegalArgumentException();
		origin = address;
	}

}