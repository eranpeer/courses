package mycode;

public class EmailEditorAdapter extends EmailEditor implements MsgEditor {

	public EmailEditorAdapter(AddressBook addressBook, Console console) {
		super(addressBook, console);
	}

	public boolean edit(Message m) {
		return super.edit((Email) m);
	}
}