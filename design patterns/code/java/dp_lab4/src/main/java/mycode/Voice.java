package mycode;

public class Voice extends Message {

	private String text = "";

	@Override
	public <T> T accept(MsgVisitor<T> v) {
		return v.visit(this);
	}

	public String getText() {
		return text;
	}

	public void setText(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		this.text = value;
	}
}