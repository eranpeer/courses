package mycode;

public interface MsgVisitor<T> {

	T visit(Text msg);

	T visit(Email msg);

	T visit(Voice msg);

}