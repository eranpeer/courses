package mycode;

public class ListMailboxUIFlow {

	private AddressBook addressBook;
	private Console console;
	private TitleBuilderVisitor tb;

	public ListMailboxUIFlow(Console console, AddressBook addressBook) {
		this.console = console;
		this.addressBook = addressBook;
		this.tb = new TitleBuilderVisitor();
	}

	public void listMailbox() {

		Mailbox mbx = null;
		String address = console.readLine("Enter mailbox address:");
		mbx = addressBook.getMailbox(address);
		if (mbx == null) {
			System.out.println("invalid sender mailbox");
			return;
		}

		// list the content of the mailbox.
		Message messages[] = mbx.getMessages();
		console.show("Content of mailbox " + mbx.getAddress() + ":");
		for (int i = 0; i < messages.length; i++) {
			String title = (i + 1) + ": " + messages[i].accept(tb);
			console.show(title.toString());
		}
	}

}