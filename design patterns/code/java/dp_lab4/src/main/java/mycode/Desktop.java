package mycode;

import thirdparty.Menu;
import thirdparty.MenuItem;

public class Desktop {

	private class DeleteMessageMenuItem extends MenuItem {

		public DeleteMessageMenuItem() {
			super("Delete Message");
		}

		@Override
		public void click() {
			new DeleteMessageUIFlow(application.getConsole(), application.getAddressBook()).deleteMessage();
		}
	}

	private class ExitMenuItem extends MenuItem {

		public ExitMenuItem() {
			super("Exit");
		}

		@Override
		public void click() {
			application.stop();
		}
	}

	private class ListMessagesMenuItem extends MenuItem {

		public ListMessagesMenuItem() {
			super("List Messages");
		}

		@Override
		public void click() {
			new ListMailboxUIFlow(application.getConsole(), application.getAddressBook()).listMailbox();
		}
	}

	private class SendMenuItem extends MenuItem {

		public SendMenuItem() {
			super("Send Message");
		}

		@Override
		public void click() {
			new SendMessageUIFlow(application.getConsole(), application.getAddressBook(), new MessageFactoryImpl(),
					new MsgEditorFactoryImpl(application.getAddressBook(), application.getConsole())).sendMessage();
		}
	}

	private final Application application;

	private boolean isActive;

	public Desktop(Application app) {
		this.application = app;
	}

	public void start() {
		Menu mainMenu = new Menu("Select an option:");

		mainMenu.addItem(new SendMenuItem());
		mainMenu.addItem(new DeleteMessageMenuItem());
		mainMenu.addItem(new ListMessagesMenuItem());
		mainMenu.addItem(new ExitMenuItem());

		isActive = true;
		while (isActive) {
			mainMenu.show();
		}
	}

	public void stop() {
		isActive = false;
	}

}