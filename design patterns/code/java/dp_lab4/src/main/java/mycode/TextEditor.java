package mycode;

public class TextEditor extends java.lang.Object {

	private AddressBook addressBook;
	private Console console;

	public TextEditor(AddressBook addressBook, Console console) {
		this.addressBook = addressBook;
		this.console = console;
	}

	public boolean edit(Text msg) {
		if (!MessageEditingUtils.editAddresses(msg, addressBook, console))
			return false;
		msg.setContent(console.readLine("Enter text's content:", msg.getContent()));
		return true;
	}
}