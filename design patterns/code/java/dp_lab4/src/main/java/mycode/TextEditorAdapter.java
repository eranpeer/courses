package mycode;

public class TextEditorAdapter extends TextEditor implements MsgEditor {

	public TextEditorAdapter(AddressBook addressBook, Console console) {
		super(addressBook, console);
	}

	public boolean edit(Message m) {
		return super.edit((Text) m);
	}

}