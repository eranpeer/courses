package mycode;

public class SendMessageUIFlow {

	private AddressBook addressBook;
	private Console console;
	private MessageFactory messageFactory;
	private MsgEditorFactory msgEditorFactory;

	public SendMessageUIFlow(Console console, AddressBook addressBook, MessageFactory messageFactory,
			MsgEditorFactory msgEditorFactory) {
		this.console = console;
		this.addressBook = addressBook;
		this.messageFactory = messageFactory;
		this.msgEditorFactory = msgEditorFactory;
	}

	public void sendMessage() {
		String messageType = console.readLine("Enter type of message:");
		sendMessage(messageType);
	}

	public void sendMessage(String messageType) {
		Message newMessage = messageFactory.createMessage(messageType);
		if (newMessage == null) {
			console.show("invalid message type !");
			return;
		}

		MsgEditor editor = msgEditorFactory.createMsgEditor(messageType);
		if (!editor.edit(newMessage))
			return;

		new Mailer(addressBook).deliver(newMessage);
		console.show("New " + messageType + " for " + newMessage.getDestination() + "!");
	}

}