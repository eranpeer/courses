package mycode;

public class ChangeDefaultsCommand extends Desktop.LeafCommand {
	private final String messageType;
	private EditPrototypeUIFlow uiFlow;

	public ChangeDefaultsCommand(Application application, String messageType) {
		this.messageType = messageType;
		uiFlow = new EditPrototypeUIFlow(application.getConsole(), (Prototypes) application.getMessageFactory(),
				application.getEditorFactory());
	}

	@Override
	public void execute() {
		uiFlow.editPrototype(messageType);
	}

	@Override
	public String getDisplayName() {
		return "Change " + messageType + " default values";
	}

}