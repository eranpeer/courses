package mycode;

public class DeleteMessageCommand extends Desktop.LeafCommand {

	private DeleteMessageUIFlow uiFlow;

	public DeleteMessageCommand(Application application) {
		uiFlow = new DeleteMessageUIFlow(application.getConsole(), application.getAddressBook());
	}

	@Override
	public void execute() {
		uiFlow.deleteMessage();
	}

	@Override
	public String getDisplayName() {
		return "Delete Message";
	}
}