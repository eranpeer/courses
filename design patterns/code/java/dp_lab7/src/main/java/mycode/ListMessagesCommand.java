package mycode;

public class ListMessagesCommand extends Desktop.LeafCommand {

	private ListMailboxUIFlow uiFlow;

	public ListMessagesCommand(Application application) {
		uiFlow = new ListMailboxUIFlow(application.getConsole(), application.getAddressBook());
	}

	@Override
	public void execute() {
		uiFlow.listMailbox();
	}

	@Override
	public String getDisplayName() {
		return "List Messages";
	}
}