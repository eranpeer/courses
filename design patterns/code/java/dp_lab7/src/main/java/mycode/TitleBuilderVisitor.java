package mycode;

public class TitleBuilderVisitor implements MsgVisitor<String> {

	@Override
	public String visit(Text msg) {
		return "Text: " + msg.getContent();
	}

	@Override
	public String visit(Email msg) {
		return "Email: " + msg.getSubject();
	}

	@Override
	public String visit(Voice msg) {
		return "Voice: from " + msg.getOrigin();
	}

}