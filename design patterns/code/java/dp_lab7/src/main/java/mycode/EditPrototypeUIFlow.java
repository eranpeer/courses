package mycode;

public class EditPrototypeUIFlow {

	private Console console;
	private final MsgEditorFactory msgEditorFactory;
	private Prototypes prototypes;

	public EditPrototypeUIFlow(Console console, Prototypes prototypes, MsgEditorFactory msgEditorFactory) {
		this.console = console;
		this.prototypes = prototypes;
		this.msgEditorFactory = msgEditorFactory;
	}

	public void editPrototype() {
		String messageType = console.readLine("Enter type of message:");
		editPrototype(messageType);
	}

	public void editPrototype(String messageType) {
		Message newMessage = prototypes.getPrototype(messageType);
		if (newMessage == null) {
			console.show("invalid message type !");
			return;
		}

		console.show("Please fill default values for " + messageType + ":");
		MsgEditor editor = msgEditorFactory.createMsgEditor(messageType);
		editor.edit(newMessage);
	}

}