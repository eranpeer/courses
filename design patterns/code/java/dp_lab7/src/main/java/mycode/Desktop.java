package mycode;

import thirdparty.Menu;
import thirdparty.MenuItem;

public class Desktop {

	public static interface Command {

		boolean addCommand(Command c);

		void execute();

		String getDisplayName();
	}

	private static class CommandMenuItem extends MenuItem {

		private final Desktop.Command command;

		public CommandMenuItem(Desktop.Command command) {
			super(command.getDisplayName());
			this.command = command;
		}

		@Override
		public void click() {
			command.execute();
		}
	}

	public static abstract class LeafCommand implements Command {

		@Override
		public boolean addCommand(Command c) {
			return false;
		}
	}

	private static class MenuCommand implements Command {
		private final Menu menu;

		public MenuCommand(String displayName) {
			menu = new Menu(displayName);
		}

		@Override
		public boolean addCommand(Command command) {
			menu.addItem(new CommandMenuItem(command));
			return true;
		}

		@Override
		public void execute() {
			menu.show();
		}

		@Override
		public String getDisplayName() {
			return menu.getName();
		}

		private Menu getMenu() {
			return menu;
		}
	}

	private boolean isActive;
	private final MenuCommand mainMenu;

	public Desktop() {
		mainMenu = new MenuCommand("Select an option:");
	}

	public void addCommand(Desktop.Command command) {
		mainMenu.addCommand(command);
	}

	public Command createMenuCommand(String displayName) {
		return new MenuCommand(displayName);
	}

	public void start() {
		isActive = true;
		while (isActive) {
			mainMenu.getMenu().show();
		}
	}

	public void stop() {
		isActive = false;
	}

}