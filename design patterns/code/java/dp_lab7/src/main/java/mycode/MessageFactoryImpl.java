package mycode;

import java.util.HashMap;

public class MessageFactoryImpl implements MessageFactory, Prototypes {

	private HashMap<String, Message> prototypes = new HashMap<String, Message>();

	public MessageFactoryImpl() {
	}

	public void addPrototype(String messageType, final Message prototype) {
		prototypes.put(messageType, prototype);
	}

	@Override
	public Message createMessage(String messageType) {
		Message prototype = getPrototype(messageType);
		if (prototype != null)
			return prototype.clone();
		return null;
	}

	@Override
	public Message getPrototype(String messageType) {
		return prototypes.get(messageType);
	}

}