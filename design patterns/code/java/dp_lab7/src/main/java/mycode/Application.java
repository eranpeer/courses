package mycode;

public class Application {

	public static void main(String[] args) {
		new Application().start();
	}

	private final AddressBook addressBook;
	private final Console console;
	private final Desktop desktop;
	private final MessageFactoryImpl messageFactory;

	private final MsgEditorFactoryImpl msgEditorFactory;

	public Application() {
		addressBook = new AddressBook();
		console = new Console();
		messageFactory = new MessageFactoryImpl();
		msgEditorFactory = new MsgEditorFactoryImpl(addressBook, console);

		messageFactory.addPrototype("email", new Email());
		messageFactory.addPrototype("text", new Text());
		messageFactory.addPrototype("voice", new Voice());
		desktop = new Desktop();

	}

	public AddressBook getAddressBook() {
		return addressBook;
	}

	public Console getConsole() {
		return console;
	}

	public MsgEditorFactory getEditorFactory() {
		return msgEditorFactory;
	}

	public MessageFactory getMessageFactory() {
		return messageFactory;
	}

	private void initDesktopCommands(Desktop desktop) {
		Desktop.Command sendCommand = desktop.createMenuCommand("Send message");
		sendCommand.addCommand(new SendDesktopCommand(this, "email"));
		sendCommand.addCommand(new SendDesktopCommand(this, "text"));
		sendCommand.addCommand(new SendDesktopCommand(this, "voice"));

		Desktop.Command setDefaultsCommand = desktop.createMenuCommand("Set defaults");
		setDefaultsCommand.addCommand(new ChangeDefaultsCommand(this, "email"));
		setDefaultsCommand.addCommand(new ChangeDefaultsCommand(this, "text"));
		setDefaultsCommand.addCommand(new ChangeDefaultsCommand(this, "voice"));

		desktop.addCommand(sendCommand);
		desktop.addCommand(new DeleteMessageCommand(this));
		desktop.addCommand(new ListMessagesCommand(this));
		desktop.addCommand(setDefaultsCommand);
		desktop.addCommand(new ExitCommand(this));
	}

	public void start() {
		Mailbox eranMbx = new Mailbox("eran");
		Mailbox ronenMbx = new Mailbox("ronen");

		addressBook.addMailbox(eranMbx);
		addressBook.addMailbox(ronenMbx);

		initDesktopCommands(desktop);
		desktop.start();
	}

	public void stop() {
		desktop.stop();
	}

}