package mycode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Mailbox implements Iterable<Message> {

	private String address;

	private List<Message> messages;

	public Mailbox(String address) {
		this.address = address;
		messages = new ArrayList<Message>();
	}

	public void add(Message msg) {
		messages.add(msg);
	}

	public Message delete(int msgId) {
		if (msgId < 0 || msgId >= messages.size())
			return null;
		return messages.remove(msgId);
	}

	public void delete(Message msg) {
		messages.remove(msg);
	}

	public String getAddress() {
		return address;
	}

	public Message[] getMessages() {
		return this.messages.toArray(new Message[0]);
	}

	@Override
	public Iterator<Message> iterator() {
		return messages.iterator();
	}

}