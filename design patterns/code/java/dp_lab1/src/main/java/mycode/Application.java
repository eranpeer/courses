package mycode;

public class Application {

	public static void main(String[] args) {
		new Application().start();
	}

	private final AddressBook addressBook;

	private final Desktop desktop;

	public Application() {
		addressBook = new AddressBook();
		desktop = new Desktop(this);
	}

	public AddressBook getAddressBook() {
		return addressBook;
	}

	public void start() {

		Mailbox eranMbx = new Mailbox("eran");
		Mailbox ronenMbx = new Mailbox("ronen");

		addressBook.addMailbox(eranMbx);
		addressBook.addMailbox(ronenMbx);

		desktop.start();
	}

	public void stop() {
		desktop.stop();
	}
}