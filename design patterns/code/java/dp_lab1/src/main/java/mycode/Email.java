package mycode;

public class Email extends Message {

	private String content = "";

	private String subject = "";

	public String getContent() {
		return content;
	}

	public String getSubject() {
		return subject;
	}

	void setContent(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		this.content = value;
	}

	void setSubject(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		this.subject = value;
	}
}