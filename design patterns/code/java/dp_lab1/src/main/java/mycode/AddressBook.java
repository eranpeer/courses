package mycode;

import java.util.Hashtable;

public class AddressBook {

	private Hashtable<String, Mailbox> content;

	public AddressBook() {
		content = new Hashtable<String, Mailbox>();
	}

	public void addMailbox(Mailbox mbx) {
		content.put(mbx.getAddress(), mbx);
	}

	public Mailbox getMailbox(String address) {
		return content.get(address);
	}

}