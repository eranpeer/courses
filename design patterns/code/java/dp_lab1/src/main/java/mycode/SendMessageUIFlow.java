package mycode;

import java.util.Arrays;

public class SendMessageUIFlow {

	private AddressBook addressBook;
	private Console console;

	public SendMessageUIFlow(AddressBook addressBook) {
		this.console = new Console();
		this.addressBook = addressBook;
	}

	public void sendMessage() {
		String messageType = console.readLine("Enter type of message:");
		if (!Arrays.asList("email", "text", "voice").contains(messageType)) {
			console.show("invalid message type !");
			return;
		}

		Message newMessage = null;
		if (messageType.equals("email")) {
			newMessage = new Email();
			EmailEditor editor = new EmailEditor(addressBook);
			if (!editor.edit((Email) newMessage))
				return;
		}

		if (messageType.equals("text")) {
			newMessage = new Text();
			TextEditor editor = new TextEditor(addressBook);
			if (!editor.edit((Text) newMessage))
				return;
		}

		if (messageType.equals("voice")) {
			newMessage = new Voice();
			VoiceEditor editor = new VoiceEditor(addressBook);
			if (!editor.edit((Voice) newMessage))
				return;
		}

		new Mailer(addressBook).deliver(newMessage);
		console.show("New " + messageType + " for " + newMessage.getDestination() + "!");
	}

}