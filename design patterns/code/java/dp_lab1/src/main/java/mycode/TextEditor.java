package mycode;

public class TextEditor extends java.lang.Object {

	private AddressBook addressBook;

	public TextEditor(AddressBook addressBook) {
		this.addressBook = addressBook;
	}

	public boolean edit(Text msg) {
		Console console = new Console();
		if (!MessageEditingUtils.editAddresses(msg, addressBook, console))
			return false;
		msg.setContent(console.readLine("Enter text's content:", msg.getContent()));
		return true;
	}
}