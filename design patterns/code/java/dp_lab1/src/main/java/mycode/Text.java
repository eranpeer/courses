package mycode;

public class Text extends Message {

	private String content = "";

	public String getContent() {
		return content;
	}

	public void setContent(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		this.content = value;
	}
}
