package mycode;

public class Voice extends Message {

	private String text = "";

	public String getText() {
		return text;
	}

	public void setText(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		this.text = value;
	}
}