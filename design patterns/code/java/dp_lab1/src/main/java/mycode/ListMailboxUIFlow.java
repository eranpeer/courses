package mycode;

public class ListMailboxUIFlow {

	private AddressBook addressBook;
	private Console console;

	public ListMailboxUIFlow(AddressBook addressBook) {
		this.console = new Console();
		this.addressBook = addressBook;
	}

	private String buildTitle(Message message) {
		String title = null;
		if (message instanceof Email) {
			Email msg = (Email) message;
			title = "Email: " + msg.getSubject();
		}
		if (message instanceof Text) {
			Text msg = (Text) message;
			title = "Text: " + msg.getContent();
		}
		if (message instanceof Voice) {
			Voice msg = (Voice) message;
			title = "Voice: from " + msg.getOrigin();
		}
		return title;
	}

	public void listMailbox() {

		Mailbox mbx = null;
		String address = console.readLine("Enter mailbox address:");
		mbx = addressBook.getMailbox(address);
		if (mbx == null) {
			System.out.println("invalid mailbox address");
			return;
		}

		// list the content of the mailbox.
		Message messages[] = mbx.getMessages();
		console.show("Content of mailbox " + mbx.getAddress() + ":");
		for (int i = 0; i < messages.length; i++) {
			Message message = messages[i];
			String title = buildTitle(message);
			title = (i + 1) + ": " + title;
			console.show(title);
		}
	}
}