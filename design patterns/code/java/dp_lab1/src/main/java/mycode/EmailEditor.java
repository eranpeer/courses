package mycode;

public class EmailEditor {

	private AddressBook addressBook;

	public EmailEditor(AddressBook addressBook) {
		this.addressBook = addressBook;
	}

	public boolean edit(Email msg) {
		Console console = new Console();
		if (!MessageEditingUtils.editAddresses(msg, addressBook, console))
			return false;
		msg.setSubject(console.readLine("Enter email's subject:", msg.getSubject()));
		msg.setContent(console.readLine("Enter email's content:", msg.getContent()));
		return true;
	}

}
