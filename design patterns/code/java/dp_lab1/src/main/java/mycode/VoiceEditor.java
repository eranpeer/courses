package mycode;

public class VoiceEditor {

	private AddressBook addressBook;

	public VoiceEditor(AddressBook addressBook) {
		this.addressBook = addressBook;
	}

	public boolean edit(Voice msg) {
		Console console = new Console();
		if (!MessageEditingUtils.editAddresses(msg, addressBook, console))
			return false;
		msg.setText(console.readLine("Please record your message (type something):", msg.getText()));
		return true;
	}

}