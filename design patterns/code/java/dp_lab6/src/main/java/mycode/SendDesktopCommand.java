package mycode;

public class SendDesktopCommand extends Desktop.LeafCommand {
	private final String messageType;
	private SendMessageUIFlow uiFlow;

	public SendDesktopCommand(Application application, String messageType) {
		this.messageType = messageType;
		uiFlow = new SendMessageUIFlow(application.getConsole(), application.getAddressBook(), application.getMessageFactory(),
				application.getEditorFactory());
	}

	@Override
	public void execute() {
		uiFlow.sendMessage(messageType);
	}

	@Override
	public String getDisplayName() {
		return "Send " + messageType;
	}

}