package mycode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Console {
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

	public String readLine(String prompt) {
		String line = null;
		System.out.println(prompt);
		try {
			line = reader.readLine();
		} catch (IOException e) {
		}
		return line;
	}

	public String readLine(String prompt, String defaultValue) {
		if (defaultValue != null && !defaultValue.isEmpty()) {
			prompt += '[' + defaultValue + ']';
		}
		System.out.println(prompt);
		try {
			String line = reader.readLine();
			if (line.isEmpty())
				line = defaultValue;
			return line;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void show(String str) {
		System.out.println(str);
	}
}