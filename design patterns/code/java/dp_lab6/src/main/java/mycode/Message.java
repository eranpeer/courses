package mycode;

public abstract class Message {

	private String destination = "";
	private String origin = "";

	public abstract <T> T accept(MsgVisitor<T> v);

	public String getDestination() {
		return destination;
	}

	public String getOrigin() {
		return origin;
	}

	public void setDestination(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		destination = value;
	}

	public void setOrigin(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		origin = value;
	}

}