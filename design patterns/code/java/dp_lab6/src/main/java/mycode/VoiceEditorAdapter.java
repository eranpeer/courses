package mycode;

public class VoiceEditorAdapter extends VoiceEditor implements MsgEditor {

	public VoiceEditorAdapter(AddressBook addressBook, Console console) {
		super(addressBook, console);
	}

	@Override
	public boolean edit(Message m) {
		return super.edit((Voice) m);
	}
}