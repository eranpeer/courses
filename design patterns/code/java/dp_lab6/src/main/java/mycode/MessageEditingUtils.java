package mycode;

public class MessageEditingUtils {

	public static boolean editAddresses(Message msg, AddressBook addressBook, Console console) {
		String senderAddress = console.readLine("Enter sender's address:", msg.getOrigin());
		if (addressBook.getMailbox(senderAddress) == null) {
			console.show("invalid sender mailbox");
			return false;
		}
		msg.setOrigin(senderAddress);

		String destinationAddress = console.readLine("Enter destination address:", msg.getDestination());
		if (addressBook.getMailbox(destinationAddress) == null) {
			console.show("invalid destination mailbox");
			return false;
		}
		msg.setDestination(destinationAddress);
		return true;
	}

}