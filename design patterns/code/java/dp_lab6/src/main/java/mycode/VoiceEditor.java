package mycode;

public class VoiceEditor {

	private AddressBook addressBook;
	private final Console console;

	public VoiceEditor(AddressBook addressBook, Console console) {
		this.addressBook = addressBook;
		this.console = console;
	}

	public boolean edit(Voice msg) {
		if (!MessageEditingUtils.editAddresses(msg, addressBook, console))
			return false;
		msg.setText(console.readLine("Enter voice's text:", msg.getText()));
		return true;
	}

}