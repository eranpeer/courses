package mycode;

public interface MsgEditorFactory {

	MsgEditor createMsgEditor(String messageType);

}