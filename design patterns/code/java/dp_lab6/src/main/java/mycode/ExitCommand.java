package mycode;

public class ExitCommand extends Desktop.LeafCommand {

	private Application application;

	public ExitCommand(Application application) {
		this.application = application;
	}

	@Override
	public void execute() {
		application.stop();
	}

	@Override
	public String getDisplayName() {
		return "Exit";
	}
}