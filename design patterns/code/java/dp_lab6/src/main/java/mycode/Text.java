package mycode;

public class Text extends Message {

	private String content = "";

	@Override
	public <T> T accept(MsgVisitor<T> v) {
		return v.visit(this);
	}

	public String getContent() {
		return content;
	}

	public void setContent(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		this.content = value;
	}
}
