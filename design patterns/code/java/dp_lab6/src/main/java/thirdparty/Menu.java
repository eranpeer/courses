package thirdparty;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.Vector;

public class Menu {

	private Vector<MenuItem> items_;

	private String name;

	public Menu(String name) {
		items_ = new Vector<MenuItem>();
		this.name = name;
	}

	public void addItem(MenuItem item) {
		items_.addElement(item);
	}

	public String getName() {
		return name;
	}

	public void show() {
		System.out.println(name);
		Enumeration<MenuItem> e = items_.elements();
		int i = 1;
		while (e.hasMoreElements()) {
			System.out.print(i + ". " + e.nextElement().getName());
			System.out.println();
			i++;
		}
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Select Option:");
			String option = reader.readLine();
			Integer intOption = new Integer(option);
			MenuItem item = items_.elementAt(intOption.intValue() - 1);
			item.click();
		} catch (Exception ex) {
			System.out.println("Invalid Option !!");
		}
	}
}
