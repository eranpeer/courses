package mycode;

public class Application {

	public static void main(String[] args) {
		new Application().start();
	}

	private final AddressBook addressBook;
	private final Console console;
	private final Desktop desktop;
	private final MessageFactory messageFactory;

	private final MsgEditorFactoryImpl msgEditorFactory;

	public Application() {
		addressBook = new AddressBook();
		console = new Console();
		messageFactory = new MessageFactoryImpl();
		msgEditorFactory = new MsgEditorFactoryImpl(addressBook, console);
		desktop = new Desktop();
	}

	public AddressBook getAddressBook() {
		return addressBook;
	}

	public Console getConsole() {
		return console;
	}

	public MsgEditorFactory getEditorFactory() {
		return msgEditorFactory;
	}

	public MessageFactory getMessageFactory() {
		return messageFactory;
	}

	private void initDesktopCommands(Desktop desktop) {
		desktop.addDesktopConmmand(new SendDesktopCommand(this));
		desktop.addDesktopConmmand(new DeleteMessageCommand(this));
		desktop.addDesktopConmmand(new ListMessagesCommand(this));
		desktop.addDesktopConmmand(new ExitCommand(this));
	}

	public void start() {
		Mailbox eranMbx = new Mailbox("eran");
		Mailbox ronenMbx = new Mailbox("ronen");

		addressBook.addMailbox(eranMbx);
		addressBook.addMailbox(ronenMbx);

		initDesktopCommands(desktop);
		desktop.start();
	}

	public void stop() {
		desktop.stop();
	}
}