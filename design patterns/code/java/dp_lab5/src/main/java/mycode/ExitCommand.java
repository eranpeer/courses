package mycode;

public class ExitCommand implements Desktop.Command {

	private Application application;

	public ExitCommand(Application application) {
		this.application = application;
	}

	public void execute() {
		application.stop();
	}

	public String getDisplayName() {
		return "Exit";
	}
}