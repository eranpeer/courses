package mycode;

public class MessageFactoryImpl implements MessageFactory {

	public Message createMessage(String messageType) {
		if (messageType.equals("email")) {
			return new Email();
		}

		if (messageType.equals("text")) {
			return new Text();
		}

		if (messageType.equals("voice")) {
			return new Voice();
		}
		return null;
	}

}