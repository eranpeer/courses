package mycode;

public class Mailer {
	private AddressBook addressBook;

	public Mailer(AddressBook addressBook) {
		this.addressBook = addressBook;
	}

	public void deliver(Message m) {
		Mailbox mbx = addressBook.getMailbox(m.getDestination());
		mbx.add(m);
	}
}