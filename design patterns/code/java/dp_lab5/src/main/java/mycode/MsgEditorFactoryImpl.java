package mycode;

public class MsgEditorFactoryImpl implements MsgEditorFactory {

	private final AddressBook addressBook;
	private final Console console;

	public MsgEditorFactoryImpl(final AddressBook addressBook, final Console console) {
		this.addressBook = addressBook;
		this.console = console;
	}

	public MsgEditor createMsgEditor(String messageType) {
		if (messageType.equals("email")) {
			return new EmailEditorAdapter(addressBook, console);
		}

		if (messageType.equals("text")) {
			return new TextEditorAdapter(addressBook, console);
		}

		if (messageType.equals("voice")) {
			return new VoiceEditorAdapter(addressBook, console);
		}
		return null;
	}

}