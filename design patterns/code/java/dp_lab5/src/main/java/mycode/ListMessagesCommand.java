package mycode;

public class ListMessagesCommand implements Desktop.Command {

	private ListMailboxUIFlow uiFlow;

	public ListMessagesCommand(Application application) {
		uiFlow = new ListMailboxUIFlow(application.getConsole(), application.getAddressBook());
	}

	public void execute() {
		uiFlow.listMailbox();
	}

	public String getDisplayName() {
		return "List Messages";
	}
}