package mycode;

public class SendDesktopCommand implements Desktop.Command {

	private SendMessageUIFlow uiFlow;

	public SendDesktopCommand(Application application) {
		uiFlow = new SendMessageUIFlow(application.getConsole(), application.getAddressBook(), application.getMessageFactory(),
				application.getEditorFactory());
	}

	public void execute() {
		uiFlow.sendMessage();
	}

	public String getDisplayName() {
		return "Send Message";
	}

}