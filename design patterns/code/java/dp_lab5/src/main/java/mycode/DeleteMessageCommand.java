package mycode;

public class DeleteMessageCommand implements Desktop.Command {

	private DeleteMessageUIFlow uiFlow;

	public DeleteMessageCommand(Application application) {
		uiFlow = new DeleteMessageUIFlow(application.getConsole(), application.getAddressBook());
	}

	public void execute() {
		uiFlow.deleteMessage();
	}

	public String getDisplayName() {
		return "Delete Message";
	}
}