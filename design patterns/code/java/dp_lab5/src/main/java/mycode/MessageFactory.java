package mycode;

public interface MessageFactory {

	Message createMessage(String messageType);

}