package mycode;

public class Email extends Message {

	private String content = "";
	private String subject = "";

	@Override
	public <T> T accept(MsgVisitor<T> v) {
		return v.visit(this);
	}

	public String getContent() {
		return content;
	}

	public String getSubject() {
		return subject;
	}

	void setContent(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		this.content = value;
	}

	void setSubject(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		this.subject = value;
	}
}