package mycode;

public class TitleBuilderVisitor implements MsgVisitor<String> {

	public String visit(Text msg) {
		return "Text: " + msg.getContent();
	}

	public String visit(Email msg) {
		return "Email: " + msg.getSubject();
	}

	public String visit(Voice msg) {
		return "Voice: from " + msg.getOrigin();
	}

}