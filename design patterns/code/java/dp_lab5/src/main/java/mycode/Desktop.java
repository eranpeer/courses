package mycode;

import thirdparty.Menu;
import thirdparty.MenuItem;

public class Desktop {

	public static interface Command {
		void execute();

		String getDisplayName();
	}

	private static class CommandMenuItem extends MenuItem {

		private final Desktop.Command command;

		public CommandMenuItem(Desktop.Command command) {
			super(command.getDisplayName());
			this.command = command;
		}

		@Override
		public void click() {
			command.execute();
		}
	}

	private boolean isActive;
	private final Menu mainMenu;

	public Desktop() {
		mainMenu = new Menu("Select an option:");
	}

	public void addDesktopConmmand(Desktop.Command command) {
		mainMenu.addItem(new CommandMenuItem(command));
	}

	public void start() {
		isActive = true;
		while (isActive) {
			mainMenu.show();
		}
	}

	public void stop() {
		isActive = false;
	}

}