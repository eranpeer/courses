package mycode;

public class DeleteMessageUIFlow {

	private AddressBook addressBook;
	private Console console;

	public DeleteMessageUIFlow(Console console, AddressBook addressBook) {
		this.console = console;
		this.addressBook = addressBook;
	}

	private String buildTitle(Message message) {
		return message.accept(new TitleBuilderVisitor());
	}

	public void deleteMessage() {

		String mbxAddress = console.readLine("Delete message from mailbox:");
		Mailbox mbx = addressBook.getMailbox(mbxAddress);

		if (mbx == null) {
			console.show(mbxAddress + " is not a valid mailbox address !");
			return;
		}

		try {
			Integer messageId = new Integer(console.readLine("Enter message id:"));
			Message deletedMsg = mbx.delete(messageId.intValue());
			if (deletedMsg != null)
				console.show(buildTitle(deletedMsg) + " was deleted from mailbox " + mbxAddress);
			else {
				console.show("invalid message id!");
			}
		} catch (NumberFormatException e) {
			console.show("invalid message id!");
		}
	}

}