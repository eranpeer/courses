namespace MyCode
{
    internal class ExitCommand : Desktop.ICommand
    {
        private readonly Application application;

        public ExitCommand(Application application)
        {
            this.application = application;
        }

        #region ICommand Members

        public void Execute()
        {
            application.Stop();
        }

        public string DisplayName
        {
            get { return "Exit"; }
        }

        #endregion
    }
}