using System;

namespace MyCode
{
    public class Application
    {
        private AddressBook addressBook;
        private Console console;
        private Desktop desktop;
        private IMessageFactory messageFactory;
        private IMsgEditorFactory msgEditorFactory;

        public IMessageFactory MessageFactory
        {
            get { return messageFactory; }
        }

        public IMsgEditorFactory MsgEditorFactory
        {
            get { return msgEditorFactory; }
        }

        public AddressBook AddressBook
        {
            get { return addressBook; }
        }

        public Console Console
        {
            get { return console; }
        }


        public void Start()
        {
            console = new Console();
            addressBook = new AddressBook();

            messageFactory = new MessageFactoryImpl();
            msgEditorFactory = new MsgEditorFactoryImpl(addressBook, console);

            var eranMbx = new Mailbox("eran");
            var ronenMbx = new Mailbox("ronen");

            AddressBook.AddMailbox(eranMbx);
            AddressBook.AddMailbox(ronenMbx);

            desktop = new Desktop();

            desktop.AddDesktopCommand(new SendCommand(this));
            desktop.AddDesktopCommand(new DeleteMessageCommand(this));
            desktop.AddDesktopCommand(new ListMessagesCommand(this));
            desktop.AddDesktopCommand(new ExitCommand(this));

            desktop.Start();
        }


        public void Stop()
        {
            desktop.Stop();
        }

        [STAThread]
        public static void Main(string[] args)
        {
            new Application().Start();
        }
    }
}