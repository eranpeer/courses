namespace MyCode
{
    internal class ListMessagesCommand : Desktop.ICommand
    {
        private readonly Application application;

        public ListMessagesCommand(Application application)
        {
            this.application = application;
        }

        #region ICommand Members

        public void Execute()
        {
            var listMessagesUIFlow = new ListMessagesUIFlow(application.AddressBook, application.Console);
            listMessagesUIFlow.ListMailbox();
        }

        public string DisplayName
        {
            get { return "List Messages"; }
        }

        #endregion
    }
}