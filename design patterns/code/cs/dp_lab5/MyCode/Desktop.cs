using Thirdparty;

namespace MyCode
{
    public class Desktop
    {
        private readonly Menu mainMenu;
        private bool isActive;

        public Desktop()
        {
            mainMenu = new Menu("Select an option:");
        }

        public virtual void Start()
        {
            isActive = true;
            while (isActive)
            {
                mainMenu.Show();
            }
        }

        public void AddDesktopCommand(ICommand command)
        {
            mainMenu.AddItem(new CommandMenuItem(command));
        }

        public void Stop()
        {
            isActive = false;
        }

        #region Nested type: CommandMenuItem

        private class CommandMenuItem : MenuItem
        {
            private readonly ICommand command;

            public CommandMenuItem(ICommand command) : base(command.DisplayName)
            {
                this.command = command;
            }

            public override void Click()
            {
                command.Execute();
            }
        }

        #endregion

        #region Nested type: ICommand

        public interface ICommand
        {
            string DisplayName { get; }
            void Execute();
        }

        #endregion
    }
}