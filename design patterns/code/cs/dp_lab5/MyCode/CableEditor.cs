namespace MyCode
{
    public class CableEditor
    {
        private readonly AddressBook addressBook;
        private readonly Console console;

        public CableEditor(AddressBook addressBook, Console console)
        {
            this.addressBook = addressBook;
            this.console = console;
        }

        public bool Edit(Cable msg)
        {
            if (!MessageEditingUtils.EditAddresses(msg, addressBook, console))
                return false;
            msg.Content = console.ReadLine("Enter cable's content:", msg.Content);
            return true;
        }
    }
}