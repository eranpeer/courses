namespace MyCode
{
    public class Mailer
    {
        private readonly AddressBook addressBook;

        public Mailer(AddressBook addressBook)
        {
            this.addressBook = addressBook;
        }

        public virtual void Deliver(Message m)
        {
            Mailbox mbx = addressBook.GetMailbox(m.Destination);
            mbx.Add(m);
        }
    }
}