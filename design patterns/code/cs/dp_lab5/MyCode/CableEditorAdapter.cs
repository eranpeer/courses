namespace MyCode
{
    public class CableEditorAdapter : CableEditor, IMsgEditor
    {
        public CableEditorAdapter(AddressBook addressBook, Console console)
            : base(addressBook, console)
        {
        }

        #region IMsgEditor Members

        public bool Edit(Message msg)
        {
            return base.Edit(msg as Cable);
        }

        #endregion
    }
}