namespace MyCode
{
    internal class SendCommand : Desktop.ICommand
    {
        private readonly Application application;

        public SendCommand(Application application)
        {
            this.application = application;
        }

        #region ICommand Members

        public void Execute()
        {
            var sendMessageUIFlow = new SendMessageUIFlow(application.Console, application.AddressBook, application.MessageFactory,
                                                          application.MsgEditorFactory);
            sendMessageUIFlow.SendMessage();
        }

        public string DisplayName
        {
            get { return "Send Message"; }
        }

        #endregion
    }
}