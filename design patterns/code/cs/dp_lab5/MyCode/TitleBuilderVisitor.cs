namespace MyCode
{
    public class TitleBuilderVisitor : IMsgVisitor<string>
    {
        #region IMsgVisitor<string> Members

        public string Visit(Postcard msg)
        {
            return "Postcard: from " + msg.Origin;
        }

        public string Visit(Cable msg)
        {
            return "Cable: " + msg.Content;
        }

        public string Visit(Letter msg)
        {
            return "Letter: " + msg.Subject;
        }

        #endregion
    }
}