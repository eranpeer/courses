namespace MyCode
{
    internal class DeleteMessageCommand : Desktop.ICommand
    {
        private readonly Application application;

        public DeleteMessageCommand(Application application)
        {
            this.application = application;
        }

        #region ICommand Members

        public void Execute()
        {
            var deleteMessageUIFlow = new DeleteMessageUIFlow(application.AddressBook, application.Console);
            deleteMessageUIFlow.DeleteMessage();
        }

        public string DisplayName
        {
            get { return "Delete Message"; }
        }

        #endregion
    }
}