using System;

namespace MyCode
{
    public class Postcard : Message
    {
        private string text = string.Empty;

        public string Text
        {
            get { return text; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException();
                text = value;
            }
        }

        public override T Accept<T>(IMsgVisitor<T> v)
        {
            return v.Visit(this);
        }
    }
}