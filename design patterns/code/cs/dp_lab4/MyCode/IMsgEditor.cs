namespace MyCode
{
    public interface IMsgEditor
    {
        bool Edit(Message msg);
    }
}