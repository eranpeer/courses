using System;

namespace MyCode
{
    public abstract class Message
    {
        private string destination = string.Empty;
        private string origin = string.Empty;

        public virtual string Destination
        {
            get { return destination; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException();
                destination = value;
            }
        }

        public virtual string Origin
        {
            get { return origin; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException();
                origin = value;
            }
        }

        public abstract T Accept<T>(IMsgVisitor<T> v);
    }
}