namespace MyCode
{
    public interface IMsgEditorFactory
    {
        IMsgEditor CreateMsgEditor(string messageType);
    }
}