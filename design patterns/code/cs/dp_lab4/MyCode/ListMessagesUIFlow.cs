namespace MyCode
{
    internal class ListMessagesUIFlow
    {
        private readonly AddressBook addressBook;
        private readonly Console console;

        public ListMessagesUIFlow(AddressBook addressBook, Console console)
        {
            this.addressBook = addressBook;
            this.console = console;
        }

        public virtual void ListMailbox()
        {
            var v = new TitleBuilderVisitor();
            string address = console.ReadLine("Enter mailbox address:");
            Mailbox mbx = addressBook.GetMailbox(address);
            if (mbx == null)
            {
                console.Show("invalid sender mailbox");
                return;
            }

            // list the content of the mailbox.
            console.Show("Content of mailbox " + mbx.Address + ":");
            int i = 1;
            foreach (Message m in mbx)
            {
                string title = m.Accept(v);
                title = i + ": " + title;
                console.Show(title);
                i++;
            }
        }
    }
}