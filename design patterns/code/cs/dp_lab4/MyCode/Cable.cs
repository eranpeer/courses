using System;

namespace MyCode
{
    public class Cable : Message
    {
        private string content = string.Empty;

        public string Content
        {
            get { return content; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException();
                content = value;
            }
        }

        public override T Accept<T>(IMsgVisitor<T> v)
        {
            return v.Visit(this);
        }
    }
}