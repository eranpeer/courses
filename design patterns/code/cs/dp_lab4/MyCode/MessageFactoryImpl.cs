namespace MyCode
{
    public class MessageFactoryImpl : IMessageFactory
    {
        #region IMessageFactory Members

        public Message CreateMessage(string messageType)
        {
            if (messageType == "letter")
            {
                return new Letter();
            }
            if (messageType == "cable")
            {
                return new Cable();
            }
            if (messageType == "postcard")
            {
                return new Postcard();
            }
            return null;
        }

        #endregion
    }
}