namespace MyCode
{
    public class SendMessageUIFlow
    {
        private readonly AddressBook addressBook;
        private readonly Console console;
        private readonly IMessageFactory messageFactory;
        private readonly IMsgEditorFactory msgEditorFactory;

        public SendMessageUIFlow(Console console, AddressBook addressBook, IMessageFactory messageFactory,
                                 IMsgEditorFactory msgEditorFactory)
        {
            this.console = console;
            this.addressBook = addressBook;
            this.messageFactory = messageFactory;
            this.msgEditorFactory = msgEditorFactory;
        }

        public void SendMessage()
        {
            string messageType = console.ReadLine("Enter type of message:");
            Message newMessage = messageFactory.CreateMessage(messageType);
            if (newMessage == null)
            {
                console.Show("invalid message type !");
                return;
            }

            IMsgEditor editor = msgEditorFactory.CreateMsgEditor(messageType);
            if (!editor.Edit(newMessage))
                return;

            new Mailer(addressBook).Deliver(newMessage);
            console.Show("New " + messageType + " for " + newMessage.Destination + "!");
        }
    }
}