namespace MyCode
{
    public class LetterEditorAdapter : LetterEditor, IMsgEditor
    {
        public LetterEditorAdapter(AddressBook addressBook, Console console) : base(addressBook, console)
        {
        }

        #region IMsgEditor Members

        public bool Edit(Message msg)
        {
            return base.Edit(msg as Letter);
        }

        #endregion
    }
}