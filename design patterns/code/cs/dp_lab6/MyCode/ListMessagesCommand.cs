namespace MyCode
{
    internal class ListMessagesCommand : Desktop.LeafCommand
    {
        private readonly Application application;

        public ListMessagesCommand(Application application)
        {
            this.application = application;
        }

        public override string DisplayName
        {
            get { return "List Messages"; }
        }

        public override void Execute()
        {
            var listMessagesUIFlow = new ListMessagesUIFlow(application.AddressBook, application.Console);
            listMessagesUIFlow.ListMailbox();
        }
    }
}