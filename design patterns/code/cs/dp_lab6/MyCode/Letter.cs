using System;

namespace MyCode
{
    public class Letter : Message
    {
        private string content = string.Empty;
        private string subject = string.Empty;

        public virtual string Subject
        {
            set
            {
                if (value == null)
                    throw new ArgumentNullException();
                subject = value;
            }
            get { return subject; }
        }

        public virtual string Content
        {
            set
            {
                if (value == null)
                    throw new ArgumentNullException();
                content = value;
            }
            get { return content; }
        }

        public override T Accept<T>(IMsgVisitor<T> v)
        {
            return v.Visit(this);
        }
    }
}