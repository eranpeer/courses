namespace MyCode
{
    internal class SendCommand : Desktop.LeafCommand
    {
        private readonly Application application;
        private readonly string messageType;

        public SendCommand(Application application, string messageType)
        {
            this.application = application;
            this.messageType = messageType;
        }

        public override string DisplayName
        {
            get { return "Send " + messageType; }
        }

        public override void Execute()
        {
            var sendMessageUIFlow = new SendMessageUIFlow(application.Console, application.AddressBook, application.MessageFactory,
                                                          application.MsgEditorFactory);
            sendMessageUIFlow.SendMessage(messageType);
        }
    }
}