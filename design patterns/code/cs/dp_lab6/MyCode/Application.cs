using System;

namespace MyCode
{
    public class Application
    {
        private AddressBook addressBook;
        private Console console;
        private Desktop desktop;
        private IMessageFactory messageFactory;
        private IMsgEditorFactory msgEditorFactory;

        public AddressBook AddressBook
        {
            get { return addressBook; }
        }

        public Console Console
        {
            get { return console; }
        }

        public IMessageFactory MessageFactory
        {
            get { return messageFactory; }
        }

        public IMsgEditorFactory MsgEditorFactory
        {
            get { return msgEditorFactory; }
        }

        public void Start()
        {
            console = new Console();
            addressBook = new AddressBook();
            
            messageFactory = new MessageFactoryImpl();
            msgEditorFactory = new MsgEditorFactoryImpl(addressBook, console);

            var eranMbx = new Mailbox("eran");
            var ronenMbx = new Mailbox("ronen");

            AddressBook.AddMailbox(eranMbx);
            AddressBook.AddMailbox(ronenMbx);

            desktop = new Desktop();

            Desktop.ICommand sendCommand = desktop.CreateMenuCommand("Send Message");
            sendCommand.AddCommand(new SendCommand(this, "letter"));
            sendCommand.AddCommand(new SendCommand(this, "cable"));
            sendCommand.AddCommand(new SendCommand(this, "postcard"));
            desktop.AddDesktopCommand(sendCommand);

            desktop.AddDesktopCommand(new DeleteMessageCommand(this));
            desktop.AddDesktopCommand(new ListMessagesCommand(this));
            desktop.AddDesktopCommand(new ExitCommand(this));

            desktop.Start();
        }


        public void Stop()
        {
            desktop.Stop();
        }

        [STAThread]
        public static void Main(string[] args)
        {
            new Application().Start();
        }
    }
}