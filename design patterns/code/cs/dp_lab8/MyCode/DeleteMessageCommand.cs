namespace MyCode
{
    internal class DeleteMessageCommand : Desktop.LeafCommand
    {
        private readonly MessagingService messagingService;
        private readonly Application application;

        public DeleteMessageCommand(MessagingService messagingService, Application application)
        {
            this.messagingService = messagingService;
            this.application = application;
        }

        public override string DisplayName
        {
            get { return "Delete Message"; }
        }

        public override void Execute()
        {
            var deleteMessageUIFlow = new DeleteMessageUIFlow(messagingService, application.Console);
            deleteMessageUIFlow.DeleteMessage();
        }
    }
}