namespace MyCode
{
    public class Console
    {
        public string ReadLine(string prompt)
        {
            Show(prompt);
            return System.Console.ReadLine();
        }

        public string ReadLine(string prompt, string defaultValue)
        {
            if (!string.IsNullOrEmpty(defaultValue))
            {
                prompt += '[' + defaultValue + ']';
            }
            Show(prompt);
            string line = System.Console.ReadLine();
            if (string.IsNullOrEmpty(line))
                line = defaultValue;
            return line;
        }

        public void Show(string str)
        {
            System.Console.WriteLine(str);
        }
    }
}