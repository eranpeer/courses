using System.Collections.Generic;

namespace MyCode
{
    public class MessagingService
    {
        private readonly AddressBook addressBook;
        private readonly IMessageFactory messageFactory;

        public MessagingService(AddressBook addressBook, IMessageFactory messageFactory)
        {
            this.addressBook = addressBook;
            this.messageFactory = messageFactory;
        }


        public bool IsValidMbxAddress(string mbxAddress)
        {
            return addressBook.GetMailbox(mbxAddress) == null;
        }

        public Message DeletedMsg(string mbxAddress, int messageId)
        {
            Mailbox mailbox = addressBook.GetMailbox(mbxAddress);
            return mailbox.Delete(messageId);
        }

        public IList<Message> GetMessagesInMbx(string address)
        {
            IEnumerable<Message> mbx = addressBook.GetMailbox(address);
            if (mbx != null)
            {
                return new List<Message>(mbx);
            }
            return null;
        }

        public Message CreateMessage(string messageType)
        {
            return messageFactory.CreateMessage(messageType);
        }

        public void Deliver(Message newMessage)
        {
            new Mailer(addressBook).Deliver(newMessage);
        }

    }
}