namespace MyCode
{
    internal class ListMessagesCommand : Desktop.LeafCommand
    {
        private readonly Application application;
        private readonly MessagingService messagingService;

        public ListMessagesCommand(Application application, MessagingService messagingService)
        {
            this.application = application;
            this.messagingService = messagingService;
        }

        public override string DisplayName
        {
            get { return "List Messages"; }
        }

        public override void Execute()
        {
            var listMessagesUIFlow = new ListMessagesUIFlow(application.Console, messagingService);
            listMessagesUIFlow.ListMailbox();
        }
    }
}