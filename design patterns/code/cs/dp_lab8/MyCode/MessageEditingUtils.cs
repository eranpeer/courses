namespace MyCode
{
    public class MessageEditingUtils
    {
        public static bool EditAddresses(Message msg, AddressBook addressBook,
                                         Console console)
        {
            string senderAddress = console.ReadLine("Enter sender's address:", msg.Origin);
            if (addressBook.GetMailbox(senderAddress) == null)
            {
                console.Show("invalid sender address: " + senderAddress);
                return false;
            }
            msg.Origin = senderAddress;

            string destinationAddress = console.ReadLine("Enter destination address:", msg.Destination);
            if (addressBook.GetMailbox(destinationAddress) == null)
            {
                console.Show("invalid destination address: " + destinationAddress);
                return false;
            }
            msg.Destination = destinationAddress;
            return true;
        }
    }
}