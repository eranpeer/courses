using System;

namespace MyCode
{
    internal class DeleteMessageUIFlow
    {
        private readonly MessagingService messagingService;
        private readonly Console console;

        public DeleteMessageUIFlow(MessagingService messagingService, Console console)
        {
            this.messagingService = messagingService;
            this.console = console;
        }

        public virtual void DeleteMessage()
        {
            try
            {
                string mbxAddress = console.ReadLine("Delete message from Mailbox:");
                if (messagingService.IsValidMbxAddress(mbxAddress))
                {
                    console.Show(mbxAddress + " is not a valid address !");
                    return;
                }
                Int32 messageId = Int32.Parse(console.ReadLine("Enter message id:"));
                var deletedMsg = messagingService.DeletedMsg(mbxAddress, messageId);
                if (deletedMsg != null)
                    console.Show(BuildTitle(deletedMsg) + " was deleted from mailbox " + mbxAddress);
                else
                {
                    console.Show("invalid message id!");
                }
            }
            catch (Exception)
            {
                console.Show("invalid message id !");
            }
        }

        private static string BuildTitle(Message deletedMsg)
        {
            var titleBuilder = new TitleBuilderVisitor();
            return deletedMsg.Accept(titleBuilder);
        }
    }
}