namespace MyCode
{
    internal class SendCommand : Desktop.LeafCommand
    {
        private readonly Application application;
        private readonly string messageType;
        private MessagingService messagingService;

        public SendCommand(Application application, string messageType, MessagingService messagingService)
        {
            this.application = application;
            this.messageType = messageType;
            this.messagingService = messagingService;
        }

        public override string DisplayName
        {
            get { return "Send " + messageType; }
        }

        public override void Execute()
        {
            var sendMessageUIFlow = new SendMessageUIFlow(application.Console, application.MsgEditorFactory, messagingService);
            sendMessageUIFlow.SendMessage(messageType);
        }
    }
}