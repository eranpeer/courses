namespace MyCode
{
    public class SendMessageUIFlow
    {
        private readonly Console console;
        private readonly IMsgEditorFactory msgEditorFactory;
        private readonly MessagingService messagingService;

        public SendMessageUIFlow(Console console, IMsgEditorFactory msgEditorFactory, MessagingService messagingService)
        {
            this.console = console;
            this.msgEditorFactory = msgEditorFactory;
            this.messagingService = messagingService;
        }

        public void SendMessage(string messageType)
        {
            var newMessage = messagingService.CreateMessage(messageType);
            if (newMessage == null)
            {
                console.Show("invalid message type !");
                return;
            }

            IMsgEditor editor = msgEditorFactory.CreateMsgEditor(messageType);
            if (!editor.Edit(newMessage))
                return;

            messagingService.Deliver(newMessage);
            console.Show("New " + messageType + " for " + newMessage.Destination + "!");
        }
    }
}