namespace MyCode
{
    internal class ExitCommand : Desktop.LeafCommand
    {
        private readonly Application application;

        public ExitCommand(Application application)
        {
            this.application = application;
        }

        public override string DisplayName
        {
            get { return "Exit"; }
        }

        public override void Execute()
        {
            application.Stop();
        }
    }
}