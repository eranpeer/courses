using System.Collections;
using System.Collections.Generic;

namespace MyCode
{
    internal class ListMessagesUIFlow
    {
        private readonly Console console;
        private readonly MessagingService messagingService;

        public ListMessagesUIFlow(Console console, MessagingService messagingService)
        {
            this.console = console;
            this.messagingService = messagingService;
        }

        public virtual void ListMailbox()
        {
            var v = new TitleBuilderVisitor();
            string address = console.ReadLine("Enter mailbox address:");
            var messages = messagingService.GetMessagesInMbx(address);
            if (messages == null)
            {
                console.Show("invalid sender mailbox");
                return;
            }

            // list the content of the mailbox.
            console.Show("Content of mailbox " + address + ":");
            int i = 1;
            foreach (Message m in messages)
            {
                string title = m.Accept(v);
                title = i + ": " + title;
                console.Show(title);
                i++;
            }
        }

    }
}