namespace MyCode
{
    internal class ChangeDefaultsCommand : Desktop.LeafCommand
    {
        private readonly Application application;
        private readonly string messageType;

        public ChangeDefaultsCommand(Application application, string messageType)
        {
            this.application = application;
            this.messageType = messageType;
        }

        public override string DisplayName
        {
            get { return "Change " + messageType + " default values"; }
        }

        public override void Execute()
        {
            var prototypeFactory = application.MessageFactory as MessageFactoryImpl;
            var prototype = prototypeFactory.GetPrototype(messageType);
            var messageEditor = application.MsgEditorFactory.CreateMsgEditor(messageType);
            messageEditor.Edit(prototype);
        }
    }
}