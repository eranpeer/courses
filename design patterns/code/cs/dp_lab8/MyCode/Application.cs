using System;

namespace MyCode
{
    public class Application
    {
        private AddressBook addressBook;
        private Console console;
        private Desktop desktop;
        private MessageFactoryImpl messageFactory;
        private IMsgEditorFactory msgEditorFactory;

        public IMessageFactory MessageFactory
        {
            get { return messageFactory; }
        }

        public IMsgEditorFactory MsgEditorFactory
        {
            get { return msgEditorFactory; }
        }

        public AddressBook AddressBook
        {
            get { return addressBook; }
        }

        public Console Console
        {
            get { return console; }
        }


        public void Start()
        {
            console = new Console();
            addressBook = new AddressBook();
            
            messageFactory = new MessageFactoryImpl();
            messageFactory.SetPrototype("letter", new Letter());
            messageFactory.SetPrototype("cable", new Cable());
            messageFactory.SetPrototype("postcard", new Postcard());

            msgEditorFactory = new MsgEditorFactoryImpl(addressBook, console);

            var eranMbx = new Mailbox("eran");
            var ronenMbx = new Mailbox("ronen");

            AddressBook.AddMailbox(eranMbx);
            AddressBook.AddMailbox(ronenMbx);
            var messagingService = new MessagingService(AddressBook, MessageFactory);

            desktop = new Desktop();

            Desktop.ICommand sendCommand = desktop.CreateMenuCommand("Send Message");
            sendCommand.AddCommand(new SendCommand(this, "letter", messagingService));
            sendCommand.AddCommand(new SendCommand(this, "cable", messagingService));
            sendCommand.AddCommand(new SendCommand(this, "postcard", messagingService));

            Desktop.ICommand changeDefaultsCommand = desktop.CreateMenuCommand("Change defaults");
            changeDefaultsCommand.AddCommand(new ChangeDefaultsCommand(this, "letter"));
            changeDefaultsCommand.AddCommand(new ChangeDefaultsCommand(this, "cable"));
            changeDefaultsCommand.AddCommand(new ChangeDefaultsCommand(this, "postcard"));

            
            desktop.AddDesktopCommand(sendCommand);
            desktop.AddDesktopCommand(new DeleteMessageCommand(messagingService, this));
            desktop.AddDesktopCommand(new ListMessagesCommand(this, messagingService));
            desktop.AddDesktopCommand(changeDefaultsCommand);
            desktop.AddDesktopCommand(new ExitCommand(this));

            desktop.Start();
        }


        public void Stop()
        {
            desktop.Stop();
        }

        [STAThread]
        public static void Main(string[] args)
        {
            new Application().Start();
        }
    }
}