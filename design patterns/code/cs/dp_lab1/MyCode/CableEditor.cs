namespace MyCode
{
    public class CableEditor
    {
        private readonly AddressBook addressBook;

        public CableEditor(AddressBook addressBook)
        {
            this.addressBook = addressBook;
        }

        public bool Edit(Cable msg)
        {
            var console = new Console();
            if (!MessageEditingUtils.EditAddresses(msg, addressBook, console))
                return false;
            msg.Content = console.ReadLine("Enter cable's content:", msg.Content);
            return true;
        }
    }
}