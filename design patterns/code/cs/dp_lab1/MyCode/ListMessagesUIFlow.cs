namespace MyCode
{
    internal class ListMessagesUIFlow
    {
        private readonly AddressBook addressBook;
        private readonly Console console = new Console();

        public ListMessagesUIFlow(AddressBook addressBook)
        {
            this.addressBook = addressBook;
        }

        public virtual void ListMailbox()
        {
            string address = console.ReadLine("Enter mailbox address:");
            Mailbox mbx = addressBook.GetMailbox(address);
            if (mbx == null)
            {
                console.Show("invalid sender mailbox");
                return;
            }

            console.Show("Content of mailbox " + mbx.Address + ":");
            int i = 1;
            foreach (Message m in mbx)
            {
                string title = null;
                if (m is Letter)
                {
                    var msg = (Letter) m;
                    title = "Letter: " + msg.Subject;
                }
                if (m is Cable)
                {
                    var msg = (Cable) m;
                    title = "Cable: " + msg.Content;
                }
                if (m is Postcard)
                {
                    var msg = (Postcard) m;
                    title = "Postcard: from " + msg.Origin;
                }
                title = i + ": " + title;
                console.Show(title);
                i++;
            }
        }
    }
}