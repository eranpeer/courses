namespace MyCode
{
    public class LetterEditor
    {
        private readonly AddressBook addressBook;

        public LetterEditor(AddressBook addressBook)
        {
            this.addressBook = addressBook;
        }

        public bool Edit(Letter msg)
        {
            var console = new Console();
            if (!MessageEditingUtils.EditAddresses(msg, addressBook, console))
                return false;
            msg.Subject = console.ReadLine("Enter letter's subject:", msg.Subject);
            msg.Content = console.ReadLine("Enter letter's content:", msg.Content);
            return true;
        }
    }
}