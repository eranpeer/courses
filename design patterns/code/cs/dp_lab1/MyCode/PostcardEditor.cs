namespace MyCode
{
    public class PostcardEditor
    {
        private readonly AddressBook addressBook;

        public PostcardEditor(AddressBook addressBook)
        {
            this.addressBook = addressBook;
        }

        public bool Edit(Postcard msg)
        {
            var console = new Console();
            if (!MessageEditingUtils.EditAddresses(msg, addressBook, console))
                return false;
            msg.Text = console.ReadLine("Enter postcard's text:", msg.Text);
            return true;
        }
    }
}