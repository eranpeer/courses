using System;
using System.Collections.Generic;

namespace Thirdparty
{
    public class Menu
    {
        private readonly List<MenuItem> items;

        public Menu(string name)
        {
            items = new List<MenuItem>();
            Name = name;
        }

        public string Name { get; private set; }

        public void Show()
        {
            Console.WriteLine(Name);
            int i = 1;
            foreach (MenuItem item in items)
            {
                Console.WriteLine(i + ". " + item.Name);
                i++;
            }
            try
            {
                Console.WriteLine("Select Option:");
                string option = Console.ReadLine();
                int intOption = Int32.Parse(option);
                MenuItem item = items[intOption - 1];
                item.Click();
            }
            catch (Exception)
            {
                Console.WriteLine("Invalid Option!!");
            }
        }

        public void AddItem(MenuItem item)
        {
            items.Add(item);
        }
    }
}