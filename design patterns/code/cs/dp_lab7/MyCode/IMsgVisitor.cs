namespace MyCode
{
    public interface IMsgVisitor<T>
    {
        T Visit(Postcard msg);
        T Visit(Cable msg);
        T Visit(Letter msg);
    }
}