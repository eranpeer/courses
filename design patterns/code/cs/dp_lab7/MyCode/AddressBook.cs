using System.Collections.Generic;

namespace MyCode
{
    public class AddressBook
    {
        private readonly Dictionary<string, Mailbox> content;

        public AddressBook()
        {
            content = new Dictionary<string, Mailbox>();
        }

        public virtual Mailbox GetMailbox(string address)
        {
            Mailbox result;
            content.TryGetValue(address, out result);
            return result;
        }

        public virtual void AddMailbox(Mailbox mbx)
        {
            content.Add(mbx.Address, mbx);
        }
    }
}