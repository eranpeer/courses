using System.Collections.Generic;

namespace MyCode
{
    public class MessageFactoryImpl : IMessageFactory
    {

        private readonly Dictionary<string,Message> prototypes = new Dictionary<string, Message>();
  
        public void SetPrototype(string messageType, Message prototype)
        {
            prototypes[messageType] = prototype;
        }

        public Message GetPrototype(string messageType)
        {
            Message prototype;
            prototypes.TryGetValue(messageType, out prototype);
            return prototype;
        }

        #region IMessageFactory Members

        public Message CreateMessage(string messageType)
        {
            Message prototype = GetPrototype(messageType);
            if (prototype == null)
                return null;
            return prototype.Clone();
        }

        #endregion
    }
}