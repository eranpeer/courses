namespace MyCode
{
    public interface IMessageFactory
    {
        Message CreateMessage(string messageType);
    }
}