using System.Collections;
using System.Collections.Generic;

namespace MyCode
{
    public class Mailbox : IEnumerable<Message>
    {
        private readonly string address;
        private readonly List<Message> messages;

        public Mailbox(string address)
        {
            this.address = address;
            messages = new List<Message>();
        }

        public string Address
        {
            get { return address; }
        }

        #region IEnumerable<Message> Members

        public IEnumerator<Message> GetEnumerator()
        {
            return messages.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        public void Add(Message msg)
        {
            messages.Add(msg);
        }

        public Message Delete(int msgId)
        {
            if (msgId < 0 || msgId >= messages.Count)
                return null;
            Message m = messages[msgId];
            messages.RemoveAt(msgId);
            return m;
        }
    }
}