namespace MyCode
{
    public class MsgEditorFactoryImpl : IMsgEditorFactory
    {
        private readonly AddressBook addressBook;
        private readonly Console console;

        public MsgEditorFactoryImpl(AddressBook addressBook, Console console)
        {
            this.addressBook = addressBook;
            this.console = console;
        }

        #region IMsgEditorFactory Members

        public IMsgEditor CreateMsgEditor(string messageType)
        {
            if (messageType == "letter")
            {
                return new LetterEditorAdapter(addressBook, console);
            }
            if (messageType == "cable")
            {
                return new CableEditorAdapter(addressBook, console);
            }
            if (messageType == "postcard")
            {
                return new PostcardEditorAdapter(addressBook, console);
            }
            return null;
        }

        #endregion
    }
}