namespace MyCode
{
    internal class DeleteMessageCommand : Desktop.LeafCommand
    {
        private readonly Application application;

        public DeleteMessageCommand(Application application)
        {
            this.application = application;
        }

        public override string DisplayName
        {
            get { return "Delete Message"; }
        }

        public override void Execute()
        {
            var deleteMessageUIFlow = new DeleteMessageUIFlow(application.AddressBook, application.Console);
            deleteMessageUIFlow.DeleteMessage();
        }
    }
}