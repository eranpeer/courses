namespace MyCode
{
    public class PostcardEditorAdapter : PostcardEditor, IMsgEditor
    {
        public PostcardEditorAdapter(AddressBook addressBook, Console console)
            : base(addressBook, console)
        {
        }

        #region IMsgEditor Members

        public bool Edit(Message msg)
        {
            return base.Edit(msg as Postcard);
        }

        #endregion
    }
}