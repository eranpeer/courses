using Thirdparty;

namespace MyCode
{
    public class Desktop
    {
        private readonly MenuCommand mainMenu;
        private bool isActive;

        public Desktop()
        {
            mainMenu = new MenuCommand("Select an option:");
        }

        public virtual void Start()
        {
            isActive = true;
            while (isActive)
            {
                mainMenu.Execute();
            }
        }

        public void AddDesktopCommand(ICommand command)
        {
            mainMenu.AddCommand(command);
        }

        public ICommand CreateMenuCommand(string displayName)
        {
            return new MenuCommand(displayName);
        }

        public void Stop()
        {
            isActive = false;
        }

        #region Nested type: CommandMenuItem

        private class CommandMenuItem : MenuItem
        {
            private readonly ICommand command;

            public CommandMenuItem(ICommand command) : base(command.DisplayName)
            {
                this.command = command;
            }

            public override void Click()
            {
                command.Execute();
            }
        }

        #endregion

        #region Nested type: ICommand

        public interface ICommand
        {
            string DisplayName { get; }
            void Execute();
            bool AddCommand(ICommand command);
        }

        #endregion

        #region Nested type: LeafCommand

        public abstract class LeafCommand : ICommand
        {
            #region ICommand Members

            public abstract void Execute();
            public abstract string DisplayName { get; }

            public bool AddCommand(ICommand command)
            {
                return false;
            }

            #endregion
        }

        #endregion

        #region Nested type: MenuCommand

        private class MenuCommand : ICommand
        {
            private readonly Menu menu;

            public MenuCommand(string displayName)
            {
                menu = new Menu(displayName);
            }

            #region ICommand Members

            public void Execute()
            {
                menu.Show();
            }

            public string DisplayName
            {
                get { return menu.Name; }
            }

            public bool AddCommand(ICommand command)
            {
                menu.AddItem(new CommandMenuItem(command));
                return true;
            }

            #endregion
        }

        #endregion
    }
}