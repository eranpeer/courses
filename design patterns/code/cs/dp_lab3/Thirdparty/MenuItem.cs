namespace Thirdparty
{
    public class MenuItem
    {
        public MenuItem(string name)
        {
            Name = name;
        }

        public string Name { get; set; }

        public virtual void Click()
        {
        }
    }
}