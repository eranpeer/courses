using System;

namespace MyCode
{
    public class Application
    {
        private AddressBook addressBook;
        private Console console;
        private Desktop desktop;

        public AddressBook AddressBook
        {
            get { return addressBook; }
        }

        public Console Console
        {
            get { return console; }
        }

        public void Start()
        {
            console = new Console();
            var eranMbx = new Mailbox("eran");
            var ronenMbx = new Mailbox("ronen");

            addressBook = new AddressBook();

            AddressBook.AddMailbox(eranMbx);
            AddressBook.AddMailbox(ronenMbx);

            desktop = new Desktop(this);
            desktop.Start();
        }


        public void Stop()
        {
            desktop.Stop();
        }

        [STAThread]
        public static void Main(string[] args)
        {
            new Application().Start();
        }
    }
}