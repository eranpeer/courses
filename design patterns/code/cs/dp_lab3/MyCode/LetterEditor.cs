namespace MyCode
{
    public class LetterEditor
    {
        private readonly AddressBook addressBook;
        private readonly Console console;

        public LetterEditor(AddressBook addressBook, Console console)
        {
            this.addressBook = addressBook;
            this.console = console;
        }

        public bool Edit(Letter msg)
        {
            if (!MessageEditingUtils.EditAddresses(msg, addressBook, console))
                return false;
            msg.Subject = console.ReadLine("Enter letter's subject:", msg.Subject);
            msg.Content = console.ReadLine("Enter letter's content:", msg.Content);
            return true;
        }
    }
}