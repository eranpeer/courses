namespace MyCode
{
    public class SendMessageUIFlow
    {
        private readonly AddressBook addressBook;
        private readonly Console console;

        public SendMessageUIFlow(AddressBook addressBook, Console console)
        {
            this.addressBook = addressBook;
            this.console = console;
        }

        public void SendMessage()
        {
            string messageType = console.ReadLine("Enter type of message:");
            if (messageType != "cable" && messageType != "letter" && messageType != "postcard")
            {
                console.Show("invalid message type !");
                return;
            }

            Message newMessage = null;
            if (messageType == "letter")
            {
                newMessage = new Letter();
                var editor = new LetterEditor(addressBook, console);
                if (!editor.Edit((Letter) newMessage))
                    return;
            }

            if (messageType == "cable")
            {
                newMessage = new Cable();
                var editor = new CableEditor(addressBook, console);
                if (!editor.Edit((Cable) newMessage))
                    return;
            }

            if (messageType == "postcard")
            {
                newMessage = new Postcard();
                var editor = new PostcardEditor(addressBook, console);
                if (!editor.Edit((Postcard) newMessage))
                    return;
            }

            new Mailer(addressBook).Deliver(newMessage);
            console.Show("New " + messageType + " for " + newMessage.Destination + "!");
        }
    }
}