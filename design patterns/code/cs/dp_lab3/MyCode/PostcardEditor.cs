namespace MyCode
{
    public class PostcardEditor
    {
        private readonly AddressBook addressBook;
        private readonly Console console;

        public PostcardEditor(AddressBook addressBook, Console console)
        {
            this.addressBook = addressBook;
            this.console = console;
        }

        public bool Edit(Postcard msg)
        {
            if (!MessageEditingUtils.EditAddresses(msg, addressBook, console))
                return false;
            msg.Text = console.ReadLine("Enter postcard's text:", msg.Text);
            return true;
        }
    }
}