using Thirdparty;

namespace MyCode
{
    public class Desktop
    {
        private readonly Application application;
        private bool isActive;

        public Desktop(Application app)
        {
            application = app;
        }

        public virtual void Start()
        {
            var mainMenu = new Menu("Select an option:");

            mainMenu.AddItem(new SendMenuItem(application));
            mainMenu.AddItem(new DeleteMessageMenuItem(application));
            mainMenu.AddItem(new ListMessagesMenuItem(application));
            mainMenu.AddItem(new ExitMenuItem(application));

            isActive = true;
            while (isActive)
            {
                mainMenu.Show();
            }
        }

        public void Stop()
        {
            isActive = false;
        }

        #region Nested type: DeleteMessageMenuItem

        internal class DeleteMessageMenuItem : MenuItem
        {
            private readonly Application application;

            public DeleteMessageMenuItem(Application application)
                : base("Delete Message")
            {
                this.application = application;
            }

            public override void Click()
            {
                var deleteMessageUIFlow = new DeleteMessageUIFlow(application.AddressBook, application.Console);
                deleteMessageUIFlow.DeleteMessage();
            }
        }

        #endregion

        #region Nested type: ExitMenuItem

        internal class ExitMenuItem : MenuItem
        {
            private readonly Application application;

            public ExitMenuItem(Application application)
                : base("Exit")
            {
                this.application = application;
            }

            public override void Click()
            {
                application.Stop();
            }
        }

        #endregion

        #region Nested type: ListMessagesMenuItem

        internal class ListMessagesMenuItem : MenuItem
        {
            private readonly Application application;

            public ListMessagesMenuItem(Application application)
                : base("List Messages")
            {
                this.application = application;
            }

            public override void Click()
            {
                var listMessagesUIFlow = new ListMessagesUIFlow(application.AddressBook, application.Console);
                listMessagesUIFlow.ListMailbox();
            }
        }

        #endregion

        #region Nested type: SendMenuItem

        internal class SendMenuItem : MenuItem
        {
            private readonly Application application;

            public SendMenuItem(Application application)
                : base("Send Message")
            {
                this.application = application;
            }

            public override void Click()
            {
                var sendMessageUIFlow = new SendMessageUIFlow(application.AddressBook, application.Console);
                sendMessageUIFlow.SendMessage();
            }
        }

        #endregion
    }
}