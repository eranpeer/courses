using System;

namespace MyCode
{
    internal class DeleteMessageUIFlow
    {
        private readonly AddressBook addressBook;
        private readonly Console console;

        public DeleteMessageUIFlow(AddressBook addressBook, Console console)
        {
            this.addressBook = addressBook;
            this.console = console;
        }

        public virtual void DeleteMessage()
        {
            try
            {
                string mbxAddress = console.ReadLine("Delete message from Mailbox:");
                Mailbox mbx = addressBook.GetMailbox(mbxAddress);

                if (mbx == null)
                {
                    console.Show(mbxAddress + " is not a valid address !");
                    return;
                }

                Int32 messageId = Int32.Parse(console.ReadLine("Enter message id:"));
                Message deletedMsg = mbx.Delete(messageId);

                if (deletedMsg != null)
                    console.Show(BuildTitle(deletedMsg) + " was deleted from mailbox " + mbxAddress);
                else
                {
                    console.Show("invalid message id!");
                }
            }
            catch (Exception)
            {
                console.Show("invalid message id !");
            }
        }

        private static string BuildTitle(Message deletedMsg)
        {
            string title = null;
            if (deletedMsg is Letter)
            {
                var msg = (Letter) deletedMsg;
                title = "Letter: " + msg.Subject;
            }
            if (deletedMsg is Cable)
            {
                var msg = (Cable) deletedMsg;
                title = "Cable: " + msg.Content;
            }
            if (deletedMsg is Postcard)
            {
                var msg = (Postcard) deletedMsg;
                title = "Postcard: from " + msg.Origin;
            }
            return title;
        }
    }
}