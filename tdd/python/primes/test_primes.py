import pytest


def primes(num) -> list:
    result = []

    divider = 2
    while num > 1:
        while num % divider == 0:
            result.append(divider)
            num //= divider
        divider += 1

    return result


def test_primes():
    assert primes(1) == []
    assert primes(2) == [2]
    assert primes(3) == [3]
    assert primes(4) == [2, 2]
    assert primes(5) == [5]
    assert primes(6) == [2, 3]
    assert primes(7) == [7]
    assert primes(8) == [2, 2, 2]
    assert primes(9) == [3, 3]
    assert primes(10) == [2, 5]
    assert primes(11) == [11]
    assert primes(12) == [2, 2, 3]
    assert primes(13) == [13]
    assert primes(14) == [2, 7]
    assert primes(15) == [3, 5]
    assert primes(25) == [5, 5]
    assert primes(2 * 2 * 5 * 7 * 7 * 11) == [2, 2, 5, 7, 7, 11]
