from sqlalchemy import Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import sessionmaker

from parkingservice import *
from parkingservice_tests import *
from parkingapp import ParkingApp
from sqlalchemy import create_engine


class TimerMock(Timer):
    def __init__(self):
        self.curr_time = 0

    def set_time(self, curr_time: float):
        self.curr_time = curr_time

    def now(self):
        return self.curr_time


engine = create_engine('sqlite:///:memory:', echo=True)
Session = sessionmaker(bind=engine)
Base = declarative_base()
session = Session()


class ParkingEntry(Base):
    __tablename__ = 'entries'
    id = Column(Integer, primary_key=True)
    time = Column(Float)


class OrmParkingStorage(ParkingStorage):
    def __init__(self):
        pass

    def add_parking_entry(self, entry_time):
        entry = ParkingEntry(time=entry_time)
        session.add(entry)
        session.commit()
        return entry.id

    def get_entry_time(self, card_id):
        entry = session.query(ParkingEntry).get(card_id)
        return entry.time


Base.metadata.create_all(engine)

timer_mock = TimerMock()

Base.metadata.create_all(engine)
app = ParkingApp(timer_mock, OrmParkingStorage())


@app.app.route("/test/set_time/<curr_time>")
def set_time(curr_time):
    timer_mock.set_time(float(curr_time))
    return "done", 200


app.run()
