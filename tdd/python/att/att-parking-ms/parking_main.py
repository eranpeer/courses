from parkingservice import *
from parkingservice_tests import *
from parkingapp import ParkingApp


class TimerMock(Timer):
    def __init__(self):
        self.curr_time = 0

    def set_time(self, curr_time: float):
        self.curr_time = curr_time

    def now(self):
        return self.curr_time


class ParkingStorageFake(ParkingStorage):
    def __init__(self):
        self.entries = {}
        self.last_card_id = 0

    def add_parking_entry(self, entry_time):
        card_id = self.last_card_id + 1
        self.last_card_id = card_id
        self.entries[card_id] = entry_time
        return card_id

    def get_entry_time(self, card_id):
        entry_time = self.entries[card_id]
        return entry_time


app = ParkingApp(TimerMock(), ParkingStorageFake())

app.run()
