from flask import Flask

from parkingservice import Timer, ParkingStorage, ParkingService


class ParkingApp(object):
    def __init__(self, timer: Timer, storage: ParkingStorage):
        self.timer = timer
        self.storage = storage
        self.app = Flask(__name__)
        self.app.add_url_rule("/", "hello_world", self.hello_world, methods=['GET', ])
        self.app.add_url_rule("/enter", "enter_parking", self.enter_parking, methods=['GET', ])
        self.app.add_url_rule("/calc_payment/<card_id>", "calc_payment", self.calc_payment, methods=['GET', ])

    # app.route('/')
    def hello_world(self):
        return 'Hello, World!'

    # @app.route('/enter')
    def enter_parking(self):
        return str(self.parking.enter_parking())

    # @app.route('/calc_payment/<card_id>')
    def calc_payment(self, card_id):
        return str(self.parking.calc_payment(int(card_id)))

    def run(self):
        self.parking = ParkingService(self.timer, self.storage)
        self.app.run(port=80)
