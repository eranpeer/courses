import pytest
import requests

from parkingservice import Timer, ParkingStorage


class TimerClient(object):
    def set_time(self, curr_time: float):
        requests.get("http://127.0.0.1:80/test/set_time/{}".format(curr_time))
        pass


class ParkingServiceClient(object):
    def enter_parking(self) -> int:
        r = requests.get("http://127.0.0.1:80/enter")
        return int(r.text)

    def calc_payment(self, card_id: int) -> int:
        r = requests.get("http://127.0.0.1:80/calc_payment/{}".format(card_id))
        return int(r.text)


@pytest.fixture
def timer():
    return TimerClient()


@pytest.fixture
def parking_service():
    return ParkingServiceClient()


from parkingservice_tests import *


def test_dummy():
    pass
