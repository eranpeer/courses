from flask import Flask
from flask import request

from postfix import Postfix

app = Flask(__name__)


@app.route('/hello')
def hello():
    return "hello world", 200


@app.route('/calc', methods=['POST'])
def calc():
    json = request.get_json()
    expression = json['expression']
    result = Postfix().calc(expression)
    return str(result), 200
