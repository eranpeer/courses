import pytest
import requests

from postfix import Postfix


class PostfixClient(object):
    def calc(self, expression: str) -> float:
        expression_json = {"expression": expression}
        post = requests.post(url="http://localhost:8080/calc", json=expression_json)
        return float(post.text)


@pytest.fixture
def postfix():
    return PostfixClient()


from postfix_spec import *


def test_demo():
    pass
