import pytest

from parkingservice import Timer, ParkingStorage


class TimerMock(Timer):
    def __init__(self):
        self.curr_time = 0

    def set_time(self, curr_time: float):
        self.curr_time = curr_time

    def now(self):
        return self.curr_time


class ParkingStorageFake(ParkingStorage):
    def __init__(self):
        self.entries = {}
        self.last_card_id = 0

    def add_parking_entry(self, entry_time):
        card_id = self.last_card_id + 1
        self.last_card_id = card_id
        self.entries[card_id] = entry_time
        return card_id

    def get_entry_time(self, card_id):
        entry_time = self.entries[card_id]
        return entry_time


@pytest.fixture
def timer():
    return TimerMock()


@pytest.fixture
def storage():
    return ParkingStorageFake()


@pytest.fixture
def parking_service(timer: Timer, storage):
    return ParkingService(timer, storage)


from parkingservice_tests import *
