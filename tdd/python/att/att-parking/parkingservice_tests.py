import pytest

from parkingservice import Timer, ParkingService
from test_parkingservice import TimerMock


def test_pay_nothing_on_first_10_min(parking_service: ParkingService, timer: TimerMock):
    check_payment(parking_service, timer, entry_time=0, payment_time=(10 * 60 - 1), expected_payment=0)


def test_pay_10_after_first_10_min(parking_service: ParkingService, timer: TimerMock):
    check_payment(parking_service, timer, entry_time=0, payment_time=(10 * 60), expected_payment=10)


def test_pay_20_after_first_10_min_of_second_hour(parking_service: ParkingService, timer: TimerMock):
    check_payment(parking_service, timer, entry_time=0, payment_time=((60 + 10) * 60), expected_payment=20)


def test_use_entry_time_of_card_id(parking_service: ParkingService, timer: TimerMock):
    timer.set_time(0)
    card_id1 = parking_service.enter_parking()
    timer.set_time(10 * 60)
    parking_service.enter_parking()
    amount_to_pay = parking_service.calc_payment(card_id1)
    assert amount_to_pay == 10


@pytest.mark.parametrize(
    "entry_time,payment_time,expected_payment", [
        (0, 10 * 60 - 1, 0),
        (0, 10 * 60, 10),
        (0, 70 * 60, 20),
    ])
def test_payment(parking_service, timer, entry_time, payment_time, expected_payment):
    check_payment(parking_service, timer, entry_time, payment_time, expected_payment)


def check_payment(parking_service, timer, entry_time, payment_time, expected_payment):
    timer.set_time(entry_time)
    card_id = parking_service.enter_parking()
    timer.set_time(payment_time)
    amount_to_pay = parking_service.calc_payment(card_id)
    assert amount_to_pay == expected_payment
