from abc import ABC, abstractmethod


class Timer(ABC):
    def now(self):
        pass


class ParkingStorage(ABC):
    @abstractmethod
    def add_parking_entry(self, entry_time):
        pass

    @abstractmethod
    def get_entry_time(self, card_id):
        pass


class ParkingService:
    def __init__(self, timer: Timer, parking_storage: ParkingStorage):
        self.timer = timer
        self.storage = parking_storage

    def enter_parking(self) -> int:
        entry_time = self.timer.now()
        return self.storage.add_parking_entry(entry_time)

    def calc_payment(self, card_id: int) -> int:
        payment_time = self.timer.now()
        entry_time = self.storage.get_entry_time(card_id)
        return self.calc_amount_to_pay(entry_time, payment_time)

    @staticmethod
    def calc_amount_to_pay(entry_time, payment_time):
        time_in_parking = payment_time - entry_time
        hours_to_pay = (time_in_parking + 50 * 60) // (60 * 60)
        pay = 10 * hours_to_pay
        return int(pay)
