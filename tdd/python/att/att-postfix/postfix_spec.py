def test_return_operator_on_one_operator_expression(postfix):
    assert postfix.calc("0") == 0.0
    assert postfix.calc("10") == 10.0


def test_two_operands_expression(postfix):
    assert postfix.calc("1,2+") == 3.0
    assert postfix.calc("3,2-") == 1.0
    assert postfix.calc("3,2*") == 6.0
    assert postfix.calc("6,2/") == 3.0


def test_three_operands_expression(postfix):
    assert postfix.calc("1,2,3++") == 6.0


def test_complex(postfix):
    assert postfix.calc("1,2+3+") == 6.0