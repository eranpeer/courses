class StackCalculator:
    def __init__(self):
        self.operands = []

    def top(self):
        return self.operands[len(self.operands) - 1]

    def push(self, operand):
        self.operands.append(operand)

    def apply_operator(self, c):
        if c == '+':
            self.push(self.operands.pop() + self.operands.pop())
        elif c == '-':
            self.push(-self.operands.pop() + self.operands.pop())
        elif c == '*':
            self.push(self.operands.pop() * self.operands.pop())
        elif c == '/':
            self.push(1.0 / self.operands.pop() * self.operands.pop())

    def clear(self):
        self.operands = []


class Postfix(object):
    def __init__(self):
        self.operand_token = ""
        self.stack_calculator = StackCalculator()

    def calc(self, expression: str) -> float:
        self.stack_calculator.clear()
        for c in expression:
            if c.isdigit():
                self.operand_token += c
                continue

            if len(self.operand_token) > 0:
                operand = self.take_token()
                self.stack_calculator.push(operand)

            if c == ',':
                continue

            self.stack_calculator.apply_operator(c)

        if len(self.operand_token) > 0:
            operand = float(self.operand_token)
            self.stack_calculator.push(operand)

        return self.stack_calculator.top()

    def take_token(self):
        operand = float(self.operand_token)
        self.operand_token = ""
        return operand

