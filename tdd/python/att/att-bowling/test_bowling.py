import pytest

from bowling import GameServiceImpl
from bowling_spec import *


@pytest.fixture
def game():
    return GameServiceImpl()


class TestContext:
    def reset(self):
        pass


@pytest.fixture
def test_context():
    return TestContext()
