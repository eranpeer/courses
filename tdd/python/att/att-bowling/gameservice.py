import abc


class GameService:
    @abc.abstractmethod
    def roll(self, pins):
        pass

    @abc.abstractmethod
    def score(self) -> int:
        pass

    @abc.abstractmethod
    def reset(self):
        pass
