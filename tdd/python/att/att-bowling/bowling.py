from abc import abstractmethod, ABCMeta

from gameservice import GameService


class GameServiceImpl(GameService):
    def __init__(self):
        self.rolls = []

    def reset(self):
        self.rolls = []

    def is_spare(self, i):
        return self.rolls[i] + self.rolls[i + 1] == 10

    def roll(self, pins):
        self.rolls.append(pins)

    def score(self) -> int:
        result = 0
        first_in_frame = 0
        try:
            for frame in range(10):
                if self.is_strike(first_in_frame):  # strike
                    result += 10 + self.strike_bonus(first_in_frame)
                    first_in_frame += 1
                elif self.is_spare(first_in_frame):  # spare
                    result += 10 + self.spare_bonus(first_in_frame)
                    first_in_frame += 2
                else:
                    result += self.simple_frame_score(first_in_frame)
                    first_in_frame += 2
        except IndexError:
            pass
        return result

    def strike_bonus(self, first_in_frame):
        return self.rolls[first_in_frame + 1] + self.rolls[first_in_frame + 2]

    def simple_frame_score(self, i):
        return self.rolls[i] + self.rolls[i + 1]

    def spare_bonus(self, i):
        return self.rolls[i + 2]

    def is_strike(self, first_in_frame):
        return self.rolls[first_in_frame] == 10
