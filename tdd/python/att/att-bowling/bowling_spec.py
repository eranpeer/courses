import pytest

from bowling import GameService


def test_empty_game_should_score_0(game: GameService):
    # game.reset()
    assert game.score() == 0


def test_simple_frame_should_score_num_of_falling_pins(game: GameService):
    # game.reset()
    game.roll(1)
    game.roll(2)
    assert game.score() == 3


def test_should_add_next_roll_as_bonus_on_spare(game: GameService):
    # game.reset()
    roll_spare(game)
    game.roll(1)
    game.roll(0)
    assert game.score() == 12


def test_should_add_next_2_rolls_as_bonus_on_strike(game: GameService):
    # game.reset()
    game.roll(10)  # strike
    game.roll(1)
    game.roll(2)
    assert game.score() == 16


def test_perfect_game_should_score_300(game: GameService):
    # game.reset()
    for i in range(12):
        game.roll(10)
    assert game.score() == 300


def roll_spare(game: GameService):
    game.roll(1)
    game.roll(9)
