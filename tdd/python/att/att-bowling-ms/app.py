from flask import Flask

from bowling import GameServiceImpl

app = Flask(__name__)

game = GameServiceImpl()


@app.route("/")
def say_hello():
    return "hello world"


@app.route("/reset")
def reset():
    global game
    game = GameServiceImpl()
    # game.reset()
    return "done", 200


@app.route("/score")
def score():
    return str(game.score()), 200


@app.route("/roll/<int:pins>")
def roll(pins):
    game.roll(pins)
    return "done", 200


app.run(port=80)
