import pytest
import requests
from bowling_spec import *

from gameservice import GameService


class GameServiceRestClient(GameService):
    def __init__(self):
        requests.get("http://localhost/reset")

    def score(self) -> int:
        return int(requests.get("http://localhost/score").text)

    def roll(self, pins):
        requests.get("http://localhost/roll/{}".format(pins))

    def reset(self):
        requests.get("http://localhost/reset")


@pytest.fixture
def game():
    return GameServiceRestClient()

def test_demo():
    pass
