from linewrap import wrap

def test_wrap():
    assert wrap("", 1) == ""
    assert wrap("X", 1) == "X"
    assert wrap("XX", 1) == "X\nX"
    assert wrap("XXX", 1) == "X\nX\nX"
    assert wrap("X X", 2) == "X\nX"
    assert wrap("X XX", 3) == "X\nXX"
