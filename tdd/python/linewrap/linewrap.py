def wrap(text: str, line_length: int) -> str:
    if len(text) <= line_length:
        return text
    split_point = find_split_point(line_length, text)
    return text[0:split_point] + '\n' + wrap(text[split_point:].strip(), line_length)


def find_split_point(line_length, text) -> int:
    split_point = text[:line_length].rfind(' ')
    if split_point == -1:
        split_point = line_length
    return split_point
