from array import array


def calc(expression: str):
    operands = []
    token = []
    for c in expression:
        if c.isdigit():
            token.append(c)
            continue
        if c == ',':
            push_token(operands, token)
            continue
        push_operand_if_exists(operands, token)
        apply_operator(c, operands)

    push_operand_if_exists(operands, token)
    return operands.pop()


def push_token(operands, token):
    join = use_token(token)
    operand = float(join)
    operands.append(operand)


def apply_operator(operator, operands):
    if operator == '+':
        operands.append(operands.pop() + operands.pop())
    elif operator == '-':
        operands.append(-operands.pop() + operands.pop())
    elif operator == '*':
        operands.append(operands.pop() * operands.pop())
    elif operator == '/':
        operands.append(1.0 / operands.pop() * operands.pop())


def push_operand_if_exists(operands, token):
    if len(token) > 0:
        push_token(operands, token)


def use_token(token):
    result = ''.join(token)
    token.clear()
    return result
