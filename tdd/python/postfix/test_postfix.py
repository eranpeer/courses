from postfix import calc


def test_one_operand_expression_should_return_operand():
    assert calc('0') == 0.0
    assert calc('10') == 10.0


def test_two_operand_expression():
    assert calc('1,2+') == 3.0
    assert calc('2,1-') == 1.0
    assert calc('3,2*') == 6.0
    assert calc('6,2/') == 3.0


def test_two_consecutive_operators():
    assert calc('1,2,3++') == 6.0
