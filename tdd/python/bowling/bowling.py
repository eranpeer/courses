def is_spare(rolls, i) -> bool:
    return rolls[i] + rolls[i + 1] == 10


def is_strike(rolls, i) -> bool:
    return rolls[i] == 10


def simple_frame_score(rolls, i) -> int:
    return rolls[i] + rolls[i + 1]


def score(rolls: list) -> int:
    _score = 0
    try:
        first_in_frame = 0;
        for frame in range(10):
            if is_strike(rolls,first_in_frame):
                _score += 10 + rolls[first_in_frame + 1] + rolls[first_in_frame + 2]
                first_in_frame += 1
            elif is_spare(rolls, first_in_frame):
                _score += 10 + rolls[first_in_frame + 2]
                first_in_frame += 2
            else:
                _score += simple_frame_score(rolls,first_in_frame)
                first_in_frame += 2
    except IndexError:
        pass
    return _score
