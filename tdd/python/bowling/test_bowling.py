from bowling import score


def test_empty_game_should_score_0():
    assert score([]) == 0


def test_simple_frame_should_score_num_off_falling_pins():
    assert score([1, 2]) == 3


def test_should_add_next_roll_as_bonus_on_spare():
    assert score([1, 9, 1, 0]) == 12


def test_should_add_next_2_rolls_as_bonus_on_strike():
    assert score([10, 1, 2]) == 16


def test_perfect_game_should_score_300():
    assert score([10] * 12) == 300
