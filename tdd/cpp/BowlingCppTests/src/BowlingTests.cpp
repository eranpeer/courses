//============================================================================
// Name        : BowlingTests.cpp
// Copyright   : Your copyright notice
//============================================================================
#include "gtest/gtest.h"
#include "BowlingGame.h"

void rollMany(BowlingGame & g, int times, int pins) {
	for (int i = 0; i < times; i++)
		g.roll(pins);
}

void rollSpare(BowlingGame & g) {
	g.roll(9);
	g.roll(1);
}

TEST(BowlingGame, ScoreZeroOnZeroGame) {
	BowlingGame g;
	rollMany(g, 20, 0);
	EXPECT_EQ(0, g.score());
}

TEST(BowlingGame, ScoreSumOfPinsOnSimpleGame) {
	BowlingGame g;
	rollMany(g, 20, 1);
	EXPECT_EQ(20, g.score());
}

TEST(BowlingGame, AddSpareBonusOnSpare) {
	BowlingGame g;
	rollSpare(g);
	g.roll(1);
	g.roll(0);
	rollMany(g, 8 * 2, 0);

	EXPECT_EQ(12, g.score());
}


TEST(BowlingGame, AddStrikeBonusOnStrike) {
	BowlingGame g;
	g.roll(10);
	g.roll(1);
	g.roll(2);
	rollMany(g, 8 * 2, 0);

	EXPECT_EQ(16, g.score());
}
