#include "stdafx.h"
#include "CppUnitTest.h"
#include "PostfixCalc.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace PostfixCalcTests
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(OneOperandExpressionShouldReturnOperand)
		{
			postfix::PostfixCalc pc;
			Assert::AreEqual(0, pc.calc(std::string("0")), 0);
			Assert::AreEqual(10, pc.calc(std::string("10")), 0);
		}

		TEST_METHOD(TwoOperandExpression)
		{
			postfix::PostfixCalc pc;
			Assert::AreEqual(3, pc.calc(std::string("1,2+")), 0);
			Assert::AreEqual(1, pc.calc(std::string("3,2-")), 0);
			Assert::AreEqual(6, pc.calc(std::string("3,2*")), 0);
			Assert::AreEqual(2, pc.calc(std::string("6,3/")), 0);
		}

		TEST_METHOD(ComplexExpression)
		{
			postfix::PostfixCalc pc;
			Assert::AreEqual(6, pc.calc(std::string("1,2,3++")), 0);
		}

	};
}