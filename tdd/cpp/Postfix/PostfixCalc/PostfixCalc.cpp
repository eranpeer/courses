#include "PostfixCalc.h"
#include <cctype>
#include <stack>

postfix::PostfixCalc::PostfixCalc()
{
}

postfix::PostfixCalc::~PostfixCalc()
{
}

template <class T>
static T pop(std::stack<T> & stack) {
	auto top = stack.top();
	stack.pop();
	return top;
}

static void pushOperandIfExists(std::stack<float> &stack, std::string &operand)
{
	if (operand.length() == 0)
		return;
	stack.push(std::stof(operand));
	operand.clear();
}

void applyOperator(char c, std::stack<float> &stack)
{
	if (c == '+'){
		auto right = pop(stack);
		auto left = pop(stack);
		stack.push(left + right);
	}
	else if (c == '-'){
		auto right = pop(stack);
		auto left = pop(stack);
		stack.push(left - right);
	}
	else if (c == '*'){
		auto right = pop(stack);
		auto left = pop(stack);
		stack.push(left * right);
	}
	else if (c == '/'){
		auto right = pop(stack);
		auto left = pop(stack);
		stack.push(left / right);
	}
}

float postfix::PostfixCalc::calc(const std::string &expresison)
{	
	std::stack<float> stack;
	std::string operand;
	for each (char c in expresison)
	{
		if (std::isdigit(c)){
			operand += c;
		}
		else {
			pushOperandIfExists(stack, operand);
			applyOperator(c, stack);
		}
	}
	pushOperandIfExists(stack, operand);
	return stack.top();
}

