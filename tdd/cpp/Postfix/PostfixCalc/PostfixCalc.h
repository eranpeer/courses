#ifndef PostfixCalc_h__
#define PostfixCalc_h__
#include <string>

namespace postfix
{

	class PostfixCalc
	{
	public:
		PostfixCalc();
		~PostfixCalc();
		float calc(const std::string &expresison);
	};

}
#endif // PostfixCalc_h__