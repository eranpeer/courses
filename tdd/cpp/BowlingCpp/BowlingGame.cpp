/*
 * BowlingGame.cpp
 *
 *  Created on: 6 ���� 2013
 *      Author: eran
 */

#include "BowlingGame.h"

BowlingGame::BowlingGame() {
}

BowlingGame::~BowlingGame() {
}

void BowlingGame::roll(int pins) {
	_rolls[_countRolls] = pins;
	_countRolls++;
}

bool BowlingGame::isSpare(int i) {
	return _rolls[i] + _rolls[i + 1] == 10;
}

int BowlingGame::spareBonus(int i) {
	return _rolls[i + 2];
}

int BowlingGame::simpleFrameScore(int i) {
	return _rolls[i] + _rolls[i + 1];
}

bool BowlingGame::isStrike(int startFrameIndex) {
	return _rolls[startFrameIndex] == 10;
}

int BowlingGame::strikeBonus(int startFrameIndex) {
	return _rolls[startFrameIndex + 1] + _rolls[startFrameIndex + 2];
}

int BowlingGame::score() {
	int result = 0;
	int startFrameIndex = 0;
	for (int frame = 0; frame < 10; frame++) {
		if (isStrike(startFrameIndex)) {
			result += 10 + strikeBonus(startFrameIndex);
			startFrameIndex += 1;
		} else if (isSpare(startFrameIndex)) {
			result += 10 + spareBonus(startFrameIndex);
			startFrameIndex += 2;
		} else {
			result += simpleFrameScore(startFrameIndex);
			startFrameIndex += 2;
		}
	}
	return result;
}
