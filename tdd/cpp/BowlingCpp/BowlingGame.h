/*
 * BowlingGame.h
 *
 *  Created on: 6 ���� 2013
 *      Author: eran
 */

#ifndef BOWLINGGAME_H_
#define BOWLINGGAME_H_

class BowlingGame {
public:
	BowlingGame();
	virtual ~BowlingGame();

	void roll(int pins);

	int score();

private:
	int _rolls[21] = {};
	int _countRolls = 0;

	bool isSpare(int i);
	int spareBonus(int i);
	int simpleFrameScore(int i);
	bool isStrike(int startFrameIndex);
	int strikeBonus(int startFrameIndex);
};


#endif /* BOWLINGGAME_H_ */
