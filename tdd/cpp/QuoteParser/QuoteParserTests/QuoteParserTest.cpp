#include "stdafx.h"
#include "CppUnitTest.h"

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <array>
#include <regex>

#include "hippomocks.h"
#include "QuoteParser.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace quotes;

namespace QuoteParserTests
{		

	TEST_CLASS(QuoteParserTest)
	{
	public:

		class TextReaderMock : public TextReader {
		public:
			TextReaderMock(std::vector<std::string> &lines) : lines(lines){}
			bool readLine(std::string &line){
				if (lines.size() == 0)
					return false;
				line = *lines.begin();
				lines.erase(lines.begin());
				return true;
			}
		private:
			std::vector<std::string> lines;
		};

		TEST_METHOD(ReturnNullOnEmptyStream)
		{
			QuoteParser::Quote quote;
			std::vector<std::string>lines{};
			TextReaderMock readerMock(lines);
			QuoteParser quoteParser(readerMock);
			Assert::IsFalse(quoteParser.next(quote));
		}

		TEST_METHOD(ReturnNullOnEmptyStream_HippoMocks)
		{
			MockRepository mocks;
			TextReader *readerMock = mocks.InterfaceMock<TextReader>();
			mocks.ExpectCall(readerMock, TextReader::readLine).Do([](std::string &line){return false; });
			QuoteParser::Quote quote;
			QuoteParser quoteParser(*readerMock);
			Assert::IsFalse(quoteParser.next(quote));
		}

		TEST_METHOD(ParseLines)
		{
			QuoteParser::Quote quote;
			std::vector<std::string>lines{"ATT,1.1,2"};
			TextReaderMock readerMock(lines);
			QuoteParser quoteParser(readerMock);
			Assert::IsTrue(quoteParser.next(quote));
			Assert::AreEqual(std::string("ATT"),quote.getSymbol());
			Assert::AreEqual(1.1, quote.getBid());
			Assert::AreEqual(2.0, quote.getAsk());
			Assert::IsFalse(quoteParser.next(quote));
		}

		TEST_METHOD(ParseLines_HippoMocks)
		{
			MockRepository mocks;
			TextReader *readerMock = mocks.InterfaceMock<TextReader>();
			mocks.ExpectCall(readerMock, TextReader::readLine).Do([](std::string &line){
				line = "ATT,1.1,2";
				return true;
			});
			mocks.ExpectCall(readerMock, TextReader::readLine).Return(false);
			QuoteParser::Quote quote;
			QuoteParser quoteParser(*readerMock);
			Assert::IsTrue(quoteParser.next(quote));
			Assert::AreEqual(std::string("ATT"), quote.getSymbol());
			Assert::AreEqual(1.1, quote.getBid());
			Assert::AreEqual(2.0, quote.getAsk());
			Assert::IsFalse(quoteParser.next(quote));
		}


	};
}