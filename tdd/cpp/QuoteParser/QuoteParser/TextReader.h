#ifndef TextReader_h__
#define TextReader_h__

#include <string>

struct TextReader
{
	virtual ~TextReader(){};
	virtual bool readLine(std::string &line) = 0;
};
#endif // TextReader_h__


