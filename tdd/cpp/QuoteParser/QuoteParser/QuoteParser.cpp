#include "QuoteParser.h"
#include <regex>

std::vector<std::string> quotes::QuoteParser::split(const std::string& input)
{
	std::regex splitter{ ',' };
	std::sregex_token_iterator first(input.begin(), input.end(), splitter, -1);
	std::sregex_token_iterator last;
	return { first, last };
}
