#ifndef QuoteParser_h__
#define QuoteParser_h__

#include <string>
#include <vector>
#include "TextReader.h"

namespace quotes
{

	class QuoteParser {
	public:

		class Quote {
		public:
			Quote() : bid(0), ask(0){}
			void setSymbol(const std::string& value) {
				symbol = value;
			}
			const std::string & getSymbol() const { return symbol; }

			double getBid() const { return bid; }
			void setBid(double value){ bid = value; }

			double getAsk() const { return ask; }
			void setAsk(double value){ ask = value; }

		private:
			std::string symbol;
			double bid;
			double ask;
		};

		QuoteParser(TextReader& reader) : reader(reader){}

		bool next(Quote& q){
			std::string line;
			if (!reader.readLine(line)){
				return false;
			}
			parseLine(line, q);
			return true;
		}

		void parseLine(std::string line, Quote &q)
		{
			auto tokens = split(line);
			q.setSymbol(tokens[0]);
			q.setBid(std::stod(tokens[1]));
			q.setAsk(std::stod(tokens[2]));
		}

	private:
		TextReader& reader;

		std::vector<std::string> split(const std::string& input);
	};

}
#endif // QuoteParser_h__
