package ui;
import thirdparty.Console;
import messaging.AddressBook;
import messaging.Letter;

public class LetterEditor {

	private AddressBook addressBook;

	public LetterEditor(AddressBook addressBook) {
		this.addressBook = addressBook;
	}

	public boolean edit(Letter msg) {
		Console console = new Console();
		if (!MessageEditingUtils.editAddresses(msg, addressBook, console))
			return false;
		readSubject(msg, console);
		readContnet(msg, console);
		return true;
	}

	private void readContnet(Letter msg, Console console) {
		String messageContent = console.readLine("Enter letter's content:");
		if (!messageContent.trim().equals("")) {
			msg.setContent(messageContent);
		}
	}

	private void readSubject(Letter msg, Console console) {
		String messageSubject = console.readLine("Enter letter's subject:");
		if (!messageSubject.trim().equals("")) {
			msg.setSubject(messageSubject);
		}
	}

}
