package ui;
import thirdparty.Console;
import messaging.AddressBook;
import messaging.Postcard;

public class PostcardEditor {

	private AddressBook addressBook;

	public PostcardEditor(AddressBook addressBook) {
		this.addressBook = addressBook;
	}

	public boolean edit(Postcard msg) {
		Console console = new Console();
		if (!MessageEditingUtils.editAddresses(msg, addressBook, console))
			return false;
		readText(msg, console);
		return true;
	}

	private void readText(Postcard msg, Console console) {
		String messageText = console.readLine("Enter postcard's text:");
		if (!messageText.trim().equals("")) {
			msg.setText(messageText);
		}
	}

}