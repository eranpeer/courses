package ui;
import java.util.Enumeration;
import java.util.Vector;

import thirdparty.Console;

public class Menu {

	private final Console console;
	private Vector<MenuItem> items;
	private String name;

	public Menu(Console console, String name) {
		this.console = console;
		items = new Vector<MenuItem>();
		this.name = name;
	}

	public void addItem(MenuItem item) {
		items.addElement(item);
	}

	public void show() {
		console.writeLine(name);
		Enumeration<MenuItem> e = items.elements();
		int i = 1;
		while (e.hasMoreElements()) {
			console.writeLine(i + ". " + ((MenuItem) e.nextElement()).getName());
			i++;
		}
		try {
			String option = console.readLine("Select Option:");
			Integer intOption = new Integer(option);
			MenuItem item = (MenuItem) items
					.elementAt(intOption.intValue() - 1);
			item.click();
		} catch (Exception ex) {
			console.writeLine("Invalid Option !!");
		}
	}

}