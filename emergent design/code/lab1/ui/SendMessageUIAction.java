package ui;
import thirdparty.Console;
import messaging.AddressBook;
import messaging.Cable;
import messaging.Letter;
import messaging.Mailer;
import messaging.Message;
import messaging.Postcard;


public class SendMessageUIAction {
	private final AddressBook addressBook;
	private final Console console = new Console();

	public SendMessageUIAction(AddressBook addressBook) {
		this.addressBook = addressBook;
	}

	public void sendMessage() {
		Message newMessage = null;
		String messageType = null;

		messageType = console.readLine("Enter type of message:");
		if (!messageType.equals("cable") && !messageType.equals("letter")
				&& !messageType.equals("postcard")) {
			console.writeLine("invalid message type: " + messageType);
			return;
		}

		if (messageType.equals("letter")) {
			newMessage = new Letter();
			LetterEditor editor = new LetterEditor(addressBook);
			if (!editor.edit((Letter) newMessage))
				return;
		}

		if (messageType.equals("cable")) {
			newMessage = new Cable();
			CableEditor editor = new CableEditor(addressBook);
			if (!editor.edit((Cable) newMessage))
				return;
		}

		if (messageType.equals("postcard")) {
			newMessage = new Postcard();
			PostcardEditor editor = new PostcardEditor(addressBook);
			if (!editor.edit((Postcard) newMessage))
				return;
		}

		new Mailer(addressBook).deliver(newMessage);
		console.writeLine("New " + messageType + " for "
				+ newMessage.getDestination() + "!");
	}

}