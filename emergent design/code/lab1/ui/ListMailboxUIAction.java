package ui;

import messaging.AddressBook;
import messaging.Mailbox;
import thirdparty.Console;

public class ListMailboxUIAction {

	private final AddressBook addressBook;
	private final Console console = new Console();

	public ListMailboxUIAction(AddressBook addressBook) {
		this.addressBook = addressBook;
	}

	public void listMailbox() {
		String address = console.readLine("Enter mailbox address:");
		Mailbox mbx = addressBook.getMailbox(address);
		if (mbx == null) {
			console.writeLine("invalid mailbox address");
			return;
		}
		mbx.show(mbx, console);
	}
}