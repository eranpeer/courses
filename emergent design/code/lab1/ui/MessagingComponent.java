package ui;
import messaging.AddressBook;

public class MessagingComponent {

	private ListMailboxUIAction listMailboxUIAction;
	private SendMessageUIAction sendMessageUIAction;
	private DeleteMessageUIAction deleteMessageUIAction;

	public MessagingComponent(AddressBook addressBook) {
		listMailboxUIAction = new ListMailboxUIAction(addressBook);
		sendMessageUIAction = new SendMessageUIAction(addressBook);
		deleteMessageUIAction = new DeleteMessageUIAction(addressBook);
	}

	public void listMailbox() {
		listMailboxUIAction.listMailbox();
	}

	public void sendMessage() {
		sendMessageUIAction.sendMessage();
	}

	void deleteMessage() {
		deleteMessageUIAction.deleteMessage();
	}

}