package ui;
import thirdparty.Console;
import messaging.AddressBook;
import messaging.Mailbox;

public class DeleteMessageUIAction {
	
	private final Console console = new Console();
	private final AddressBook addressBook;

	public DeleteMessageUIAction(AddressBook addressBook) {
		this.addressBook = addressBook;
	}

	public void deleteMessage() {
		String mbxAddress = console.readLine("Delete message from Mailbox:");
		Mailbox mbx = addressBook.getMailbox(mbxAddress);

		if (mbx == null) {
			console.writeLine(mbxAddress + " is not a valid address !");
			return;
		}

		try {
			Integer messageId = new Integer(
					console.readLine("Enter message id:"));
			mbx.delete(messageId.intValue() - 1);
		} catch (Exception e) {
			console.writeLine("invalid message id !");
		}
	}

}