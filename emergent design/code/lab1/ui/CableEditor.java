package ui;
import thirdparty.Console;
import messaging.AddressBook;
import messaging.Cable;

public class CableEditor extends java.lang.Object {

	private AddressBook addressBook;

	public CableEditor(AddressBook addressBook) {
		this.addressBook = addressBook;
	}

	public boolean edit(Cable msg) {
		Console console = new Console();
		if (!MessageEditingUtils.editAddresses(msg, addressBook, console))
			return false;
		editContent(msg, console);
		return true;
	}

	private void editContent(Cable msg, Console console) {
		String messageContent = console.readLine("Enter cable's content:");
		if (!messageContent.trim().equals("")) {
			msg.setContent(messageContent);
		}
	}
}