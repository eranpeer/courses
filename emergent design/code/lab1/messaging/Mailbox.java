package messaging;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import thirdparty.Console;

public class Mailbox implements Iterable<Message> {

	private String address;

	private List<Message> messages;

	public Mailbox(String address) {
		this.address = address;
		messages = new ArrayList<Message>();
	}

	public void add(Message msg) {
		messages.add(msg);
	}

	public Message delete(int msgId) {
		Message m = (Message) messages.get(msgId);
		messages.remove(msgId);
		return m;
	}

	public void delete(Message msg) {
		messages.remove(msg);
	}

	public String getAddress() {
		return address;
	}

	public int count() {
		return messages.size();
	}

	@Override
	public Iterator<Message> iterator() {
		return messages.iterator();
	}

	public void show(Mailbox mailbox, Console console) {

		if (mailbox.count() == 0) {
			console.writeLine("Mailbox " + mailbox.getAddress() + " is empty");
			return;
		}

		console.writeLine("Content of mailbox " + mailbox.getAddress() + ":");
		int msgNum = 0;
		for (Message m : mailbox) {
			msgNum++;
			console.write(msgNum + ": ");
			m.show(console);
		}
	}

}