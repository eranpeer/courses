package messaging;
import thirdparty.Console;

public class Postcard extends Message {

	public void setText(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}
	
	public void show(Console console) {
		console.writeLine("Postcard: from " + getOrigin());
	}

	String text;
}