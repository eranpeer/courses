package messaging;
import thirdparty.Console;

public class Cable extends Message {

	public void setContent(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	@Override
	public void show(Console console) {
		console.writeLine("Cable: " + getContent());
	}

	private String content;
}
