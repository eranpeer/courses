package messaging;
public class Mailer {
	public Mailer(AddressBook addressBook) {
		this.addressBook = addressBook;
	}

	public void deliver(Message m) {
		Mailbox mbx = addressBook.getMailbox(m.getDestination());
		mbx.add(m);
	}

	private AddressBook addressBook;
}