package messaging;
import java.util.Hashtable;

public class AddressBook {

	private Hashtable<String,Mailbox> content;

	public AddressBook() {
		content = new Hashtable<String,Mailbox>();
	}

	public Mailbox getMailbox(String address) {
		return (Mailbox) content.get(address);
	}

	public void addMailbox(Mailbox mbx) {
		content.put(mbx.getAddress(), mbx);
	}

}