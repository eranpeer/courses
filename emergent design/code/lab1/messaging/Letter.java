package messaging;
import thirdparty.Console;

public class Letter extends Message {

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSubject() {
		return subject;
	}

	public String getContent() {
		return content;
	}

	public void show(Console console) {
		console.writeLine("Letter: " + getSubject());
	}
	
	String subject;

	String content;
}