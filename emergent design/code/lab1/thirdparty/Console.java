package thirdparty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class Console {
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

	public String readLine(String prompt) {
		String line = null;
		System.out.println(prompt);
		try {
			line = reader.readLine();
		} catch (IOException e) {
		}
		return line;
	}

	public void writeLine(String str) {
		System.out.println(str);
	}

	public void write(String str) {
		System.out.print(str);
	}
}